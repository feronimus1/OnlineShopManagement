﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopManager
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            //textBoxPassword.Text = "user";
            //textBoxUserName.Text = "user";
            caching.ReCache();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            DataAccess dts = new DataAccess();
            SQLCreation sql = new SQLCreation();
            int author = Int32.Parse(dts.LoginAuth(Properties.Settings.Default.DBName, sql.AuthUser(textBoxUserName.Text, textBoxPassword.Text)));
            if (author > 0)
            {
                caching.LoginFlag = author;
                this.Close();
            }
            else
            {

                //textBoxUserName.Text = "";
                textBoxPassword.Text = "";
                textBoxPassword.Focus();
                MessageBox.Show("Λάθος κωδικός ή όνομα χρήστη. ");
            }
        }
        public void exitClick()
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {
            // DataAccess dt = new DataAccess();
            //SQLCreation sql = new SQLCreation();
            //int author = Int32.Parse(dt.LoginAuth(Properties.Settings.Default.DBName, sql.AuthUser(textBoxUserName.Text, textBoxPassword.Text)));

            //int author = 1;
            //if (author > 0)
            //{
            //    caching.LoginFlag = author;
            //    this.Close();
            //}
            //else
            //{

            //    //textBoxUserName.Text = "";
            //    textBoxPassword.Text = "";
            //    textBoxPassword.Focus();
            //    MessageBox.Show("Λάθος κωδικός ή όνομα χρήστη. ");
            //}
        }
    }
}
