﻿namespace ShopManager
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelLogo = new System.Windows.Forms.Panel();
            this.pictureBox_Logo = new System.Windows.Forms.PictureBox();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.panelleft = new System.Windows.Forms.Panel();
            this.button_Users = new System.Windows.Forms.Button();
            this.button_CarGR = new System.Windows.Forms.Button();
            this.button_GetProduct = new System.Windows.Forms.Button();
            this.button_Sell = new System.Windows.Forms.Button();
            this.button_Dashboard = new System.Windows.Forms.Button();
            this.elementHostMain = new System.Windows.Forms.Integration.ElementHost();
            this.panelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Logo)).BeginInit();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLogo
            // 
            this.panelLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(126)))), ((int)(((byte)(49)))));
            this.panelLogo.Controls.Add(this.pictureBox_Logo);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(233, 133);
            this.panelLogo.TabIndex = 0;
            // 
            // pictureBox_Logo
            // 
            this.pictureBox_Logo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.pictureBox_Logo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_Logo.Image = global::ShopManager.Properties.Resources.ShopManagerLogoLight;
            this.pictureBox_Logo.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_Logo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox_Logo.Name = "pictureBox_Logo";
            this.pictureBox_Logo.Size = new System.Drawing.Size(233, 133);
            this.pictureBox_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox_Logo.TabIndex = 0;
            this.pictureBox_Logo.TabStop = false;
            this.pictureBox_Logo.Click += new System.EventHandler(this.pictureBox_Logo_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.panelleft);
            this.panelMenu.Controls.Add(this.button_Users);
            this.panelMenu.Controls.Add(this.button_CarGR);
            this.panelMenu.Controls.Add(this.button_GetProduct);
            this.panelMenu.Controls.Add(this.button_Sell);
            this.panelMenu.Controls.Add(this.button_Dashboard);
            this.panelMenu.Controls.Add(this.panelLogo);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(233, 812);
            this.panelMenu.TabIndex = 1;
            this.panelMenu.SizeChanged += new System.EventHandler(this.panelMenu_SizeChanged_1);
            // 
            // panelleft
            // 
            this.panelleft.BackColor = System.Drawing.Color.Gainsboro;
            this.panelleft.Location = new System.Drawing.Point(213, 133);
            this.panelleft.Margin = new System.Windows.Forms.Padding(0);
            this.panelleft.Name = "panelleft";
            this.panelleft.Size = new System.Drawing.Size(20, 100);
            this.panelleft.TabIndex = 3;
            this.panelleft.Visible = false;
            // 
            // button_Users
            // 
            this.button_Users.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.button_Users.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Users.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Users.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(55)))), ((int)(((byte)(67)))));
            this.button_Users.FlatAppearance.BorderSize = 0;
            this.button_Users.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_Users.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_Users.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Users.Font = new System.Drawing.Font("DejaVu Sans Condensed", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Users.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_Users.Location = new System.Drawing.Point(0, 693);
            this.button_Users.Margin = new System.Windows.Forms.Padding(3, 16, 3, 16);
            this.button_Users.Name = "button_Users";
            this.button_Users.Size = new System.Drawing.Size(233, 140);
            this.button_Users.TabIndex = 3;
            this.button_Users.Text = "Χρήστες";
            this.button_Users.UseVisualStyleBackColor = false;
            this.button_Users.Click += new System.EventHandler(this.button_Users_Click);
            // 
            // button_CarGR
            // 
            this.button_CarGR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.button_CarGR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_CarGR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_CarGR.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_CarGR.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(55)))), ((int)(((byte)(67)))));
            this.button_CarGR.FlatAppearance.BorderSize = 0;
            this.button_CarGR.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_CarGR.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_CarGR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_CarGR.Font = new System.Drawing.Font("DejaVu Sans Condensed", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_CarGR.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_CarGR.Location = new System.Drawing.Point(0, 553);
            this.button_CarGR.Margin = new System.Windows.Forms.Padding(3, 16, 3, 16);
            this.button_CarGR.Name = "button_CarGR";
            this.button_CarGR.Size = new System.Drawing.Size(233, 140);
            this.button_CarGR.TabIndex = 4;
            this.button_CarGR.Text = "Διασύνδεση";
            this.button_CarGR.UseVisualStyleBackColor = false;
            this.button_CarGR.Click += new System.EventHandler(this.button_CarGR_Click);
            // 
            // button_GetProduct
            // 
            this.button_GetProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.button_GetProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_GetProduct.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_GetProduct.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(55)))), ((int)(((byte)(67)))));
            this.button_GetProduct.FlatAppearance.BorderSize = 0;
            this.button_GetProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_GetProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_GetProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_GetProduct.Font = new System.Drawing.Font("DejaVu Sans Condensed", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_GetProduct.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_GetProduct.Location = new System.Drawing.Point(0, 413);
            this.button_GetProduct.Margin = new System.Windows.Forms.Padding(3, 16, 3, 16);
            this.button_GetProduct.Name = "button_GetProduct";
            this.button_GetProduct.Size = new System.Drawing.Size(233, 140);
            this.button_GetProduct.TabIndex = 5;
            this.button_GetProduct.Text = "ΠΑΡΑΓΓΕΛΙΑ";
            this.button_GetProduct.UseVisualStyleBackColor = false;
            this.button_GetProduct.Visible = false;
            this.button_GetProduct.Click += new System.EventHandler(this.button_GetProduct_Click);
            // 
            // button_Sell
            // 
            this.button_Sell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.button_Sell.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Sell.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Sell.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(55)))), ((int)(((byte)(67)))));
            this.button_Sell.FlatAppearance.BorderSize = 0;
            this.button_Sell.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_Sell.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_Sell.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Sell.Font = new System.Drawing.Font("DejaVu Sans Condensed", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Sell.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_Sell.Location = new System.Drawing.Point(0, 273);
            this.button_Sell.Margin = new System.Windows.Forms.Padding(3, 16, 3, 16);
            this.button_Sell.Name = "button_Sell";
            this.button_Sell.Size = new System.Drawing.Size(233, 140);
            this.button_Sell.TabIndex = 2;
            this.button_Sell.Text = "Αποθήκη";
            this.button_Sell.UseVisualStyleBackColor = false;
            this.button_Sell.Click += new System.EventHandler(this.button_Sell_Click);
            // 
            // button_Dashboard
            // 
            this.button_Dashboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.button_Dashboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_Dashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Dashboard.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Dashboard.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(55)))), ((int)(((byte)(67)))));
            this.button_Dashboard.FlatAppearance.BorderSize = 0;
            this.button_Dashboard.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_Dashboard.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(89)))), ((int)(((byte)(110)))));
            this.button_Dashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Dashboard.Font = new System.Drawing.Font("MS Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Dashboard.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_Dashboard.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button_Dashboard.Location = new System.Drawing.Point(0, 133);
            this.button_Dashboard.Margin = new System.Windows.Forms.Padding(3, 16, 3, 16);
            this.button_Dashboard.Name = "button_Dashboard";
            this.button_Dashboard.Size = new System.Drawing.Size(233, 140);
            this.button_Dashboard.TabIndex = 0;
            this.button_Dashboard.Text = "Dashboard";
            this.button_Dashboard.UseVisualStyleBackColor = false;
            this.button_Dashboard.Click += new System.EventHandler(this.button_Dashboard_Click);
            // 
            // elementHostMain
            // 
            this.elementHostMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.elementHostMain.BackgroundImage = global::ShopManager.Properties.Resources.logoInvWhite;
            this.elementHostMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.elementHostMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHostMain.Location = new System.Drawing.Point(233, 0);
            this.elementHostMain.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.elementHostMain.Name = "elementHostMain";
            this.elementHostMain.Size = new System.Drawing.Size(1412, 812);
            this.elementHostMain.TabIndex = 2;
            this.elementHostMain.Text = "elementHost1";
            this.elementHostMain.Child = null;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.ClientSize = new System.Drawing.Size(1645, 812);
            this.Controls.Add(this.elementHostMain);
            this.Controls.Add(this.panelMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shop Manager";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Logo)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Integration.ElementHost elementHostMain;
        private System.Windows.Forms.PictureBox pictureBox_Logo;
        private System.Windows.Forms.Button button_Users;
        private System.Windows.Forms.Button button_Sell;
        private System.Windows.Forms.Button button_Dashboard;
        private System.Windows.Forms.Button button_GetProduct;
        private System.Windows.Forms.Button button_CarGR;
        private System.Windows.Forms.Panel panelleft;
    }
}

