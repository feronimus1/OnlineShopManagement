﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Globalization;
using System.Net;
using System.IO;
using System.Windows.Forms;
using System.Windows.Threading;


namespace ShopManager
{
    /// <summary>
    /// Interaction logic for AddAD.xaml
    /// </summary>
    public partial class AddAD : System.Windows.Controls.UserControl
    {
        //Variables
        List<CarProduct> AdProducts = new List<CarProduct>();
        List<CarProduct> Cars = new List<CarProduct>();
        List<VehicleStorage> VehicleStorages = new List<VehicleStorage>();
        List<WorkFlowScriptIDDataStore> lst = new List<WorkFlowScriptIDDataStore>();
        List<categories> data = new List<categories>();
        List<vehicles> vehicles = new List<vehicles>();
        List<string> makelist = new List<string>();
        List<CarProduct> ListAddproducts = new List<CarProduct>();
        List<VehicleStorage> vehiclestorageList = new List<VehicleStorage>();
        List<CatNameIDPair> catlist = new List<CatNameIDPair>();
        List<string> photolist = new List<string>();
        //List<AdTitleIDPair> adlist = new List<AdTitleIDPair>();
        DataAccess dts = new DataAccess();
        string Imagepath = "";
        string Url = "";
        bool IsParent = true;
        string ThisUniqueID;
        bool IsSaved = false;
        //Start Class
        public AddAD()
        {
            InitializeComponent();

        }
        
       

        public void LoadProcedure()
        {
            busyIndo.IsBusy = true;
            Cars.Clear();
            Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            VehicleStorages.Clear();
            VehicleStorages.AddRange(dts.ReadVehicleStorage(Properties.Settings.Default.DBName));
            //LoadComboBoxes(); 
            LoadCachedProduct();
            IsSaved = true;
            LoadProductsData();
            //
            LoadComboBoxes();
            //
            Picturebox.Source = null;
            busyIndo.IsBusy = false;
            IsParent = caching.CachedCar.isParent == "ΟΧΙ" ? false : true;

        }
        public void LoadCachedProduct()
        {
            ThisUniqueID = caching.CachedCar.unique_id;
            LabelID.Content = "Αγγελία " + caching.CachedCar.unique_id;
            TextTitle.Text = caching.CachedCar.title;
            TextPrice.Value = (long)caching.CachedCar.price;
            //TextAmount.Value = caching.CachedCar.amount;
            //TextTaken.Value = caching.CachedCar.taken;
            //TextColor.Text = caching.CachedCar.color;
            //StoragePlaceTextBox.Text = caching.CachedCar.StoragePlace;
            TextDescription.Text = caching.CachedCar.description;
            TextCondition.Text = caching.CachedCar.conditions;
            CheckBoxPublish.IsChecked = caching.CachedCar.Public;

            //Textcategory_id.Content = caching.CachedCar.category_name;
        }
        public void LoadProductsData()
        {
            
            foreach (CarProduct c in Cars)
            {
                //IMPORTANT
                if (!c.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList().Contains(ThisUniqueID)) continue;

                //Category
                List<string> tempID = new List<string>();
                List<string> tempName = new List<string>();
                tempName = c.category_name.Split(new string[] { ",,," }, StringSplitOptions.None).ToList();
                tempID = c.category_id.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList();
                for (int i = 0; i < tempName.Count; i++)
                {
                    if (tempName[i].Length == 0) continue;

                    CatNameIDPair tempcat = new CatNameIDPair(tempName[i], tempID[i]);
                    bool flag = true;
                    foreach (CatNameIDPair g in catlist)
                    {
                        if (g.Have(tempcat))
                        {
                            flag = false;
                            break;
                        }
                    }
                    if (flag)
                    {
                        catlist.Add(tempcat);
                    }
                }

                //Photos
                foreach (string photo in c.photo.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList())
                {
                    if (photolist.Contains(photo)) continue;
                    if (photo == "") continue;
                    photolist.Add(photo);
                }
                //photolist = c.photo.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList();
                

                //Vehicles
                //vehiclestorageList.AddRange(dts.ReadVehicleStorageID(Properties.Settings.Default.DBName, c.unique_id));

                foreach (VehicleStorage temp in VehicleStorages.Where(vs => vs.unique_id == c.unique_id))
                {
                    bool flag = true;
                    foreach (VehicleStorage g in vehiclestorageList)
                    {
                        if (g.Have(temp))
                        {
                            flag = false;
                            break;
                        }
                    }
                    if (flag)
                    {
                        vehiclestorageList.Add(temp);                        
                    }
                }
               

                //Products
                AdProducts.Add(c);
               

            }

            ListBoxCategory_id.ItemsSource = catlist;
            ListBoxCategory_id.DisplayMemberPath = "name";

            ListBoxPhoto.ItemsSource = photolist;

            ListBoxMake.ItemsSource = vehiclestorageList;
            ListBoxMake.DisplayMemberPath = "readable";

            ListBoxAd.ItemsSource = AdProducts;
            //try
            //{                
            //    ListBoxAd.AutoGenerateColumns = false;
            //    for (int i = 0; i < 23; i++) ListBoxAd.Columns[i].Visibility = Visibility.Collapsed;
            //    ListBoxAd.Columns[0].Visibility = Visibility.Visible;
            //    ListBoxAd.Columns[1].Visibility = Visibility.Visible;
            //    ListBoxAd.Columns[6].Visibility = Visibility.Visible;
            //}
            //catch (Exception)
            //{
            //}
           

        }
       

        private void Button_Click_IncreasePrice(object sender, RoutedEventArgs e)
        {

            TextPrice.Value = Increase10(TextPrice.Value ?? 0);
        }

        private void Button_Click_DecreasePrice(object sender, RoutedEventArgs e)
        {

            TextPrice.Value = Decrease10(TextPrice.Value ?? 0);
        }
        private long Increase10(long value)
        {
            return value + 10;
        }
        private long Decrease10(long value)
        {
            long temp = value - 10;
            if (temp < 0) value = 0;
            else value = temp;
            return value;
        }

      
        private void loadImage(string url)
        {
            //if (!Properties.Settings.Default.ShowPhotos) return;
            try
            {
                Url = url;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Url, UriKind.Absolute);
                bitmap.EndInit();
                Picturebox.Source = bitmap;
            }
            catch (Exception)
            {
            }
        }

        private void ListBoxPhoto_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string curItem = ListBoxPhoto.SelectedItem.ToString();
                loadImage(curItem);
            }
            catch (Exception)
            {

                return;
            }
          
        }
        


        internal void EndProcedure()
        {
            ListBoxAd.ItemsSource = null;
            ListBoxCategory_id.ItemsSource = null;
            ListBoxMake.ItemsSource = null;
            ListBoxPhoto.ItemsSource = null;
            Gridproducts.ItemsSource = null;

            Cars.Clear();
            makelist.Clear();
            vehiclestorageList.Clear();
            catlist.Clear();
            photolist.Clear();
            AdProducts.Clear();
            ListAddproducts.Clear();



            VehicleStorages.Clear();
            lst.Clear();
            Imagepath = "";
            Url = "";
            IsParent = true;
            ThisUniqueID = "";
            CheckBoxPublish.IsChecked = true;
        }

        private void Button_Click_Update(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckConditions()) return;


                Cars.Clear();
                Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
                //VehicleStorages.Clear();
                //VehicleStorages.AddRange(dts.ReadVehicleStorage(Properties.Settings.Default.DBName));
                string Cat_names = "";
                string Cat_IDs = "";

                try
                {

                    foreach (CatNameIDPair cat in catlist)
                    {
                        Cat_names = Cat_names + ",,," + cat.name;
                        Cat_IDs = Cat_IDs + "!#!" + cat.id;
                    }


                    Cat_names = Cat_names.Remove(0, 3);
                    Cat_IDs = Cat_IDs.Remove(0, 3);
                }
                catch (Exception)
                {

                    Cat_names ="";
                    Cat_IDs = "";
                }

                //List<string> MakeIDStrings = new List<string>();
                //List<string> MakeNameStrings = new List<string>();
                //List<string> ModelIDStrings = new List<string>();
                //List<string> ModelNameStrings = new List<string>();
                //int yearFromStrings = 0;
                //int yearToStrings = 0;

                //foreach (VehicleStorage v in vehiclestorageList)
                //{
                //    if (!MakeIDStrings.Contains(v.make_id)) MakeIDStrings.Add(v.make_id);
                //    if (!MakeNameStrings.Contains(v.make_name)) MakeNameStrings.Add(v.make_name);
                //    if (!ModelIDStrings.Contains(v.model_id)) ModelIDStrings.Add(v.model_id);
                //    if (!ModelNameStrings.Contains(v.model_name)) ModelNameStrings.Add(v.model_name);
                //    yearFromStrings = v.yearfrom;
                //    yearToStrings = v.yearto;
                //}

                string adIDStrings = "";


                //if (adlist.Count > 0)
                //{
                //    foreach (AdTitleIDPair ad in adlist)
                //    {
                //        adIDStrings = adIDStrings + "!#!" + ad.id;
                //    }
                //    adIDStrings = adIDStrings.Remove(0, 3);
                //}

                CarProduct car = new CarProduct
                {
                    unique_id = ThisUniqueID,
                    category_id = Cat_IDs,
                    category_name = Cat_names,
                    title = TextTitle.Text,
                    description = TextDescription.Text,
                    price = TextPrice.Value ?? 0,
                    amount =  0,
                    taken =  0,
                    color = "N/A",
                    StoragePlace = "",
                    photo = "",
                    conditions = TextCondition.Text,
                    isParent = "ΝΑΙ",
                    update_interval = 1,
                    make = " ",
                    make_name = " ",
                    model = " ",
                    model_name = " ",
                    yearfrom = 0,
                    yearto = 0,
                    AdId = adIDStrings,
                    manufacturer_number = "none",
                    aftermarket_number = "none",
                    info = "",
                    Public = CheckBoxPublish.IsChecked ?? false
                };

                //if (Combo_Aftermarket_Manufacturer.Text == "Aftermarket" && !String.IsNullOrEmpty(Text_Aftermarket_Manufacturer.Text)) car.aftermarket_number = Text_Aftermarket_Manufacturer.Text;
                //else if (Combo_Aftermarket_Manufacturer.Text == "Manufacturer" && !String.IsNullOrEmpty(Text_Aftermarket_Manufacturer.Text)) car.manufacturer_number = Text_Aftermarket_Manufacturer.Text;



                List<CarProduct> lst = new List<CarProduct>();
                lst.Add(car);

                //If exists update it
                if (dts.CheckIfCarProductExists(Properties.Settings.Default.DBName, car.unique_id))
                {
                    dts.UpdateCarPoduct(Properties.Settings.Default.DBName, lst);

                }

                ////Does not exist and is a parent
                //else if (car.isParent == "ΝΑΙ")
                //{
                //    dts.CreateCarProduct(Properties.Settings.Default.DBName, lst);
                //}

                //Does not exist  and is a child
                else
                {
                    //If parent exists
                    //string temp2 = car.unique_id.Split('-')[0];
                    //if (!dts.CheckIfCarProductExists(Properties.Settings.Default.DBName, car.unique_id.Split('-')[0]))
                    //{
                    //    System.Windows.MessageBox.Show("Δεν υπάρχει πατέρας για αυτο το αντικειμενο. Κάτι έχει πάει στραβά!");
                    //    return;
                    //}
                    dts.CreateCarProduct(Properties.Settings.Default.DBName, lst);
                }

                //Create/Update vehicle storages and
                //Remove existing vehicle storages from vehiclestoragelist

                //List<VehicleStorage> vlist = new List<VehicleStorage>();
                //foreach (VehicleStorage v in VehicleStorages)
                //{
                //    if (v.unique_id == ThisUniqueID)
                //    {
                //        vlist.Add(v);
                //    }
                //}
                //dts.DeleteVehicleStorage(Properties.Settings.Default.DBName, vlist);
                //dts.CreateVehicleStorage(Properties.Settings.Default.DBName, vehiclestorageList);

                //Update Parent quantity
                //if (!IsParent)
                //{
                //    //Search all items of parent and add the quantities. Add them to parent
                //    List<CarProduct> temp = new List<CarProduct>();
                //    temp.AddRange(dts.ReadCarProductLike(Properties.Settings.Default.DBName, car.unique_id.Split('-')[0]));
                //    int count = 0;
                //    lst.Clear();
                //    foreach (CarProduct tempcar in temp)
                //    {
                //        if (tempcar.isParent == "ΟΧΙ")
                //        {
                //            count += tempcar.amount;
                //        }
                //        if (tempcar.isParent == "ΝΑΙ")
                //        {
                //            lst.Add(tempcar);
                //        }
                //    }
                //    lst[0].amount = count;
                //    dts.UpdateCarPoduct(Properties.Settings.Default.DBName, lst);
                //}
                IsSaved = true;
                System.Windows.MessageBox.Show("Αποθηκεύτηκε με Επιτυχία!");
            }
            catch (Exception ex)
            {

                System.Windows.MessageBox.Show("Παρουσιάστηκε πρόβλημα. Δεν αποθηκεύτηκε η Αγγελία.      " + ex.ToString());
            }


        }
        private bool CheckConditions()
        {
            //Open when halndling code
            //if (IsParent)
            //{
            //    if (String.IsNullOrEmpty(TextUnique_id_Parent.Text))
            //    {
            //        //Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε Κωδικό!");

            //        //Autocreate code 
            //        return true;
            //    }
            //}
            //else
            //{
            //    if (String.IsNullOrEmpty(TextUnique_id_Child_Secondary.Text) || String.IsNullOrEmpty(TextUnique_id_Child_Primary.SelectedValue.ToString()))
            //    {
            //        //Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε Κωδικό!");

            //        //Autocreate code 
            //        return true;

            //    }
            //}


            if (String.IsNullOrEmpty(TextTitle.Text)
                     //|| catlist.Count == 0
                     || String.IsNullOrEmpty((TextPrice.Value ?? 0).ToString())
                     //|| String.IsNullOrEmpty((TextInterval.Value ?? 0).ToString())
                     //|| String.IsNullOrEmpty(TextPhoto.Text)
                     || String.IsNullOrEmpty(TextCondition.Text)
                         //|| String.IsNullOrEmpty(TextisParent.Text)
                         //|| String.IsNullOrEmpty(TextColor.Text)
                         || String.IsNullOrEmpty(TextDescription.Text)
                             //|| vehiclestorageList.Count == 0
                             //|| String.IsNullOrEmpty(TextModel.SelectedValue.ToString())
                             //|| String.IsNullOrEmpty(TextYearfrom.Text)
                             //|| String.IsNullOrEmpty(TextYearTo.Text)
                             //|| String.IsNullOrEmpty((TextAmount.Value ?? 0).ToString()))
                             )

            {
                Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε όλα τα πεδία.");
                return true;
            }
            else return false;
        }
        private void Button_Click_ΝewProduct(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Θα χάσετε ότι πληροφορίες ΔΕΝ έχετε αποθηκεύσει. Είστε σίγουροι ότι θέλετε να συνεχίσετε;", "ΠΡΟΣΟΧΗ", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                CleanProcedure(NewCode());
            }
        }
        public string NewCode()
        {
            String ran = "";
            bool flagAd = true;
            List<CarProduct> tempCars = new List<CarProduct>();
            tempCars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            while (flagAd)
            {
                Random generator = new Random();
                ran = generator.Next(0, 999).ToString("D4");
                flagAd = tempCars.FindIndex(df => df.unique_id == ran) > 0 ? true : false;
            }
            ThisUniqueID = ran;
            return ran;
        }
        public void CleanProcedure(string id)
        {
            busyIndo.IsBusy = true;
            caching.CachedCar = null;
            Cars.Clear();
            Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            VehicleStorages.Clear();
            VehicleStorages.AddRange(dts.ReadVehicleStorage(Properties.Settings.Default.DBName));
            LoadComboBoxes();
            //StartClean
            LabelID.Content = "Αγγελία " + id;
            TextTitle.Text = "";
            TextPrice.Value = 0;
            //TextAmount.Value = 0;
            //TextTaken.Value = 0;
            //TextColor.Text = "N/A";
            //StoragePlaceTextBox.Text = "";
            TextDescription.Text = "";
            make.Text = "";
            model.Text = "";
            //Textcategory_id.Content = caching.CachedCar.category_name;
            ListBoxCategory_id.ItemsSource = null;
            catlist.Clear();
            ListBoxCategory_id.DisplayMemberPath = "name";


            photolist.Clear();
            ListBoxPhoto.ItemsSource = null;


            ListBoxAd.ItemsSource = null;
            AdProducts.Clear();
            ListBoxAd.DisplayMemberPath = "title";


            vehiclestorageList.Clear();
            ListBoxMake.ItemsSource = vehiclestorageList;
            ListBoxMake.DisplayMemberPath = null;
            //end

            Picturebox.Source = null;
            busyIndo.IsBusy = false;
            IsParent = false;

        }

        private void Button_Click_DeleteAd(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Η Αγγελία αυτη θα διαγραφεί μόνιμα. Είστε σίγουροι ότι θέλετε να συνεχίσετε;", "ΠΡΟΣΟΧΗ", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                Cars.Clear();
                Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));

                List<CarProduct> lst = new List<CarProduct>();
                foreach (CarProduct item in Cars)
                {
                    if (item.unique_id == ThisUniqueID) lst.Add(item);
                }
                ////delete vehicle storages

                //List<VehicleStorage> vlist = new List<VehicleStorage>();
                //foreach (VehicleStorage v in dts.ReadVehicleStorage(Properties.Settings.Default.DBName))
                //{
                //    if (v.unique_id == ThisUniqueID)
                //    {
                //        vlist.Add(v);
                //    }
                //}
                //dts.DeleteVehicleStorage(Properties.Settings.Default.DBName, vlist);


                //remove this ad from ALL products

               
                List<CarProduct> lstupdate = new List<CarProduct>();
                foreach (CarProduct item in Cars)
                {
                    if (item.isParent == "ΝΑΙ") continue;


                    List<string> productlist = new List<string>();
                    foreach (string id in item.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList())
                    {
                        if (id == "") continue;
                        //if (id == ThisUniqueID) continue;
                        productlist.Add(id);
                    }



                    if (productlist.Contains(ThisUniqueID))
                    {
                        //item.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList().Remove(ThisUniqueID);
                        item.AdId = "";
                        productlist.Remove(ThisUniqueID);
                        if (productlist.Count > 0)
                        {
                            foreach (string ad in productlist)
                            {
                                item.AdId = item.AdId + "!#!" + ad;
                            }
                            item.AdId = item.AdId.Remove(0, 3);
                        }
                        lstupdate.Add(item);
                    }
                }


                dts.UpdateCarPoduct(Properties.Settings.Default.DBName, lstupdate);
                dts.DeleteCarPoduct(Properties.Settings.Default.DBName, lst);

                CleanProcedure("");
                //Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ ξαναφορτώσετε την καρτέλα ΑΠΟΘHΚΗΣ", "ΠΡΟΣΟΧΗ", MessageBoxButton.OK, MessageBoxImage.Asterisk);

            }
        }
        private void DataGrid_1_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ListBoxAd.Focus();
        }
        private void DataGrid_2_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Gridproducts.Focus();
        }
        private void LoadComboBoxes()
        {
            //LoadComboBoxes();

            TextCondition.ItemsSource = new List<string> { "Μεταχειρισμένο", "Καινούριο", "Ανακατασκευή" };
            TextCondition.SelectedIndex = 0;
            //AdProducts
            ListAddproducts.AddRange(Cars.Where(c => c.isParent == "ΟΧΙ"));
            try
            {
                foreach (CarProduct item in AdProducts)
                {
                    if (ListAddproducts.Contains(item)) ListAddproducts.Remove(item);
                }
            }
            catch { }
            Gridproducts.ItemsSource = ListAddproducts;            
        }


        private void Button_Click_AddProductToAD(object sender, RoutedEventArgs e)
        {
            //handle non existing one yet and save


            if (!IsSaved)
            {
                //saveit
                CarProduct car = new CarProduct
                {
                    unique_id = ThisUniqueID,
                    category_id = "",
                    category_name = "",
                    title = TextTitle.Text == "" ? "no title" : TextTitle.Text,
                    description = TextDescription.Text == "" ? "no description" : TextDescription.Text,
                    price = TextPrice.Value ?? 0,
                    amount = 0,
                    taken = 0,
                    color = "N/A",
                    StoragePlace = "",
                    photo = "",
                    conditions = TextCondition.Text,
                    isParent = "ΝΑΙ",
                    update_interval = 1,
                    make = "",
                    make_name = "",
                    model = "",
                    model_name = "",
                    yearfrom = 0,
                    yearto = 0,
                    AdId = "",
                    manufacturer_number = "none",
                    aftermarket_number = "none"
                };
                List<CarProduct> carlist = new List<CarProduct>();
                carlist.Add(car);
                dts.CreateCarProduct(Properties.Settings.Default.DBName, carlist);
            }

            //IsSaved = true;


            try
            {
                CarProduct temp = (CarProduct)Gridproducts.SelectedItem;
                if (temp == null)  return;
                List<string> adlist = new List<string>();

                foreach (string id in temp.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList())
                {
                    if (id == "") continue;
                    adlist.Add(id);
                }
                if (!adlist.Contains(ThisUniqueID))  adlist.Add(ThisUniqueID);

                string final ="";
                if (adlist.Count > 0)
                {
                    foreach (string ad in adlist)
                    {
                        final = final + "!#!" + ad;
                    }
                    final = final.Remove(0, 3);
                }

                temp.AdId = final;
                List<CarProduct> carlist = new List<CarProduct>();
                carlist.Add(temp);
                dts.UpdateCarPoduct(Properties.Settings.Default.DBName,carlist);                
                DropdownAds.IsOpen = false;
                if (caching.CachedCar == null) HandleNewItemCaching();
                EndProcedure();
                LoadProcedure();
            }
            catch (Exception)
            {

                return;
            }
            
        }

        private void Button_Click_RemoveProductFromAd(object sender, RoutedEventArgs e)
        {
            try
            {
                CarProduct temp = (CarProduct)ListBoxAd.SelectedItem;
                if (temp == null) return;


                List<string> adlist = new List<string>();

                foreach (string id in temp.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList())
                {
                    if (id == "") continue;
                    adlist.Add(id);
                }
                //if (!adlist.Contains(ThisUniqueID)) adlist.Add(ThisUniqueID);
                adlist.Remove(ThisUniqueID);
                string final = "";
                if (adlist.Count > 0)
                {
                    foreach (string ad in adlist)
                    {
                        final = final + "!#!" + ad;
                    }
                    final = final.Remove(0, 3);
                }
                temp.AdId = final;
                List<CarProduct> carlist = new List<CarProduct>();
                carlist.Add(temp);
                dts.UpdateCarPoduct(Properties.Settings.Default.DBName, carlist);
                DropdownAds.IsOpen = false;
                if (caching.CachedCar == null) HandleNewItemCaching();
                EndProcedure();
                LoadProcedure();
            }
            catch (Exception)
            {

                return;
            }
        }
        public void HandleNewItemCaching()
        {
            CarProduct car = new CarProduct
            {
                unique_id = ThisUniqueID,
                category_id = "",
                category_name = "",
                title = TextTitle.Text,
                description = TextDescription.Text,
                price = TextPrice.Value ?? 0,
                amount = 0,
                taken = 0,
                color = "N/A",
                StoragePlace = "",
                photo = "",
                conditions = TextCondition.Text,
                isParent = "ΝΑΙ",
                update_interval = 1,
                make = "",
                make_name = "",
                model = "",
                model_name = "",
                yearfrom = 0,
                yearto = 0,
                AdId = "",
                manufacturer_number = "none",
                aftermarket_number = "none"
            };
            caching.CachedCar = car;
        }

        private void ListBoxAd_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CarProduct temp = (CarProduct)ListBoxAd.SelectedItem;
            if (temp == null) return;

            caching.CachedCar = null;
            caching.CachedCar = temp;

            caching.CloseAd = 1;
        }

      
    }
}
