﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Globalization;
using System.Net;
using System.IO;
using System.Windows.Forms;
using System.Windows.Threading;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for AddProduct.xaml
    /// </summary>
    public partial class AddProduct : System.Windows.Controls.UserControl
    {
        //Variables
        List<CarProduct> Cars = new List<CarProduct>();
        List<VehicleStorage> VehicleStorages = new List<VehicleStorage>();
        List<WorkFlowScriptIDDataStore> lst = new List<WorkFlowScriptIDDataStore>();
        List<categories> data = new List<categories>();
        List<vehicles> vehicles = new List<vehicles>();
        List<string> makelist = new List<string>();
        List<VehicleStorage> vehiclestorageList = new List<VehicleStorage>();
        List<CatNameIDPair> catlist = new List<CatNameIDPair>();
        List<string> photolist = new List<string>();
        List<CarProduct> adlist = new List<CarProduct>();
        DataAccess dts = new DataAccess();
        string Imagepath = "";
        string Url = "";
        bool IsParent = false;
        string ThisUniqueID;
        string CargrSearchCatID;
        string CargrSearchMakeName;
        string CargrSearchModelName;

        //Start Class
        public AddProduct()
        {
            InitializeComponent();

        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            busyIndo.IsBusy = true;
            //Combo_Aftermarket_Manufacturer.ItemsSource = new List<string> { "Aftermarket", "Manufacturer" };
            //Combo_Aftermarket_Manufacturer.SelectedIndex = 0;

           // TextColor.ItemsSource = caching.colors;




            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += InisializeData;
            worker.RunWorkerCompleted += (o, a) =>
            {
                UpdateWPF();
                busyIndo.IsBusy = false;
            };
            worker.RunWorkerAsync();
        }
        private void UpdateWPF()
        {


            treeView.ItemsSource = lst;
            TextMake.ItemsSource = vehicles;
            TextMake.SelectedIndex = 23;
            TextModel.SelectedIndex = 2;
            //TextInterval.Text = "1";
            TextYearfrom.ItemsSource = GetDateList();
            TextYearfrom.SelectedIndex = 0;
            TextYearTo.ItemsSource = GetDateList();
            TextYearTo.SelectedIndex = GetDateList().Count - 1;
            TextCondition.ItemsSource = new List<string> { "Μεταχειρισμένο", "Καινούριο", "Ανακατασκευή" };
            //TextisParent.ItemsSource = new List<string> { "ΝΑΙ", "ΟΧΙ" };
            //TextisParent.SelectedIndex = 0;
            TextCondition.SelectedIndex = 0;
        }

        public void LoadProcedure()
        {
            busyIndo.IsBusy = true;
            Cars.Clear();
            Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            VehicleStorages.Clear();
            VehicleStorages.AddRange(dts.ReadVehicleStorage(Properties.Settings.Default.DBName));
            LoadComboBoxes();
            LoadCachedProduct();
            Picturebox.Source = null;
            busyIndo.IsBusy = false;
            IsParent = caching.CachedCar.isParent == "ΟΧΙ" ? false : true;

        }
        public void LoadCachedProduct()
        {
            Cars.Clear();
            Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            ThisUniqueID = caching.CachedCar.unique_id;
            LabelID.Content = "Προϊόν " + caching.CachedCar.unique_id;
            TextTitle.Text = caching.CachedCar.title;
            TextPrice.Value = (long)caching.CachedCar.price;
            TextAmount.Value = caching.CachedCar.amount;
            TextTaken.Value = caching.CachedCar.taken;
            TextColor.Text = caching.CachedCar.color;
            StoragePlaceTextBox.Text = caching.CachedCar.StoragePlace;
            TextDescription.Text = caching.CachedCar.description;
            TextCondition.Text = caching.CachedCar.conditions;
            CheckBoxPublish.IsChecked = caching.CachedCar.Public;
            TextInfo.Text = caching.CachedCar.info;

            //Textcategory_id.Content = caching.CachedCar.category_name;
            List<string> tempID = new List<string>();
            List<string> tempName = new List<string>();
            tempName = caching.CachedCar.category_name.Split(new string[] { ",,," }, StringSplitOptions.None).ToList();
            tempID = caching.CachedCar.category_id.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList();
            for (int i = 0; i < tempName.Count; i++)
            {
                if (tempName[i].Length == 0)   continue;
                catlist.Add(new CatNameIDPair(tempName[i], tempID[i]));
            }
            ListBoxCategory_id.ItemsSource = catlist;
            ListBoxCategory_id.DisplayMemberPath = "name";
            ListBoxCategory_id.SelectedValuePath = "id";


            photolist = caching.CachedCar.photo.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList();            
            ListBoxPhoto.ItemsSource = photolist;


           // adlist = caching.CachedCar.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList();
            foreach (string id in caching.CachedCar.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList())
            {
                try
                {
                    if (id == "") continue;
                    adlist.AddRange(Cars.Where(c => c.unique_id == id));
                }
                catch (Exception)
                {
                    continue;
                }
              
            }
            ListBoxAd.ItemsSource = null;
            ListBoxAd.ItemsSource = adlist;
            //ListBoxAd.DisplayMemberPath = "title";

            //handle VehicleStorage data


            vehiclestorageList.Clear();
            vehiclestorageList.AddRange(VehicleStorages.Where(vs => vs.unique_id == ThisUniqueID));            
            ListBoxMake.ItemsSource = vehiclestorageList;
            ListBoxMake.DisplayMemberPath = "readable";
            ListBoxMake.SelectedValuePath = "IDStore";
            //end
        }      
        public void LoadComboBoxes()
        {
            TextColor.ItemsSource = caching.colors;
            TextUnique_id_Ads.ItemsSource = Cars.Where(c => c.isParent == "ΝΑΙ");
            //TextUnique_id_Ads.DisplayMemberPath = "title";
            //TextUnique_id_Ads.SelectedValuePath = "unique_id";
            TextColor.ItemsSource = caching.colors;
            TextCondition.ItemsSource = new List<string> { "Μεταχειρισμένο", "Καινούριο", "Ανακατασκευή" };

            TextMake.ItemsSource = vehicles;
            TextMake.SelectedIndex = 23;
            TextModel.SelectedIndex = 2;
        }

        private void Button_Click_IncreasePrice(object sender, RoutedEventArgs e)
        {
           
            TextPrice.Value = Increase10(TextPrice.Value ?? 0);
        }

        private void Button_Click_DecreasePrice(object sender, RoutedEventArgs e)
        {
            
            TextPrice.Value = Decrease10(TextPrice.Value ?? 0);
        }
        private long Increase10(long value)
        {
            return value + 10;
        }
        private long Decrease10(long value)
        {
            long temp = value - 10;
            if (temp < 0) value = 0;
            else value = temp;
            return value;
        }

        //Tree
        private void InisializeData(object sender, DoWorkEventArgs e)
        {
            while (caching.VehiclesCached.Count == 0 || caching.CategoriesCached.Count == 0)
            {
                System.Threading.Thread.Sleep(50);
            }
            while (caching.NumberOfCarProducts == 0 && caching.NumberOfCarProducts != 0)
            {
                System.Threading.Thread.Sleep(50);
            }
            data.Clear();
            data.AddRange(caching.CategoriesCached);
            vehicles.Clear();
            vehicles.AddRange(caching.VehiclesCached
                                 .GroupBy(item => item.make_id)
                                 .ToDictionary(grp => grp.Key, grp => grp.ToList())
                                 .Select(kvp => kvp.Value[0])
                                 .ToList());

            foreach (categories p in data.ToList())
            {
                //if (t.parent_id ==  "" && t.id=="30")
                if (p.parent_id == "77")
                {
                    WorkFlowScriptIDDataStore parent = new WorkFlowScriptIDDataStore()
                    {
                        Id = p.id,
                        Name = p.name,
                        ParentId = p.parent_id,
                        Place = 0,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    parent.Subcategories.AddRange(FindChilds(parent.Id));
                    lst.Add(parent);
                }
            }

        }
        public List<WorkFlowScriptIDDataStore> FindChilds(string id)
        {
            List<WorkFlowScriptIDDataStore> childList = new List<WorkFlowScriptIDDataStore>();
            while (data.Count == 0)
            {
                System.Threading.Thread.Sleep(10);
            }
            //int tempint = data.Count;
            //int count = 0;
            //categories tempcat;
            foreach (categories t in data.ToList())
            {
                //count++;
                if (t is null) continue;
                if (t.parent_id == id)
                {
                    WorkFlowScriptIDDataStore child = new WorkFlowScriptIDDataStore()
                    {
                        Id = t.id,
                        Name = t.name,
                        ParentId = t.parent_id,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    child.Subcategories.AddRange(FindChilds(child.Id));
                    if (child.Subcategories.Count == 0) child.Place = 2;
                    else child.Place = 1;
                    childList.Add(child);
                }
                //tempcat = t;
            }
            return childList;
        }

        //definitions
        private void treeView_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //if (!IsParent) return;
            if (e.Key == Key.Return)
            {
                WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
                if (temp.Place != 2) return;
                Textcategory_id.IsOpen = false;

                CatNameIDPair tempcat = new CatNameIDPair(temp.Name, temp.Id);
                bool flag = true;
                foreach (CatNameIDPair g in catlist)
                {
                    if (g.Have(tempcat))
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    catlist.Add(tempcat);
                    ListBoxCategory_id.ItemsSource = null;
                    ListBoxCategory_id.ItemsSource = catlist;
                }
            }
        }
        private void treeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //if (!IsParent) return;
            try
            {
                WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
                if (temp.Place != 2) return;
                Textcategory_id.IsOpen = false;

                CatNameIDPair tempcat = new CatNameIDPair(temp.Name, temp.Id);
                bool flag = true;
                foreach (CatNameIDPair g in catlist)
                {
                    if (g.Have(tempcat))
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    catlist.Add(tempcat);
                    ListBoxCategory_id.ItemsSource = null;
                    ListBoxCategory_id.ItemsSource = catlist;
                }

            }
            catch (Exception)
            {

                return;
            }
            
        }
        private void Button_Click_PhotoUpload(object sender, RoutedEventArgs e)
        {
            string path = "";
            OpenFileDialog filedialog = new OpenFileDialog();
            filedialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            filedialog.FilterIndex = 1;
            filedialog.Multiselect = false;

            if (filedialog.ShowDialog() == DialogResult.OK)
            {
                path = filedialog.FileName;
            }
            else return;
            Imagepath = path;
            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Uploading Image...";
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += UploadImage;
            worker.RunWorkerCompleted += (o, a) =>
            {
                int pos = Imagepath.LastIndexOf("/") + 1;
                TextPhoto.Text = "http://sklavis.gr/car/" + Imagepath.Substring(pos, Imagepath.Length - pos);


                if (!photolist.Contains(TextPhoto.Text))
                {
                    photolist.Add(TextPhoto.Text);
                    ListBoxPhoto.ItemsSource = null;
                    ListBoxPhoto.ItemsSource = photolist;
                }
                busyIndo.IsBusy = false;
            };
            worker.RunWorkerAsync();

        }
        private void UploadImage(object sender, DoWorkEventArgs e)
        {

            //string path = Imagepath;
            //string filename = System.IO.Path.GetFileName(path);
            //using (WebClient client = new WebClient())
            //{
            //    client.Credentials = new NetworkCredential(Properties.Settings.Default.Username, Properties.Settings.Default.Password);
            //    string FtpImagePath = Properties.Settings.Default.CarImagePath;
            //    FtpImagePath = FtpImagePath.Replace("/", "//");
            //    FtpImagePath = FtpImagePath.Replace("http:////", "ftp://");
            //    //client.UploadFile("ftp://antallaktika-smart.com//wp-content//uploads//Car_gr//" + filename, "STOR", path);
            //    client.UploadFile(FtpImagePath + filename, "STOR", path);
            //}
            //Imagepath = Properties.Settings.Default.CarImagePath + filename;

            string path = Imagepath;
            string filename = System.IO.Path.GetFileName(path);
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(Properties.Settings.Default.SklavisUsername, Properties.Settings.Default.SklavisPassword);
                string FtpImagePath = Properties.Settings.Default.SklavisPartsImagePath;
                FtpImagePath = FtpImagePath.Replace("/", "//");
                FtpImagePath = FtpImagePath.Replace("http:////", "ftp://");
                //client.UploadFile("ftp://antallaktika-smart.com//wp-content//uploads//Car_gr//" + filename, "STOR", path);
                client.UploadFile(FtpImagePath + filename, "STOR", path);
            }
            Imagepath = Properties.Settings.Default.SklavisPartsImagePath + filename;
        }
        private void TextModel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                model.Text = TextModel.SelectedValue.ToString();
            }
            catch (Exception)
            {

                return;
            }

        }
        private void TextMake_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string temp = TextMake.SelectedValue.ToString();
                TextModel.ItemsSource = dts.ReadModelVehicles(Properties.Settings.Default.DBName, temp);
                make.Text = temp;
            }
            catch (Exception)
            {
                return;
            }

        }
        private void loadImage(string url)
        {
            //if (!Properties.Settings.Default.ShowPhotos) return;
            try
            {
                Url = url;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Url, UriKind.Absolute);
                bitmap.EndInit();
                Picturebox.Source = bitmap;
            }
            catch (Exception)
            {
            }
        }

        private void ListBoxPhoto_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string curItem = ListBoxPhoto.SelectedItem.ToString();
                loadImage(curItem);
            }
            catch (Exception)
            {

                return;
            }
        }
        public List<string> GetDateList()
        {
            List<string> datatemp = new List<string>();
            DateTime date;
            date = DateTime.Today;
            for (int i = 1980; i <= date.Year; i++)
            {
                datatemp.Add(i.ToString());
            }
            return datatemp;
        }

        private void Button_Click_CatDelete(object sender, RoutedEventArgs e)
        {
            try
            {
                catlist.Remove((CatNameIDPair)ListBoxCategory_id.SelectedItem);
                ListBoxCategory_id.ItemsSource = null;
                ListBoxCategory_id.ItemsSource = catlist;
            }
            catch (Exception)
            {

                return;
            }
           
        }

        private void Button_Click_PhotoDelete(object sender, RoutedEventArgs e)
        {
            try
            {
                photolist.Remove(ListBoxPhoto.SelectedItem.ToString());
                ListBoxPhoto.ItemsSource = null;
                ListBoxPhoto.ItemsSource = photolist;
            }
            catch (Exception)
            {

                return;
            }            
        }

        private void Button_Click_saveMake(object sender, RoutedEventArgs e)
        {
            VehicleStorage temp = new VehicleStorage
            {
                make_id = make.Text,
                make_name = TextMake.Text,
                model_id = model.Text,
                model_name = TextModel.Text,
                yearfrom = Int32.Parse(TextYearfrom.Text),
                yearto = Int32.Parse(TextYearTo.Text),
                unique_id = ThisUniqueID,
                readable = TextMake.Text + "-" + TextModel.Text + "-" + TextYearfrom.Text + "-" + TextYearTo.Text
            };
            
             bool flag = true;
            foreach (VehicleStorage g in vehiclestorageList)
            {
                if (g.Have(temp))
                {
                    flag = false;
                    break;
                }
            }
            if (flag)
            {
                vehiclestorageList.Add(temp);
                ListBoxMake.ItemsSource = null;
                ListBoxMake.ItemsSource = vehiclestorageList;
                ListBoxMake.DisplayMemberPath = "readable";
                ListBoxMake.SelectedValuePath = "IDStore";
                DropdownButtonMake.IsOpen = false;
            }


        }

        private void Button_Click_deleteMake(object sender, RoutedEventArgs e)
        {
            try
            {
                vehiclestorageList.Remove((VehicleStorage)ListBoxMake.SelectedItem);
                ListBoxMake.ItemsSource = null;
                ListBoxMake.ItemsSource = vehiclestorageList;
            }
            catch (Exception)
            {

                return;
            }
        }

        private void Button_Click_SaveAD(object sender, RoutedEventArgs e)
        {
            try
            {
                CarProduct temp = (CarProduct)TextUnique_id_Ads.SelectedItem;

                //AdTitleIDPair tempad = new AdTitleIDPair(temp.title, temp.unique_id);
                 bool flag = true;
                foreach (CarProduct g in adlist)
                {
                    if (g.unique_id == temp.unique_id)
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    adlist.Add(temp);
                    ListBoxAd.ItemsSource = null;
                    ListBoxAd.ItemsSource = adlist;
                }
            }
            catch (Exception)
            {

                return;
            }
           
        }
        

        private void Button_Click_DeleteAD(object sender, RoutedEventArgs e)
        {
            try
            {
                adlist.Remove((CarProduct)ListBoxAd.SelectedItem);
                ListBoxAd.ItemsSource = null;
                ListBoxAd.ItemsSource = adlist;
            }
            catch (Exception)
            {

                return;
            }
        }

        private void TextTaken_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (TextTaken.Value > TextAmount.Value) TextTaken.Value = TextAmount.Value;
        }

       

        internal void EndProcedure()
        {
            ListBoxAd.ItemsSource = null;
            ListBoxCategory_id.ItemsSource = null;
            ListBoxMake.ItemsSource = null;
            ListBoxMake.DisplayMemberPath = "readable";
            ListBoxMake.SelectedValuePath = "IDStore";
            ListBoxPhoto.ItemsSource = null;
            CheckBoxPublish.IsChecked = true;

            Cars.Clear();
            adlist.Clear();
            makelist.Clear();
            vehiclestorageList.Clear();
            catlist.Clear();
            photolist.Clear();
        }

        private void Button_Click_Update(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckConditions()) return;

                //categories
                //photos
                //make
                // ADid
                ////tempName = caching.CachedCar.category_name.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList();
                //te//mpID = caching.CachedCar.category_id.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList();

                //catlist

                Cars.Clear();
                Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
                VehicleStorages.Clear();
                VehicleStorages.AddRange(dts.ReadVehicleStorage(Properties.Settings.Default.DBName));
                string Cat_names = "";
                string Cat_IDs = "";

                foreach (CatNameIDPair cat in catlist)
                {
                    Cat_names = Cat_names + ",,," + cat.name;
                    Cat_IDs = Cat_IDs + "!#!" + cat.id;
                }

              
                Cat_names = Cat_names.Remove(0, 3);
                Cat_IDs = Cat_IDs.Remove(0, 3);

                List<string> MakeIDStrings = new List<string>();
                List<string> MakeNameStrings = new List<string>();
                List<string> ModelIDStrings = new List<string>();
                List<string> ModelNameStrings = new List<string>();
                int yearFromStrings =0;
                int yearToStrings=0;

                foreach (VehicleStorage v in vehiclestorageList)
                {
                    if (!MakeIDStrings.Contains(v.make_id)) MakeIDStrings.Add(v.make_id);
                    if (!MakeNameStrings.Contains(v.make_name)) MakeNameStrings.Add(v.make_name);
                    if (!ModelIDStrings.Contains(v.model_id)) ModelIDStrings.Add(v.model_id);
                    if (!ModelNameStrings.Contains(v.model_name)) ModelNameStrings.Add(v.model_name);
                    yearFromStrings = v.yearfrom;
                    yearToStrings = v.yearto;
                }

                string adIDStrings = "";


                if (adlist.Count>0)
                {
                    foreach (CarProduct ad in adlist)
                    {
                        adIDStrings = adIDStrings + "!#!" + ad.unique_id;
                    }
                    adIDStrings = adIDStrings.Remove(0, 3);
                }

                CarProduct car = new CarProduct
                {
                    unique_id = ThisUniqueID,
                    category_id = Cat_IDs,
                    category_name = Cat_names,
                    title = TextTitle.Text,
                    description = TextDescription.Text,
                    info = TextInfo.Text,
                    price = TextPrice.Value ?? 0,
                    amount = TextAmount.Value ?? 0,
                    taken = TextTaken.Value ?? 0,
                    color = TextColor.Text ?? "N/A",
                    StoragePlace = StoragePlaceTextBox.Text,
                    photo = String.Join("!#!", photolist),
                    conditions = TextCondition.Text,
                    isParent = "ΟΧΙ",
                    update_interval = 1,
                    make = String.Join(" ", MakeIDStrings),
                    make_name = String.Join(" ", MakeNameStrings),
                    model = String.Join(" ", ModelIDStrings),
                    model_name = String.Join(" ", ModelNameStrings),
                    yearfrom = yearFromStrings,
                    yearto = yearToStrings,
                    AdId = adIDStrings,                    
                    manufacturer_number = "none",
                    aftermarket_number = "none",
                    Public = CheckBoxPublish.IsChecked ?? false
                };

                //if (Combo_Aftermarket_Manufacturer.Text == "Aftermarket" && !String.IsNullOrEmpty(Text_Aftermarket_Manufacturer.Text)) car.aftermarket_number = Text_Aftermarket_Manufacturer.Text;
                //else if (Combo_Aftermarket_Manufacturer.Text == "Manufacturer" && !String.IsNullOrEmpty(Text_Aftermarket_Manufacturer.Text)) car.manufacturer_number = Text_Aftermarket_Manufacturer.Text;

               

                List<CarProduct> lst = new List<CarProduct>();
                lst.Add(car);

                //If exists update it
                if (dts.CheckIfCarProductExists(Properties.Settings.Default.DBName, car.unique_id))
                {
                    dts.UpdateCarPoduct(Properties.Settings.Default.DBName, lst);

                }

                ////Does not exist and is a parent
                //else if (car.isParent == "ΝΑΙ")
                //{
                //    dts.CreateCarProduct(Properties.Settings.Default.DBName, lst);
                //}

                //Does not exist  and is a child
                else
                {
                    //If parent exists
                    //string temp2 = car.unique_id.Split('-')[0];
                    //if (!dts.CheckIfCarProductExists(Properties.Settings.Default.DBName, car.unique_id.Split('-')[0]))
                    //{
                    //    System.Windows.MessageBox.Show("Δεν υπάρχει πατέρας για αυτο το αντικειμενο. Κάτι έχει πάει στραβά!");
                    //    return;
                    //}
                    dts.CreateCarProduct(Properties.Settings.Default.DBName, lst);
                }

                //Create/Update vehicle storages and
                //Remove existing vehicle storages from vehiclestoragelist

                List<VehicleStorage> vlist = new List<VehicleStorage>();
                foreach (VehicleStorage v in VehicleStorages)
                {
                    if (v.unique_id == ThisUniqueID)
                    {
                        vlist.Add(v);
                    }                                  
                }
                dts.DeleteVehicleStorage(Properties.Settings.Default.DBName, vlist);
                dts.CreateVehicleStorage(Properties.Settings.Default.DBName, vehiclestorageList);

                //Update Parent quantity
                //if (!IsParent)
                //{
                //    //Search all items of parent and add the quantities. Add them to parent
                //    List<CarProduct> temp = new List<CarProduct>();
                //    temp.AddRange(dts.ReadCarProductLike(Properties.Settings.Default.DBName, car.unique_id.Split('-')[0]));
                //    int count = 0;
                //    lst.Clear();
                //    foreach (CarProduct tempcar in temp)
                //    {
                //        if (tempcar.isParent == "ΟΧΙ")
                //        {
                //            count += tempcar.amount;
                //        }
                //        if (tempcar.isParent == "ΝΑΙ")
                //        {
                //            lst.Add(tempcar);
                //        }
                //    }
                //    lst[0].amount = count;
                //    dts.UpdateCarPoduct(Properties.Settings.Default.DBName, lst);
                //}
                System.Windows.MessageBox.Show("Αποθηκεύτηκε με Επιτυχία!");
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Παρουσιάστηκε πρόβλημα. Δεν αποθηκεύτηκε το προϊόν.      " + ex.ToString());
            }


        }
        private bool CheckConditions()
        {
            //Open when halndling code
            //if (IsParent)
            //{
            //    if (String.IsNullOrEmpty(TextUnique_id_Parent.Text))
            //    {
            //        //Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε Κωδικό!");

            //        //Autocreate code 
            //        return true;
            //    }
            //}
            //else
            //{
            //    if (String.IsNullOrEmpty(TextUnique_id_Child_Secondary.Text) || String.IsNullOrEmpty(TextUnique_id_Child_Primary.SelectedValue.ToString()))
            //    {
            //        //Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε Κωδικό!");

            //        //Autocreate code 
            //        return true;

            //    }
            //}


            if (String.IsNullOrEmpty(TextTitle.Text)
                || catlist.Count == 0
                  //|| String.IsNullOrEmpty((TextPrice.Value ?? 0).ToString())
                   //|| String.IsNullOrEmpty((TextInterval.Value ?? 0).ToString())
                     //|| String.IsNullOrEmpty(TextPhoto.Text)
                     || String.IsNullOrEmpty(TextCondition.Text)
                      //|| String.IsNullOrEmpty(TextisParent.Text)
                         //|| String.IsNullOrEmpty(TextColor.Text)
                         //|| String.IsNullOrEmpty(TextDescription.Text)
                         || vehiclestorageList.Count == 0
                          //|| String.IsNullOrEmpty(TextModel.SelectedValue.ToString())
                           //|| String.IsNullOrEmpty(TextYearfrom.Text)
                            //|| String.IsNullOrEmpty(TextYearTo.Text)
                             || String.IsNullOrEmpty((TextAmount.Value ?? 0).ToString()))

            {
                Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε όλα τα πεδία. (Κατηγορία , Μάρκα/Μοντέλο , Τίτλο , Απόθεμα)");
                return true;
            }
            else return false;
        }
        private void Button_Click_ΝewProduct(object sender, RoutedEventArgs e)
        {
             MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Θα χάσετε ότι πληροφορίες ΔΕΝ έχετε αποθηκεύσει. Είστε σίγουροι ότι θέλετε να συνεχίσετε;", "ΠΡΟΣΟΧΗ", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                CleanProcedure(NewCode());
            }           
        }
        public string NewCode()
        {
            String ran = "";
            bool flag = true;
            List<CarProduct> tempCars = new List<CarProduct>();
            tempCars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            while (flag)
            {
                Random generator = new Random();
                ran = generator.Next(1000, 9999).ToString("D4");
                flag = tempCars.FindIndex(df => df.unique_id == ran) > 0 ? true : false;
            }
            ThisUniqueID = ran;
            return ran;
        }
        public void CleanProcedure(string id)
        {

            
            busyIndo.IsBusy = true;
            Cars.Clear();
            Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            VehicleStorages.Clear();
            VehicleStorages.AddRange(dts.ReadVehicleStorage(Properties.Settings.Default.DBName));
            LoadComboBoxes();

            //StartClean
            LabelID.Content = "Προϊόν " + id;
            TextTitle.Text = "";
            TextPrice.Value = 0;
            TextAmount.Value = 0;
            TextTaken.Value = 0;
            TextColor.Text = "N/A";
            StoragePlaceTextBox.Text = "";
            TextDescription.Text = "";
            TextInfo.Text = "";
            make.Text = "";
            model.Text = "";
            CheckBoxPublish.IsChecked = true;
            //Textcategory_id.Content = caching.CachedCar.category_name;
            ListBoxCategory_id.ItemsSource = null;
            catlist.Clear();            
            ListBoxCategory_id.DisplayMemberPath = "name";


            photolist.Clear();
            ListBoxPhoto.ItemsSource = null;


            ListBoxAd.ItemsSource = null;
            adlist.Clear();           
            //ListBoxAd.DisplayMemberPath = "title";


            vehiclestorageList.Clear();
            ListBoxMake.ItemsSource = vehiclestorageList;
            ListBoxMake.DisplayMemberPath = "readable";
            ListBoxMake.SelectedValuePath = "IDStore";
            //ListBoxMake.DisplayMemberPath = null;
            //end

            Picturebox.Source = null;
            busyIndo.IsBusy = false;
            IsParent = false;

        }

        private void Button_Click_DeleteProduct(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Το προϊόν αυτο θα διαγραφεί μόνιμα. Είστε σίγουροι ότι θέλετε να συνεχίσετε;", "ΠΡΟΣΟΧΗ", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {

                List<CarProduct> lst = new List<CarProduct>();
                foreach (CarProduct item in Cars)
                {
                    if (item.unique_id == ThisUniqueID) lst.Add(item);
                }
                //delete vehicle storages

                List<VehicleStorage> vlist = new List<VehicleStorage>();
                foreach (VehicleStorage v in dts.ReadVehicleStorage(Properties.Settings.Default.DBName))
                {
                    if (v.unique_id == ThisUniqueID)
                    {
                        vlist.Add(v);
                    }
                }
                dts.DeleteVehicleStorage(Properties.Settings.Default.DBName, vlist);
                dts.DeleteCarPoduct(Properties.Settings.Default.DBName, lst);

                CleanProcedure("");
            }
        }

        private void Button_Click_CarSearch(object sender, RoutedEventArgs e)
        {
            try
            {
                if (catlist.Count==1)
                {
                    CargrSearchCatID = catlist[0].id;
                }
                else CargrSearchCatID = ListBoxCategory_id.SelectedValue.ToString();

                if (vehiclestorageList.Count>1)
                {
                    ChildWindowMake.WindowState = Xceed.Wpf.Toolkit.WindowState.Open;
                    ListBoxMake_Second.ItemsSource = vehiclestorageList;
                    ListBoxMake_Second.DisplayMemberPath = "readable";
                    ListBoxMake_Second.SelectedValuePath = "IDStore";
                }
                else
                {
                    CargrSearchMakeName =  vehiclestorageList[0].make_name;
                    CargrSearchModelName = vehiclestorageList[0].model_name;
                    OpenBrowserCargr();
                }

            }
            catch (Exception)
            {

                return;
            }
        }
        private void Button_Click_CarSearchMake(object sender, RoutedEventArgs e)
        {
            try
            {               
                VehicleStorage temp = (VehicleStorage)ListBoxMake_Second.SelectedItem;

                CargrSearchMakeName = temp.make_name;
                CargrSearchModelName = temp.model_name;
                OpenBrowserCargr();
                ListBoxMake_Second.ItemsSource = null;
                ChildWindowMake.WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;


            }
            catch (Exception)
            {

                return;
            }
        }
        private void OpenBrowserCargr()
        {
            string urlPath = "https://www.car.gr/parts/?sort=pra&ca=" + CargrSearchCatID + "&makes=" + CargrSearchMakeName + "&models=" + CargrSearchModelName;
            System.Diagnostics.Process.Start(urlPath);
        }

        private void ChildWindowMake_Closed(object sender, EventArgs e)
        {
            ListBoxMake_Second.ItemsSource = null;
        }
        private void DataGrid_2_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            TextUnique_id_Ads.Focus();
        }

        private void ListBoxAd_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                CarProduct tempPair;
                tempPair = ((CarProduct)ListBoxAd.SelectedItem);
                //Cars.Clear();
                //Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
                //foreach (CarProduct item in Cars)
                //{
                //    if (item.unique_id == tempPair.id)
                //    {
                        caching.CachedCar = tempPair;
                //        break;
                //    }
                //}
                
                caching.CloseProduct = 1;
            }
            catch (Exception)
            {

                return;
            }
        }
    }

    public class CatNameIDPair
    {
        public string name { get; set; }
        public string id { get; set; }

        public CatNameIDPair(string name, string id)
        {
            this.name = name;
            this.id = id;
        }
        public bool Have(CatNameIDPair temp)
        {
            if (temp.name == name && temp.id == id) return true;
            return false;
        }
    }
    public class AdTitleIDPair
    {
        public string title { get; set; }
        public string id { get; set; }

        public AdTitleIDPair(string title, string id)
        {
            this.title = title;
            this.id = id;
        }
        public bool Have(AdTitleIDPair temp)
        {
            if (temp.title == title && temp.id == id) return true;
            return false;
        }
    }

}
