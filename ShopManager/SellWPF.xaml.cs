﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for SellWPF.xaml
    /// </summary>
    public partial class SellWPF : System.Windows.Controls.UserControl
    {
        List<WorkFlowScriptIDDataStore> lst = new List<WorkFlowScriptIDDataStore>();
        List<categories> data = new List<categories>();
        List<vehicles> vehicles = new List<vehicles>();
        DataAccess dts = new DataAccess();
        string Imagepath = "";
        public SellWPF()
        {
            InitializeComponent();

            busyIndo.IsBusy = true;
        }
        private  void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            busyIndo.IsBusy = true;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += InisializeData;
            worker.RunWorkerCompleted += (o, a) =>
            {
                busyIndo.IsBusy = false;
                UpdateWPF();
            };
            worker.RunWorkerAsync();           
        }
        private void InisializeData(object sender, DoWorkEventArgs e)
        {
            while (caching.VehiclesCached.Count == 0 || caching.CategoriesCached.Count == 0)
            {
                System.Threading.Thread.Sleep(50);
            }
            while (caching.NumberOfCarProducts == 0 && caching.NumberOfCarProducts != 0)
            {                
                System.Threading.Thread.Sleep(50);
            }
            data.Clear();
            data.AddRange(caching.CategoriesCached);            
            vehicles.Clear();
            vehicles.AddRange(caching.VehiclesCached
                                 .GroupBy(item => item.make_id)
                                 .ToDictionary(grp => grp.Key, grp => grp.ToList())
                                 .Select(kvp => kvp.Value[0])
                                 .ToList());

            foreach (categories p in data.ToList())
            {
                //if (t.parent_id ==  "" && t.id=="30")
                if (p.parent_id == "77")
                {
                    WorkFlowScriptIDDataStore parent = new WorkFlowScriptIDDataStore()
                    {
                        Id = p.id,
                        Name = p.name,
                        ParentId = p.parent_id,
                        Place = 0,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    parent.Subcategories.AddRange(FindChilds(parent.Id));
                    lst.Add(parent);
                }
            }

        }
        private void UpdateWPF()
        {
            treeView.ItemsSource = lst;
            TextMake.ItemsSource = vehicles;
            TextMake.SelectedIndex = 23;
            TextModel.SelectedIndex = 2;
            TextInterval.Text = "1";
            TextYearfrom.ItemsSource = GetDateList();
            TextYearfrom.SelectedIndex = 0;
            TextYearTo.ItemsSource = GetDateList();
            TextYearTo.SelectedIndex = GetDateList().Count - 1;
            TextCondition.ItemsSource = new List<string> { "Μεταχειρισμένο", "Καινούριο" , "Ανακατασκευή"};
            TextisParent.ItemsSource = new List<string> { "ΝΑΙ", "ΟΧΙ" };
            TextisParent.SelectedIndex = 0;
            TextCondition.SelectedIndex = 0;
        }
        private void treeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
            if (temp.Place != 2) return;
            Textcategory_id.IsOpen = false;
            Textcategory_id.Content = temp.Name;
            buffer.Text = temp.Id;
            cat.Text = temp.Id;
        }

        private void treeView_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
                if (temp.Place != 2) return;
                Textcategory_id.IsOpen = false;
                Textcategory_id.Content = temp.Name;
                buffer.Text = temp.Id;
                cat.Text = temp.Id;
            }
        }

        public List<WorkFlowScriptIDDataStore> FindChilds(string id)
        {
            List<WorkFlowScriptIDDataStore> childList = new List<WorkFlowScriptIDDataStore>();
            while (data.Count == 0 )
            {
                System.Threading.Thread.Sleep(10);
            }
            //int tempint = data.Count;
            //int count = 0;
            //categories tempcat;
            foreach (categories t in data.ToList())
            {
                //count++;
                if (t is null) continue;
                if (t.parent_id == id)
                {
                    WorkFlowScriptIDDataStore child = new WorkFlowScriptIDDataStore()
                    {
                        Id = t.id,
                        Name = t.name,
                        ParentId = t.parent_id,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    child.Subcategories.AddRange(FindChilds(child.Id));
                    if (child.Subcategories.Count == 0) child.Place = 2;
                    else child.Place = 1;
                    childList.Add(child);
                }
                //tempcat = t;
            }
            return childList;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            TextPrice.Value = Increase10(TextPrice.Value ?? 0);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            TextPrice.Value = Decrease10(TextPrice.Value ?? 0);
         }

        private long Increase10(long value)
        {
            return value + 10;
        }
        private long Decrease10(long value)
        {
          long temp = value - 10;
            if (temp< 0) value = 0;
            else value = temp;
            return value;
        }

        private void TextMake_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            try
            {
                string temp = TextMake.SelectedValue.ToString();
                TextModel.ItemsSource = dts.ReadModelVehicles(Properties.Settings.Default.DBName, temp);
                make.Text = temp;
            }
            catch (Exception)
            {

                return;
            }

        }

        public List<string> GetDateList()
        {
            List<string> datatemp = new List<string>();
            DateTime date;
            date = DateTime.Today;
            for (int i = 1980; i <= date.Year; i++)
            {
                datatemp.Add(i.ToString());
            }
            return datatemp;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string path = "";
            OpenFileDialog filedialog = new OpenFileDialog();
            filedialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            filedialog.FilterIndex = 1;
            filedialog.Multiselect = false;

            if (filedialog.ShowDialog() == DialogResult.OK)
            {
                path = filedialog.FileName;
            }
            else return;
            Imagepath = path;
            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Uploading Image...";
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += UploadImage;
            worker.RunWorkerCompleted += (o, a) =>
            {
                TextPhoto.Text = Imagepath;
                busyIndo.IsBusy = false;                   
            };
            worker.RunWorkerAsync();

        }
        private void UploadImage(object sender, DoWorkEventArgs e)
        {

            string path = Imagepath;
            string filename = System.IO.Path.GetFileName(path);
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(Properties.Settings.Default.Username, Properties.Settings.Default.Password);
                client.UploadFile("ftp://antallaktika-smart.com//wp-content//uploads//Car_gr//" + filename, "STOR", path);
            }
            Imagepath = "http://antallaktika-smart.com/wp-content/uploads/Car_gr/" + filename;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear(){
            UpdateWPF();
            TextUnique_id.Text = "";
            TextTitle.Text = "";
            TextPhoto.Text = "";
            TextPrice.Value = 0;
        }
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckConditions()) return;
                CarProduct car = new CarProduct
                {
                    category_id = buffer.Text,
                    unique_id = TextUnique_id.Text,
                    title = TextTitle.Text,
                    description = TextDescription.Text,
                    price = TextPrice.Value ?? 0,
                    photo = TextPhoto.Text,
                    conditions = TextCondition.Text,
                    isParent = TextisParent.Text,
                    update_interval = TextInterval.Value ?? 0,
                    make = TextMake.SelectedValue.ToString(),
                    model = TextModel.SelectedValue.ToString(),
                    yearfrom = Int32.Parse(TextYearfrom.Text),
                    yearto = Int32.Parse(TextYearTo.Text),
                };
                List<CarProduct> lst = new List<CarProduct>();
                lst.Add(car);
                dts.CreateCarProduct(Properties.Settings.Default.DBName, lst);
               System.Windows.MessageBox.Show("Αποθηκεύτηκε με Επιτυχία!");

            }
            catch (Exception ex)
            {

               System.Windows.MessageBox.Show("Παρουσιάστηκε πρόβλημα. Δεν αποθηκεύτηκε το προϊόν.      " + ex.ToString());
            }
           
            
        }
        private bool CheckConditions()
        {
            if (String.IsNullOrEmpty(cat.Text)
                || String.IsNullOrEmpty(TextTitle.Text)
                 || String.IsNullOrEmpty(TextDescription.Text)
                  || String.IsNullOrEmpty((TextPrice.Value ?? 0).ToString())
                   || String.IsNullOrEmpty(TextPhoto.Text)
                    || String.IsNullOrEmpty(TextCondition.Text)
                     || String.IsNullOrEmpty(TextisParent.Text)
                      || String.IsNullOrEmpty((TextInterval.Value ?? 0).ToString())
                       || String.IsNullOrEmpty(TextMake.SelectedValue.ToString())
                        || String.IsNullOrEmpty(TextModel.SelectedValue.ToString())
                         || String.IsNullOrEmpty(TextYearfrom.Text)
                          || String.IsNullOrEmpty(TextYearTo.Text))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε όλα τα πεδία.");
                return true;
            }
            else return false;
        }
        private void TextModel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                model.Text = TextModel.SelectedValue.ToString();
            }
            catch (Exception)
            {

                return;
            }
           
        }
        
    }
}
    




