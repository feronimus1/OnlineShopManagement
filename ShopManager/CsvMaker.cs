﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;

namespace ShopManager
{
    public class CsvMaker
    {
        public void WriteWooCommerseCsv()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//WooCommerce.csv";
            List<CarProduct> Cars = new List<CarProduct>();
            DataAccess dts = new DataAccess();
            Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            List<CarProduct> Products = new List<CarProduct>();
            Products.AddRange(Cars.Where(c => c.isParent == "ΟΧΙ"));

            List<string> headers = new List<string>()
            {
                //"ID",
                "Type",
                "SKU",
                "Name",
                //"post_excerpt",
                "Is featured?",
                "Visibility in catalog",
                "Short Description",
                "Description",
                "Tax Status",
                "Tax Class",
                "In stock?",
                "Backorders allowed?",
                "Sold individually?",
                "Weight (unit)",
                "Length (unit)",
                "Width (unit)",
                "Height (unit)",
                "Allow customer reviews?",
                //"Purchase Note",
                "Price",
                "Regular Price",
                "Stock",
                "Categories",
                "Tags",
                //"Shipping Class",
                "Images"
            };


            using (TextWriter writer = new StreamWriter(path, false, System.Text.Encoding.UTF8))
            {
                var csv = new CsvWriter(writer);

                foreach (string header in headers)
                {
                    csv.WriteField(header);
                }
                csv.NextRecord();

                List<VehicleStorage> vehiclesStorage = new List<VehicleStorage>();
                vehiclesStorage.AddRange(dts.ReadVehicleStorage(Properties.Settings.Default.DBName));

                foreach (CarProduct c in Products)
                {
                    if (c.AdId == "") continue;
                    if (c.Public == false) continue;



                    //Type
                    csv.WriteField("simple");

                    //SKU
                    csv.WriteField(c.unique_id);

                    //Name
                    csv.WriteField(c.title);

                    //Is featured?
                    csv.WriteField("1");

                    //Visibility in catalog
                    if (c.Public)
                    {
                        csv.WriteField("visible");
                    }
                    else
                    {
                        csv.WriteField("hidden");
                    }

                    //Short Description
                    csv.WriteField(c.title);

                    //Description
                    if (c.description == "")
                    {
                        csv.WriteField(c.title);
                    }
                    else
                    {
                        csv.WriteField(c.description);
                    }

                    //Tax Status
                    csv.WriteField("taxable");

                    //Tax Class
                    csv.WriteField("standard");

                    //In stock?
                    csv.WriteField("1");

                    //Backorders allowed?
                    csv.WriteField("0");

                    //Sold individually?
                    csv.WriteField("1");

                    //Weight (unit)
                    csv.WriteField("");

                    //Length (unit)
                    csv.WriteField("");

                    //Width (unit)
                    csv.WriteField("");

                    //Height (unit)
                    csv.WriteField("");

                    //Allow customer reviews?
                    csv.WriteField("1");

                    ////Purchase Note
                    //csv.WriteField("Ευχαριστούμε για την επιληγή σας.");

                    //Price
                    csv.WriteField(Math.Ceiling(c.price * 1.1));

                    //Regular Price
                    csv.WriteField(c.price);

                    //Stock
                    csv.WriteField(c.amount);

                    //Categories

                    List<string> subcategories = new List<string>();
                    List<string> tags = new List<string>();
                    List<string> categories = new List<string>();
                    foreach (VehicleStorage vehicle in vehiclesStorage.Where(v => v.unique_id == c.unique_id))
                    {
                        if (!subcategories.Contains(vehicle.make_name + " > " + vehicle.model_name + " > "))
                        {
                            subcategories.Add(vehicle.make_name + " > " + vehicle.model_name + " > ");
                        }
                        if (!tags.Contains(vehicle.make_name))
                        {
                            tags.Add(vehicle.make_name);
                        }
                        if (!tags.Contains(vehicle.model_name))
                        {
                            tags.Add(vehicle.model_name);
                        }




                    }
                    tags.AddRange(c.category_name.Split(new string[] { ",,," }, StringSplitOptions.None).ToList());
                    foreach (string item in c.category_id.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList())
                    {
                        
                        string tempPath = caching.CategoriesCached.Where(cat => cat.id == item).ToList()[0].path;
                        tempPath = tempPath.Replace("-> "," > ");
                        foreach (string strcat in subcategories)
                        {
                            categories.Add(strcat + tempPath);
                        }
                    }
                    csv.WriteField(String.Join(",",categories));

                    //Tags
                    csv.WriteField(String.Join(",", tags));

                    ////Shipping Class
                    //csv.WriteField("simple");

                    //Images
                    List<string> photolist = new List<string>();
                    photolist = c.photo.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList();
                    csv.WriteField(String.Join(",", photolist));




                    csv.NextRecord();
                }
            }
           
            //Uploads
            //string ServerPath = Properties.Settings.Default.SklavisPartsImagePath;
            //ServerPath = ServerPath.Replace("/", "//");
            //ServerPath = ServerPath.Replace("http:////", "ftp://");

            //using (WebClient client = new WebClient())
            //{
            //    client.Credentials = new NetworkCredential(Properties.Settings.Default.SklavisUsername, Properties.Settings.Default.SklavisPassword);
            //    client.UploadFile(ServerPath + "//WooCommerce.csv", "STOR", path);
            //}

        }
    }
}
