﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.DataGrid;
using Xceed.Wpf.Toolkit;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for GiveProducts.xaml
    /// </summary>
    public partial class GiveProducts : UserControl
    {
        List<WorkFlowScriptIDDataStore> TreeView_List = new List<WorkFlowScriptIDDataStore>();
        List<categories> CategoriesRaw = new List<categories>();
        List<vehicles> VehiclesRaw = new List<vehicles>();
        List<string> columnstrings = new List<string>();
        List<vehicles> VehiclesMake = new List<vehicles>();
        List<vehicles> VehiclesModel = new List<vehicles>();


        ObservableCollection<vehicles> OVehiclesMake = new ObservableCollection<vehicles>();
        ObservableCollection<vehicles> OVehiclesModel = new ObservableCollection<vehicles>();
        List<CarProduct> CarProductsRaw = new List<CarProduct>();
        //List<CarProduct> carsReadable = new List<CarProduct>();
        List<CarProduct> carsSearch = new List<CarProduct>();
        List<CarProduct> CarsToDelete = new List<CarProduct>();
        DataAccess dt = new DataAccess();
        int MaxPrice = 0;
        int MinPrice = 0;
        int MaxAmount = 0;
        int MinAmount = 0;
        int ComboBox_Make_Index;
        int ComboBox_Model_Index;
        //End  variables
       
        //Start program
        public GiveProducts()
        {
            caching.StartLoopNUmber = 0;
            InitializeComponent();
            busyIndo.IsBusy = true;
            UpdateGrid();
            InitializeComonents();
        }
        /*
        * Reads Car Products remotely 
        * Clears and updates Datagrid
        *  
        */
        private void UpdateGrid()
        {
            CategoriesRaw.Clear();
            VehiclesRaw.Clear();
            CarProductsRaw.Clear();
            CategoriesRaw.AddRange(caching.CategoriesCached);
            VehiclesRaw.AddRange(caching.VehiclesCached);

            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Downloading Data...";

            //after
            DataAccess dts = new DataAccess();
            CarProductsRaw.Clear();
            CarProductsRaw.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            DataGrid_1.Visibility = Visibility.Visible;
            RefreshGrid();
            if (CarProductsRaw.Count == 0)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show("Δεν βρέθηκαν προϊόντα!");
            }
            busyIndo.IsBusy = false;
            //BackgroundWorker worker = new BackgroundWorker();
            //worker.DoWork += ReadCarProductData;
            //worker.RunWorkerCompleted += (o, a) =>
            //{
            //    try
            //    {
            //        DataAccess dts = new DataAccess();
            //        CarProductsRaw.Clear();
            //        CarProductsRaw.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            //        DataGrid_1.Visibility = Visibility.Visible;
            //        DataGrid_1.ItemsSource = null;
            //        DataGrid_1.ItemsSource = CarProductsRaw;
            //        DataGrid_1.Items.Refresh();
            //        //HandleFilteres();
            //        if (CarProductsRaw.Count == 0)
            //        {
            //            Xceed.Wpf.Toolkit.MessageBox.Show("Δεν βρέθηκαν προϊόντα!");
            //        }
            //        busyIndo.IsBusy = false;
            //    }
            //    catch (Exception) { }
            //};
            //worker.RunWorkerAsync();
        }
        /*
        *  Reads Products Data
        */
        private void ReadCarProductData(object sender, DoWorkEventArgs e)
        {
            DataAccess dts = new DataAccess();
            CarProductsRaw.Clear();
            CarProductsRaw.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
        }
        /*
         * Hides dataview 
         * Calls the initialize Data async
         */
        private void InitializeComonents()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += CreateCategories_Parents;
            worker.RunWorkerCompleted += (o, a) =>
            {
                try
                {
                    //treeView.ItemsSource = TreeView_List;
                    VehiclesMake.AddRange(VehiclesRaw.DistinctBy(e => e.make_name).ToList());
                    VehiclesModel.AddRange(VehiclesRaw.DistinctBy(e => e.model_name).ToList());

                    foreach( vehicles v in VehiclesRaw.DistinctBy(e => e.make_name).ToList())
                    {
                        OVehiclesMake.Add(v);
                    }

                    MaxPrice = (int)Math.Ceiling((Double)CarProductsRaw.Max(e => e.price));
                    MinPrice = (int)Math.Ceiling((Double)CarProductsRaw.Min(e => e.price));
                    RangeCarPrice.Maximum = MaxPrice;
                    RangeCarPrice.HigherValue = MaxPrice;
                    RangeCarPrice.Minimum = MinPrice;
                    RangeCarPrice.LowerValue = MinPrice;


                    LabelMaxPrice.Content = MaxPrice;
                    LabelMinPrice.Content = MinPrice;

                    MaxAmount = (int)Math.Ceiling((Double)CarProductsRaw.Max(e => e.amount));
                    MinAmount = (int)Math.Ceiling((Double)CarProductsRaw.Min(e => e.amount));
                    RangeCarAmount.Maximum = MaxAmount;
                    RangeCarAmount.HigherValue = MaxAmount;
                    RangeCarAmount.Minimum = MinAmount;
                    RangeCarAmount.LowerValue = MinAmount;

                    LabelMaxAmount.Content = MaxAmount;
                    LabelMinAmount.Content = MinAmount;

                    //SearchBox_Make.ItemsSource = OVehiclesMake;
                    //ComboBox_Make_Index = SearchBox_Make.SelectedIndex;
                    //SearchBox_Model.ItemsSource = VehiclesModel;
                    //ComboBox_Model_Index = SearchBox_Model.SelectedIndex;
                    //SearchBox_Color.ItemsSource = caching.colors;                    
                    HandleFilteres();


                    busyIndo.IsBusy = false;
                    //test to hide coloumns

                    //System.Collections.IList list = DataGrid_1.Columns;
                    //List<string> temp = new List<string>();
                    //temp.AddRange(Properties.Settings.Default.VisibleAndPositionAndWidth.Split(',').ToArray());
                    //for (int i = 0; i < list.Count; i++)
                    //{
                    //    DataGridTextColumn c = (DataGridTextColumn)list[i];

                    //    for (int j = 0; j < temp.Count; j++)
                    //    {
                    //        if (temp[j] == c.Header.ToString())
                    //        {
                    //            c.Visibility = Visibility.Visible;
                    //            c.DisplayIndex = Int32.Parse(temp[j + 1]);
                    //            c.Width = double.Parse(temp[j + 2]);
                    //            break;
                    //        }
                    //    }
                    //}
                    //end test
                }
                catch (Exception) { InitializeComonents(); }
            };
            worker.RunWorkerAsync();
            
        }
        private void HandleFilteres()
        {
            

            StackCode.Visibility = Visibility.Collapsed;
            if (Properties.Settings.Default.CodeFilter)  StackCode.Visibility = Visibility.Visible;

            StackTitle.Visibility = Visibility.Collapsed;
            if (Properties.Settings.Default.TitleFilter) StackTitle.Visibility = Visibility.Visible;

            StackComment.Visibility = Visibility.Collapsed;
            if (Properties.Settings.Default.CommentFilter) StackComment.Visibility = Visibility.Visible;

            StackCategory.Visibility = Visibility.Collapsed;
            if (Properties.Settings.Default.CategoryFilter) StackCategory.Visibility = Visibility.Visible;

            StackCollor.Visibility = Visibility.Collapsed;
            if (Properties.Settings.Default.CollorFilter) StackCollor.Visibility = Visibility.Visible;

            StackPrice.Visibility = Visibility.Collapsed;
            if (Properties.Settings.Default.PriceFilter) StackPrice.Visibility = Visibility.Visible;

            StackQuantity.Visibility = Visibility.Collapsed;
            if (Properties.Settings.Default.QuantityFilter) StackQuantity.Visibility = Visibility.Visible;

            StackMake.Visibility = Visibility.Collapsed;
            if (Properties.Settings.Default.MakeFilter) StackMake.Visibility = Visibility.Visible;

            StackModel.Visibility = Visibility.Collapsed;
            if (Properties.Settings.Default.ModelFilter) StackModel.Visibility = Visibility.Visible;


            checkBoxPhoto.IsChecked = Properties.Settings.Default.ShowPhotos;
            //checkFilters.IsChecked = Properties.Settings.Default.ShowFilters;
            checkCode.IsChecked = Properties.Settings.Default.CodeFilter;
            checkTitle.IsChecked = Properties.Settings.Default.TitleFilter;
            checkComment.IsChecked = Properties.Settings.Default.CommentFilter;
            checkCategory.IsChecked = Properties.Settings.Default.CategoryFilter;
            checkCollor.IsChecked = Properties.Settings.Default.CollorFilter;
            checPrice.IsChecked = Properties.Settings.Default.PriceFilter;
            checkQuantity.IsChecked = Properties.Settings.Default.QuantityFilter;
            checkMake.IsChecked = Properties.Settings.Default.MakeFilter;
            checkModel.IsChecked = Properties.Settings.Default.ModelFilter;
            CheckboxAd.IsChecked = Properties.Settings.Default.ShowADFilter;
            CheckboxProducts.IsChecked = Properties.Settings.Default.ShowProducts;
            CheckboxNoAds.IsChecked = Properties.Settings.Default.NoAds;
            CheckboxNoProducts.IsChecked = Properties.Settings.Default.NoProducts;
            CheckboxTaken.IsChecked = Properties.Settings.Default.ShowTaken;

            //colors

            Even_Backround_Product.SelectedColor =  (Color)ColorConverter.ConvertFromString(Properties.Settings.Default.EvenBackgroundProduct);
            Even_Foreground_Product.SelectedColor =  (Color)ColorConverter.ConvertFromString(Properties.Settings.Default.EvenFrontgroundProduct);
            Even_Backround_Ad.SelectedColor =  (Color)ColorConverter.ConvertFromString(Properties.Settings.Default.EvenBackgroundAdd);
            Even_Foreground_Ad.SelectedColor =  (Color)ColorConverter.ConvertFromString(Properties.Settings.Default.EvenFrontgroundAd);
            Odd_Backround_Product.SelectedColor = (Color)ColorConverter.ConvertFromString(Properties.Settings.Default.OddBackgroundProduct);
            Odd_Foreground_Product.SelectedColor = (Color)ColorConverter.ConvertFromString(Properties.Settings.Default.OddFrontgroundProduct);
            Odd_Backround_Ad.SelectedColor = (Color)ColorConverter.ConvertFromString(Properties.Settings.Default.OddBackgroundAd);
            Odd_Foreground_Ad.SelectedColor = (Color)ColorConverter.ConvertFromString(Properties.Settings.Default.OddFrontgroundAd);
        }
        /*
         * Takes Categories into CategoriesRaw
         * Takes Vehicles into VehiclesRaw
         * Creates categories Parents          
         * 
         */
        private void CreateCategories_Parents(object sender, DoWorkEventArgs e)
        {    
            foreach (categories p in CategoriesRaw.ToList())
            {
                //if (t.parent_id ==  "" && t.id=="30")
                //If its a car-parts
                if (p.parent_id == "77")
                {
                    WorkFlowScriptIDDataStore parent = new WorkFlowScriptIDDataStore()
                    {
                        Id = p.id,
                        Name = p.name,
                        ParentId = p.parent_id,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    parent.Subcategories.AddRange(FindChilds(parent.Id));
                    if (parent.Subcategories.Count == 0) parent.Place = 2;
                    else parent.Place = 0;
                    TreeView_List.Add(parent);
                }
            }
        }
        /*
        * Creates categories Childs 
        *          
        */
        public List<WorkFlowScriptIDDataStore> FindChilds(string id)
        {
            List<WorkFlowScriptIDDataStore> childList = new List<WorkFlowScriptIDDataStore>();
            while (CategoriesRaw.Count == 0) System.Threading.Thread.Sleep(10);
            foreach (categories t in CategoriesRaw.ToList())
            {
                if (t.parent_id == id)
                {
                    WorkFlowScriptIDDataStore child = new WorkFlowScriptIDDataStore()
                    {
                        Id = t.id,
                        Name = t.name,
                        ParentId = t.parent_id,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    child.Subcategories.AddRange(FindChilds(child.Id));
                    if (child.Subcategories.Count == 0) child.Place = 2;
                    else child.Place = 1;
                    childList.Add(child);
                }
            }
            return childList;
        }

        

        /*
         * Action commands For Filter
         * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         */


        private void TextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            CheckData_Filters();
        }
        //private void treeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    try
        //    {
        //        WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
        //        if (temp.Place != 2) return;
        //        Textcategory_id.IsOpen = false;
        //        Textcategory_id.Content = temp.Name;
        //        CheckData_Filters();
        //    }
        //    catch (Exception) { return; }
            
        //}
        //private void treeView_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        //{
        //    //if ENTER is pressed.
        //    if (e.Key == Key.Return)
        //    {
        //        WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
        //        if (temp.Place != 2) return;
        //        Textcategory_id.IsOpen = false;
        //        Textcategory_id.Content = temp.Name;
        //        CheckData_Filters();
        //    }
        //}
        private void RangeCarPrice_LowerValueChanged(object sender, RoutedEventArgs e)
        {
            MinPrice = (int)Math.Round(RangeCarPrice.LowerValue);
            LabelMinPrice.Content = MinPrice;
            CheckData_Filters();
        }
        private void RangeCarPrice_HigherValueChanged(object sender, RoutedEventArgs e)
        {
            MaxPrice = (int)Math.Round(RangeCarPrice.HigherValue);
            LabelMaxPrice.Content = MaxPrice;
            CheckData_Filters();
        }
        private void RangeCarAmount_LowerValueChanged(object sender, RoutedEventArgs e)
        {
            MinAmount = (int)Math.Round(RangeCarAmount.LowerValue);
            LabelMinAmount.Content = MinAmount;
            CheckData_Filters();
        }
        private void RangeCarAmount_HigherValueChanged(object sender, RoutedEventArgs e)
        {
            MaxAmount = (int)Math.Round(RangeCarAmount.HigherValue);
            LabelMaxAmount.Content = MaxAmount;
            CheckData_Filters();
        }
       


        //private void SearchBox_Make_DropDownClosed(object sender, EventArgs e)
        //{
        //    if (SearchBox_Make.SelectedIndex == -1) return;          
        //    else
        //    {
        //        CheckData_Filters();
        //        SearchBox_Model_FilterforMake();               
        //    }
        //}
        //private void SearchBox_Model_FilterforMake()
        //{
        //    if (SearchBox_Make.SelectedIndex == -1 || SearchBox_Make.Text == "" || SearchBox_Make.Text == null)
        //    {
        //        VehiclesModel.Clear();
        //        VehiclesModel.AddRange(VehiclesRaw.DistinctBy(v => v.model_name).ToList());
        //    }
        //    else
        //    {
        //        VehiclesModel.Clear();
        //        VehiclesModel.AddRange(VehiclesRaw.Where(v => v.make_name == SearchBox_Make.Text));
        //    }
        //    SearchBox_Model.ItemsSource = VehiclesModel; 
        //}
        private void SearchBox_Model_DropDownClosed(object sender, EventArgs e)
        {
            CheckData_Filters();
        }

        private void CheckData_Filters()
        {
            try
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(DataGrid_1.ItemsSource);

                itemsViewOriginal.Filter = ((o) =>
                {                    
                    //UniqueID
                    bool UniqueID = false;
                    if (
                    String.IsNullOrEmpty(SearchBox_Unique_id.Text) ||
                    RemoveSpecialCharacters(((CarProduct)o).unique_id).ToLower().Contains(RemoveSpecialCharacters(SearchBox_Unique_id.Text).ToLower()) ||
                    RemoveSpecialCharacters(((CarProduct)o).AdId).ToLower().Contains(RemoveSpecialCharacters(SearchBox_Unique_id.Text).ToLower())
                    ) UniqueID = true;
                   

                    //Title
                    bool Title = false;
                    if (
                    String.IsNullOrEmpty(SearchBox_Title.Text) ||
                    RemoveSpecialCharacters(((CarProduct)o).title).ToLower().Contains(RemoveSpecialCharacters(SearchBox_Title.Text).ToLower())
                    ) Title = true;

                                       
                    //Description
                    bool Description = false;
                    if (
                    String.IsNullOrEmpty(SearchBox_Description.Text) ||
                    RemoveSpecialCharacters(((CarProduct)o).description).ToLower().Contains(RemoveSpecialCharacters(SearchBox_Description.Text).ToLower())
                    ) Description = true;


                    //Category
                    bool Category = false;
                    if (
                    String.IsNullOrEmpty(Textcategory_id.Text) ||
                    RemoveSpecialCharacters(((CarProduct)o).category_name).ToLower().Contains(RemoveSpecialCharacters(Textcategory_id.Text).ToLower())
                    ) Category = true;


                    //Price
                    bool Price = false;
                    if (((CarProduct)o).price >= RangeCarPrice.LowerValue && ((CarProduct)o).price <= RangeCarPrice.HigherValue) Price = true;


                    //Amount
                    bool Amount = false;
                    if (((CarProduct)o).amount >= RangeCarAmount.LowerValue && ((CarProduct)o).amount <= RangeCarAmount.HigherValue) Amount = true;


                    //Make
                    bool Make = false;
                    if (
                    String.IsNullOrEmpty(SearchBox_Make.Text) ||
                    ((CarProduct)o).make_name.ToLower().Contains(SearchBox_Make.Text.ToLower())
                    ) Make = true;

                                        
                    //Model
                    bool Model = false;
                    if (
                    String.IsNullOrEmpty(SearchBox_Model.Text) ||
                    ((CarProduct)o).model_name.ToLower().Contains(SearchBox_Model.Text.ToLower())
                    ) Model = true;

                                      
                    //Color
                    bool Color = false;
                    if (
                    String.IsNullOrEmpty(SearchBox_Color.Text) ||
                    RemoveSpecialCharacters(((CarProduct)o).color).ToLower().Contains(RemoveSpecialCharacters(SearchBox_Color.Text).ToLower())
                    ) Color = true;


                    //Show Ads
                    bool ShowAdbool = false;
                    if (Properties.Settings.Default.ShowADFilter)
                    {
                        if (((CarProduct)o).isParent == "ΝΑΙ") ShowAdbool = true;
                        else ShowAdbool = true;
                    }
                    else
                    {
                        if (((CarProduct)o).isParent == "ΝΑΙ") ShowAdbool = false;
                        else ShowAdbool = true;
                    }


                    //Show Products
                    bool ShowProductbool = false;
                    if (Properties.Settings.Default.ShowProducts)
                    {
                        if (((CarProduct)o).isParent == "ΟΧΙ") ShowProductbool = true;
                        else ShowProductbool = true;
                    }
                    else
                    {
                        if (((CarProduct)o).isParent == "ΟΧΙ")  ShowProductbool = false; 
                        else ShowProductbool = true;
                    }


                    //Show NoAds
                    bool ShowNoAdsbool = false;
                    if (Properties.Settings.Default.NoAds)
                    {
                        if (((CarProduct)o).AdId == "" && ((CarProduct)o).isParent == "ΟΧΙ") ShowNoAdsbool = true;
                        else ShowNoAdsbool = false;
                    }
                    else ShowNoAdsbool = true;


                    //Show NoProducts
                    bool ShowNoProductsbool = false;
                    if (Properties.Settings.Default.NoProducts)
                    {
                        if (((CarProduct)o).isParent == "ΝΑΙ")
                        {
                            foreach (CarProduct c in CarProductsRaw)
                            {
                                if (c.isParent == "ΝΑΙ") continue;
                                if (c.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList().Contains(((CarProduct)o).unique_id))
                                {
                                    ShowNoProductsbool = false;
                                    break;
                                }
                                else ShowNoProductsbool = true;
                            }
                        }
                        else ShowNoProductsbool = true;
                    }
                    else ShowNoProductsbool = true;


                    //Show Taken
                    bool ShowTakenbool = false;
                    if (Properties.Settings.Default.ShowTaken)
                    {
                        if (((CarProduct)o).taken > 0)
                        {
                            ShowTakenbool = true;
                        }
                        else ShowTakenbool = false;
                    }
                    else ShowTakenbool = true;

                    //Show Taken
                    bool ShowOnlyHidden = false;
                    if (Properties.Settings.Default.ShowOnlyHidden)
                    {
                        if (((CarProduct)o).Public)
                        {
                            ShowOnlyHidden = false;
                        }
                        else ShowOnlyHidden = true;
                    }
                    else ShowOnlyHidden = true;


                    //Check If all
                    if (UniqueID && Title && Description && Category && Price && Amount && Make && Model && ShowAdbool && ShowProductbool && ShowNoAdsbool && ShowNoProductsbool && ShowTakenbool && Color && ShowOnlyHidden) return true;
                    else return false;
                });

                itemsViewOriginal.Refresh();
            }
            catch (Exception)
            {
                return;
            }
        }

        private void DataGrid_1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CarProduct tempcar;
            try
            {
                tempcar = (CarProduct)DataGrid_1.SelectedItem;
               
            }
            catch (Exception)
            {
                return;                
            }
            if (tempcar == null)  return;
            caching.CarsCached.Clear();
            caching.CarsCached.AddRange(CarProductsRaw);
            caching.CachedCar = CarProductsRaw.Where(v => v.unique_id == tempcar.unique_id).ToList()[0];

            if (caching.CachedCar.isParent == "ΝΑΙ")
            {
                LocalAD.LoadProcedure();
                AddAD.WindowState = Xceed.Wpf.Toolkit.WindowState.Open;

                caching.StartLoopNUmber = 1;
                //UpdateGrid();

            }
            else
            {
                LocalProduct.LoadProcedure();
                AddProduct.WindowState = Xceed.Wpf.Toolkit.WindowState.Open;

                caching.StartLoopNUmber = 2;
                //UpdateGrid();    

            }        
        }


        /*
         * Rest Actions follow
         * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         */


        private void Button_Delete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Τα προϊόντα αυτα θα διαγραφούν μόνιμα. Είστε σίγουροι ότι θέλετε να συνεχίσετε;", "ΠΡΟΣΟΧΗ", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                DeleteData();
            }
        }
        private void DeleteData()
        {
            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Deleting Selected Data...";
            CarsToDelete.Clear();
            CarsToDelete.AddRange(DataGrid_1.SelectedItems.Cast<CarProduct>().ToList());
            CarProductsRaw.Clear();
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += deletingProsses;
            worker.RunWorkerCompleted += (o, a) =>
            {
                busyIndo.IsBusy = false;
                UpdateGrid();
            };
            worker.RunWorkerAsync();
        }
        private void deletingProsses(object sender, DoWorkEventArgs e)
        {
            //CarsToDelete
            DataAccess dts = new DataAccess();
            List<VehicleStorage> allvehiclestorages = new List<VehicleStorage>();
            allvehiclestorages.AddRange(dts.ReadVehicleStorage(Properties.Settings.Default.DBName));


            foreach (CarProduct item in CarsToDelete)
            {
                List<VehicleStorage> vlist = new List<VehicleStorage>();
                foreach (VehicleStorage v in allvehiclestorages)
                {
                    if (v.unique_id == item.unique_id)
                    {
                        vlist.Add(v);
                    }
                }
                dts.DeleteVehicleStorage(Properties.Settings.Default.DBName, vlist);
            }
            
            dt.DeleteCarPoduct(Properties.Settings.Default.DBName, CarsToDelete);
        }



      

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowPhotos = true;
            Properties.Settings.Default.Save();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowPhotos = false;
            Properties.Settings.Default.Save();
        }

      

        private void Button_Update_Click(object sender, RoutedEventArgs e)
        {
            UpdateDataset();
            UpdateGrid();
        }
        private void UpdateDataset()
        {
            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Updating Data...";

            //var temp = data1.ItemsSource;
            //cars.Count();
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += UpdateProsses;
            worker.RunWorkerCompleted += (o, a) =>
            {
                busyIndo.IsBusy = false;
                UpdateGrid();
            };
            worker.RunWorkerAsync();
        }
        private void UpdateProsses(object sender, DoWorkEventArgs e)
        {
            //fix cars       
            dt.UpdateCarPoduct(Properties.Settings.Default.DBName, CarProductsRaw);
        }

        private void Button_Click_Product(object sender, RoutedEventArgs e)
        {
           
            //UpdateItemWindow.Show();
             AddProduct.Show();
             LocalProduct.CleanProcedure(LocalProduct.NewCode());
            caching.StartLoopNUmber = 2;

        }
                
        private void DataGrid_1_MouseEnter(object sender, MouseEventArgs e)
        {
            DataGrid_1.Focus();
        }

        //private void DataGrid_1_LostFocus(object sender, RoutedEventArgs e)
        //{
        //    List<string> temp = new List<string>();

        //    System.Collections.IList list = DataGrid_1.Columns;

        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        DataGridTextColumn c = (DataGridTextColumn)list[i];
        //        if (c.Visibility == Visibility.Visible)
        //        {
        //            temp.Add(c.Header.ToString());
        //        }
        //        temp.Add(c.DisplayIndex.ToString());
        //        temp.Add(c.Width.ToString());
        //    }
        //    Properties.Settings.Default.VisibleAndPositionAndWidth = String.Join(",", temp.Select(s => s.ToString()).ToArray());
        //    Properties.Settings.Default.Save();
        //}
        private void DataGrid1_LoadingRow()
        {

            
            //var row = DataGrid.Row;
            //var car = row.DataContext as CarProduct;
            //if (car.unique_id.Count() > 4)
            //{
            //    row.Background = new SolidColorBrush(Colors.Red);
            //}
        }

        private string giveCategoryName(string id)
        {
            foreach (categories cat in caching.CategoriesCached)
            {
                if (cat.id == id)
                {
                    return cat.name;
                }
            }
            return null;
        }
        private string giveMakeName(string id)
        {
            foreach (vehicles c in caching.VehiclesCached)
            {
                if (c.make_id == id)
                {
                    return c.make_name;
                }
            }
            return null;
        }
        private string giveModelName(string id)
        {
            foreach (vehicles c in caching.VehiclesCached)
            {
                if (c.model_id == id)
                {
                    return c.model_name;
                }
            }
            return null;
        }
        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'Α' && c <= 'Ω') || (c >= 'α' && c <= 'ω') || c == '.' || c == '_' || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
                else
                {
                    switch (c)
                    {
                        case 'ά':
                            sb.Append('α');
                            break;
                        case 'έ':
                            sb.Append('ε');
                            break;
                        case 'ί':
                            sb.Append('ι');
                            break;
                        case 'ό':
                            sb.Append('ο');
                            break;
                        case 'ή':
                            sb.Append('η');
                            break;
                        case 'ύ':
                            sb.Append('υ');
                            break;
                        case 'ώ':
                            sb.Append('ω');
                            break;
                        case 'ς':
                            sb.Append('σ');
                            break;
                        //Greeklish Atempt to be

                    }
                }
            }
            return sb.ToString();
        }
        private void CheckBox_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackCollor.Visibility = Visibility.Collapsed;
            StackComment.Visibility = Visibility.Collapsed;
            StackPrice.Visibility = Visibility.Collapsed;
            StackModel.Visibility = Visibility.Collapsed;
            StackMake.Visibility = Visibility.Collapsed;
            StackQuantity.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.ShowFilters = true;
            Properties.Settings.Default.Save();

        }

        private void CheckBox_Filter_Unchecked(object sender, RoutedEventArgs e)
        {

            StackCollor.Visibility = Visibility.Visible;
            StackComment.Visibility = Visibility.Visible;
            StackPrice.Visibility = Visibility.Visible;
            StackModel.Visibility = Visibility.Visible;
            StackMake.Visibility = Visibility.Visible;
            StackQuantity.Visibility = Visibility.Visible;
            Properties.Settings.Default.ShowFilters = false;
            Properties.Settings.Default.Save();
        }
        //CodeFilter
        private void Code_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackCode.Visibility = Visibility.Visible;
            Properties.Settings.Default.CodeFilter = true;
            Properties.Settings.Default.Save();
        }
        private void Code_Filter_Unchecked(object sender, RoutedEventArgs e)
        {
            StackCode.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.CodeFilter = false;
            Properties.Settings.Default.Save();
        }
        //TitleFilter
        private void Title_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackTitle.Visibility = Visibility.Visible;
            Properties.Settings.Default.TitleFilter = true;
            Properties.Settings.Default.Save();
        }
        private void Title_Filter_Unchecked(object sender, RoutedEventArgs e)
        {
            StackTitle.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.TitleFilter = false;
            Properties.Settings.Default.Save();
        }
        //CommentFilter
        private void Comment_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackComment.Visibility = Visibility.Visible;
            Properties.Settings.Default.CommentFilter = true;
            Properties.Settings.Default.Save();
        }
        private void Comment_Filter_Unchecked(object sender, RoutedEventArgs e)
        {
            StackComment.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.CommentFilter = false;
            Properties.Settings.Default.Save();
        }
        //CategoryFilter
        private void Category_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackCategory.Visibility = Visibility.Visible;
            Properties.Settings.Default.CategoryFilter = true;
            Properties.Settings.Default.Save();
        }
        private void Category_Filter_Unchecked(object sender, RoutedEventArgs e)
        {
            StackCategory.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.CategoryFilter = false;
            Properties.Settings.Default.Save();
        }
        //CollorFilter
        private void Collor_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackCollor.Visibility = Visibility.Visible;
            Properties.Settings.Default.CollorFilter = true;
            Properties.Settings.Default.Save();
        }
        private void Collor_Filter_Unchecked(object sender, RoutedEventArgs e)
        {
            StackCollor.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.CollorFilter = false;
            Properties.Settings.Default.Save();
        }
        //PriceFilter
        private void Price_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackPrice.Visibility = Visibility.Visible;
            Properties.Settings.Default.PriceFilter = true;
            Properties.Settings.Default.Save();
        }
        private void Price_Filter_Unchecked(object sender, RoutedEventArgs e)
        {
            StackPrice.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.PriceFilter = false;
            Properties.Settings.Default.Save();
        }
        //QuantityFilter
        private void Quantity_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackQuantity.Visibility = Visibility.Visible;
            Properties.Settings.Default.QuantityFilter = true;
            Properties.Settings.Default.Save();
        }
        private void Quantity_Filter_Unchecked(object sender, RoutedEventArgs e)
        {
            StackQuantity.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.QuantityFilter = false;
            Properties.Settings.Default.Save();
        }
        //MakeFilter
        private void Make_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackMake.Visibility = Visibility.Visible;
            Properties.Settings.Default.MakeFilter = true;
            Properties.Settings.Default.Save();
        }
        private void Make_Filter_Unchecked(object sender, RoutedEventArgs e)
        {
            StackMake.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.MakeFilter = false;
            Properties.Settings.Default.Save();
        }
        //ModelFilter
        private void Model_Filter_Checked(object sender, RoutedEventArgs e)
        {
            StackModel.Visibility = Visibility.Visible;
            Properties.Settings.Default.ModelFilter = true;
            Properties.Settings.Default.Save();
        }
        private void Model_Filter_Unchecked(object sender, RoutedEventArgs e)
        {
            StackModel.Visibility = Visibility.Collapsed;
            Properties.Settings.Default.ModelFilter = false;
            Properties.Settings.Default.Save();
        }

        private void DataGrid1_LoadingRow(object sender, RoutedEventArgs e)
        {

        }

        private void AddProduct_Closed(object sender, EventArgs e)
        {

            if (caching.CloseProduct == 0)
            {
                caching.CloseProduct = 2;
            }
            LocalProduct.EndProcedure();
            UpdateGrid();
            InitializeComonents();
        }

        private void AddAD_Closed(object sender, EventArgs e)
        {
            if (caching.CloseAd == 0)
            {
                caching.CloseAd = 2;
            }
            LocalAD.EndProcedure();
            UpdateGrid();
            InitializeComonents();

        }

        private void Button_Click_add(object sender, RoutedEventArgs e)
        {      
            //UpdateItemWindow.Show();
            AddAD.Show();
            LocalAD.CleanProcedure(LocalAD.NewCode());
            caching.StartLoopNUmber = 1;




        }

        private void CheckboxAd_Checked_change(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowADFilter = true;
            Properties.Settings.Default.Save();
            CheckboxTaken.IsChecked = false;
            CheckboxNoAds.IsChecked = false;
            CheckboxNoProducts.IsChecked = false;
            CheckData_Filters();
        }
        private void CheckboxAd_Unchecked(object sender, RoutedEventArgs e)
        {

            Properties.Settings.Default.ShowADFilter = false;
            Properties.Settings.Default.Save();
            CheckData_Filters();
        }
        private void CheckboxProducts_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowProducts = true;
            Properties.Settings.Default.Save();
            CheckboxTaken.IsChecked = false;
            CheckboxNoAds.IsChecked = false;
            CheckboxNoProducts.IsChecked = false;
            CheckData_Filters();
        }

        private void CheckboxProducts_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowProducts = false;
            Properties.Settings.Default.Save();
            CheckData_Filters();
        }

        private void CheckboxNoAds_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.NoAds = true;
            Properties.Settings.Default.Save();
            CheckboxTaken.IsChecked = false;
            CheckboxNoProducts.IsChecked = false;
            CheckboxProducts.IsChecked = true;
            CheckboxAd.IsChecked = false;
            CheckData_Filters();
        }

        private void CheckboxNoAds_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.NoAds = false;
            Properties.Settings.Default.Save();
            CheckData_Filters();
        }

        private void CheckboxNoProducts_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.NoProducts = true;
            Properties.Settings.Default.Save();
            CheckboxTaken.IsChecked = false;
            CheckboxNoAds.IsChecked = false;
            CheckboxAd.IsChecked = true;
            CheckboxProducts.IsChecked = false;
            CheckData_Filters();
        }

        private void CheckboxNoProducts_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.NoProducts = false;
            Properties.Settings.Default.Save();
            CheckData_Filters();
        }

        private void CheckboxTaken_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowTaken = true;
            Properties.Settings.Default.Save();
            CheckboxNoAds.IsChecked = false;
            CheckboxNoProducts.IsChecked = false;
            CheckboxAd.IsChecked = false;
            CheckboxProducts.IsChecked = true;
            CheckData_Filters();
        }

        private void CheckboxTaken_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowTaken = false;
            Properties.Settings.Default.Save();
            CheckData_Filters();
        }
        private void WaitAdProcess(object sender, DoWorkEventArgs e)
        {
            while (caching.CloseAd == 0)
            {
                System.Threading.Thread.Sleep(100);
            }
            
        }
        private void WaitProductProcess(object sender, DoWorkEventArgs e)
        {
            while (caching.CloseProduct == 0)
            {
                System.Threading.Thread.Sleep(100);
            }
            
        }
        private void LoopAll(object sender, DoWorkEventArgs e)
        {
            bool flag = true;
            while (flag)
            {
                if (caching.StartLoopNUmber == 0) 
                {
                    caching.CloseProduct = 0;
                    caching.CloseAd = 0;
                    Thread.Sleep(100);
                }
                else
                {
                    flag = false;
                }
                 
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoopExecute();
        }  
        private void LoopExecute()
        {
            if (caching.StartLoopNUmber == 0)
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += LoopAll;
                worker.RunWorkerCompleted += (o, a) =>
                {
                    LoopExecute();
                };
                worker.RunWorkerAsync();
            }
            else if (caching.StartLoopNUmber == 1)
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += WaitAdProcess;
                worker.RunWorkerCompleted += (o, a) =>
                {
                    if (caching.CloseAd == 2)
                    {
                        caching.StartLoopNUmber = 0;
                        caching.CloseAd = 0;
                        caching.CloseProduct = 0;
                    }else if (caching.CloseAd == 1)
                    {
                        AddAD.WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
                        AddAD.Close();
                        caching.StartLoopNUmber = 2;
                        caching.CloseProduct = 0;
                        caching.CloseAd = 0;
                        caching.CloseAd = 0;
                        LocalProduct.LoadProcedure();
                        AddProduct.WindowState = Xceed.Wpf.Toolkit.WindowState.Open;
                    }


                    LoopExecute();

                };
                worker.RunWorkerAsync();
            }
            else
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += WaitProductProcess;
                worker.RunWorkerCompleted += (o, a) =>
                {
                    if (caching.CloseProduct == 2)
                    {
                        caching.StartLoopNUmber = 0;
                        caching.CloseAd = 0;
                        caching.CloseProduct = 0;
                    } else if (caching.CloseProduct == 1)
                    {
                        AddProduct.WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
                        AddProduct.Close();
                        caching.StartLoopNUmber = 1;
                        caching.CloseProduct = 0;
                        caching.CloseAd = 0;
                        LocalAD.LoadProcedure();
                        AddAD.WindowState = Xceed.Wpf.Toolkit.WindowState.Open;
                    }


                    LoopExecute();
                };
                worker.RunWorkerAsync();
            }
        }

        private void DataGrid_1_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            // Get the DataRow corresponding to the DataGridRow that is loading.
            var car = (CarProduct)e.Row.DataContext;
            var bc = new BrushConverter();
            string colordfd = "#4286f4";
            if (car.isParent == "ΝΑΙ" && e.Row.AlternationIndex == 1)
            {
                //odd ad
                e.Row.Background = (Brush)bc.ConvertFrom(Properties.Settings.Default.OddBackgroundAd);
                e.Row.Foreground = (Brush)bc.ConvertFrom(Properties.Settings.Default.OddFrontgroundAd);
                e.Row.FontStyle = FontStyles.Italic;
                return;
            }
            if (car.isParent == "ΝΑΙ" && e.Row.AlternationIndex == 0)
            {
                //even ad
                e.Row.Background = (Brush)bc.ConvertFrom(Properties.Settings.Default.EvenBackgroundAdd);
                e.Row.Foreground = (Brush)bc.ConvertFrom(Properties.Settings.Default.EvenFrontgroundAd);
                e.Row.FontStyle = FontStyles.Italic;
                return;
            }
            if (car.isParent == "ΟΧΙ" && e.Row.AlternationIndex == 1)
            {
                //odd product
                e.Row.Background = (Brush)bc.ConvertFrom(Properties.Settings.Default.OddBackgroundProduct);
                e.Row.Foreground = (Brush)bc.ConvertFrom(Properties.Settings.Default.OddFrontgroundProduct);
                return;
            }
            if (car.isParent == "ΟΧΙ" && e.Row.AlternationIndex == 0)
            {
                //even product
                e.Row.Background = (Brush)bc.ConvertFrom(Properties.Settings.Default.EvenBackgroundProduct);
                e.Row.Foreground = (Brush)bc.ConvertFrom(Properties.Settings.Default.EvenFrontgroundProduct);
                return;
            }
        }

        private void ColorPicker_SelectedColorChanged_Odd_Background_Ad(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {           
            Properties.Settings.Default.OddBackgroundAd = Odd_Backround_Ad.SelectedColor.ToString();
            RefreshGrid();
        }

        private void Odd_Foreground_Ad_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Properties.Settings.Default.OddFrontgroundAd = Odd_Foreground_Ad.SelectedColor.ToString();
            RefreshGrid();
        }

        private void Even_Backround_Ad_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Properties.Settings.Default.EvenBackgroundAdd = Even_Backround_Ad.SelectedColor.ToString();
            RefreshGrid();
        }

        private void Even_Foreground_Ad_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Properties.Settings.Default.EvenFrontgroundAd = Even_Foreground_Ad.SelectedColor.ToString();
            RefreshGrid();
        }

        private void Odd_Backround_Product_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Properties.Settings.Default.OddBackgroundProduct = Odd_Backround_Product.SelectedColor.ToString();
            RefreshGrid();
        }

        private void Odd_Foreground_Product_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Properties.Settings.Default.OddFrontgroundProduct = Odd_Foreground_Product.SelectedColor.ToString();
            RefreshGrid();
        }

        private void Even_Backround_Product_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Properties.Settings.Default.EvenBackgroundProduct = Even_Backround_Product.SelectedColor.ToString();
            RefreshGrid();
        }

        private void Even_Foreground_Product_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Properties.Settings.Default.EvenFrontgroundProduct = Even_Foreground_Product.SelectedColor.ToString();
            RefreshGrid();
        }
        private void RefreshGrid()
        {
            DataGrid_1.ItemsSource = null;
            DataGrid_1.ItemsSource = CarProductsRaw;
            DataGrid_1.Items.Refresh();
            CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(DataGrid_1.ItemsSource);
            itemsViewOriginal.Refresh();
        }

        private void CheckboxHidden_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowOnlyHidden = true;
            Properties.Settings.Default.Save();
            CheckData_Filters();
        }

        private void CheckboxHidden_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowOnlyHidden = false;
            Properties.Settings.Default.Save();
            CheckData_Filters();
        }
    }
}

