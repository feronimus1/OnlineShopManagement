﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace ShopManager
{
    [Table("vehicles")]
    public class vehicles
    {
        [ExplicitKey]
        public string model_id { get; set; }
        public string model_name { get; set; }
        public string make_id { get; set; }
        public string make_name { get; set; }

        public static vehicles FromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            vehicles tempvehicle = new vehicles();
            tempvehicle.model_id = values[0].Trim('"');
            tempvehicle.model_name = values[1].Trim('"');
            tempvehicle.make_id = values[2].Trim('"');
            tempvehicle.make_name = values[3].Trim('"');
            return tempvehicle;
        }

        
    }
}
