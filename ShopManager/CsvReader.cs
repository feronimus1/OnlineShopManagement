﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace ShopManager
{
    class CsvReader
    {
        public void ReadVehicles()
        {
            try
            {

                string path = "";
                OpenFileDialog filedialog = new OpenFileDialog();
                filedialog.Filter = "CSV Files (*.csv)|*.csv";
                filedialog.FilterIndex = 1;
                filedialog.Multiselect = false;

                if (filedialog.ShowDialog() == DialogResult.OK)
                {
                    path = filedialog.FileName;
                }
                else return;


                List<vehicles> values = File.ReadAllLines(path)
                                              .Skip(1)
                                              .Select(v => vehicles.FromCsv(v))
                                              .ToList();
                DataAccess dtc = new DataAccess();
                SQLCreation cre = new SQLCreation();
                dtc.SQLCommand(Properties.Settings.Default.DBName, cre.vehicles());
                dtc.CreateBulckVehicles(Properties.Settings.Default.DBName, path);

            }
            catch  (Exception e)
            { 
                MessageBox.Show(@"There was an error. Probably you tried to read a wrong csv file."+ Environment.NewLine + Environment.NewLine + "Error: " +e.ToString());                
            }     
            
        }

        public void ReadCategories()
        {
            try
            {

                string path = "";
                OpenFileDialog filedialog = new OpenFileDialog();
                filedialog.Filter = "CSV Files (*.csv)|*.csv";
                filedialog.FilterIndex = 1;
                filedialog.Multiselect = false;

                if (filedialog.ShowDialog() == DialogResult.OK)
                {
                    path = filedialog.FileName;
                }
                else return;


                List<categories> values = File.ReadAllLines(path)
                                              .Skip(1)
                                              .Select(v => categories.FromCsv(v))
                                              .ToList();

                
                DataAccess dtc = new DataAccess();
                SQLCreation cre = new SQLCreation();
                dtc.SQLCommand(Properties.Settings.Default.DBName, cre.categories());
                dtc.CreateBulckCategories(Properties.Settings.Default.DBName, path);

            }
            catch (Exception e)
            {
                MessageBox.Show(@"There was an error. Probably you tried to read a wrong csv file." + Environment.NewLine + Environment.NewLine + "Error: " + e.ToString());
            }
        }
    }
}
