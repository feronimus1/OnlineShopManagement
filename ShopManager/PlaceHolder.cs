﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopManager
{
    public partial class PlaceHolder: Form
    {

        public PlaceHolder()
        {
            InitializeComponent();
            elementHost1.Child = new CheckOut();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
