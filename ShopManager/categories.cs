﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace ShopManager
{
    [Table("categories")]
    public class categories
    {

        [ExplicitKey]
        public string id { get; set; }
        public string parent_id { get; set; }
        public string name { get; set; }
        public string path { get; set; }
        public string has_make { get; set; }

        public static categories FromCsv(string csvLine)
        {
            string[] split = new string[] { "\",\"" };
            string[] values = csvLine.Split(split, StringSplitOptions.None);
            categories tempcategorie = new categories();
            tempcategorie.id = values[0].Trim('"');
            tempcategorie.parent_id = values[1].Trim('"');
            tempcategorie.name = values[2].Trim('"');
            tempcategorie.path = values[3].Trim('"');
            tempcategorie.has_make = values[4].Trim('"');
            return tempcategorie;
        }

    }
}
