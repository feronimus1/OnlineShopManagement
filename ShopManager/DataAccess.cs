﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Windows;
using System.Data;
using MySql.Data.MySqlClient;
using Dapper.Contrib.Extensions;

using MySql.Data;
namespace ShopManager
{
    public class DataAccess
    {
        public void SQLCommand(string connectionstring, string sqlCommand)
        {
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    mycon.Execute(sqlCommand);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("SQLCommand    " + sqlCommand + "          " + ex.ToString());
            }
        }
        public  string LoginAuth(string connectionstring, string sqlCommand)
        {
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    mycon.Open();
                    MySqlCommand command = new MySqlCommand(sqlCommand, mycon);
                    string result;
                    try
                    {
                        result = command.ExecuteScalar().ToString();
                    }
                    catch
                    {
                        result = "0";
                    }
                    mycon.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("SQLCommand    " + sqlCommand + "          " + ex.ToString());
                return null;
            }
        }        

        public void CreateCarProduct(string connectionstring, List<CarProduct> car)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    //connection.Execute(@"INSERT INTO CarProduct 
                    //                (unique_id,title,description,category_id,price,photo,conditions,update_interval,make,model,yearfrom,yearto)  
                    //                VALUES
                    //                (@unique_id,@title,@description,@category_id,@price,@photo,@conditions,@update_interval,@make,@model,@yearfrom,@yearto)"
                    //              , car);
                    connection.Insert(car);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CreateCarProduct    " + ex.ToString());
            }
        }
        public List<CarProduct> ReadCarProduct(string connectionstring)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    //return connection.Query<CarProduct>($"SELECT * FROM CarProduct").ToList();
                    return connection.GetAll<CarProduct>().ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadCarProduct    " + ex.ToString());
                return null;
            }
        }
        public List<CarProduct> ReadCarProductLike(string connectionstring,string LikeID)
        {
            try
            {
                string sql = $"SELECT * FROM CarProduct WHERE unique_id LIKE '%{LikeID}%' ;";
                using (var connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    return connection.Query<CarProduct>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadModelVehicles    " + ex.ToString());
                return null;
            }
        }
        public void UpdateCarPoduct(string connectionstring, List<CarProduct> car)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    connection.Update(car);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("UpdateCarPoduct    " + ex.ToString());
            }
        }
        public void DeleteCarPoduct(string connectionstring, List<CarProduct> car)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    connection.Delete(car);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("DeleteCarPoduct    " + ex.ToString());
            }
        }            

        public void CreateBulckVehicles(string connectionstring, string path)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {

                    //using (StreamWriter writer = new StreamWriter(path))
                    //{
                    //    Rfc4180Writer.WriteDataTable(rawData, writer, false);
                    //}
                    var msbl = new MySqlBulkLoader(connection);
                    msbl.TableName = "vehicles";
                    msbl.FileName = path;
                    msbl.FieldTerminator = ",";
                    msbl.LineTerminator = "\r\n";
                    msbl.NumberOfLinesToSkip = 1;
                    msbl.FieldQuotationCharacter = '"';
                    msbl.Load();
                    // System.IO.File.Delete(tempCsvFileSpec);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CreateCarProduct    " + ex.ToString());
            }
        }
        public void CreateBulckCategories(string connectionstring, string path)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {

                    //using (StreamWriter writer = new StreamWriter(path))
                    //{
                    //    Rfc4180Writer.WriteDataTable(rawData, writer, false);
                    //}
                    var msbl = new MySqlBulkLoader(connection);
                    msbl.TableName = "categories";
                    msbl.FileName = path;
                    msbl.FieldTerminator = ",";
                    msbl.LineTerminator = "\r\n";
                    msbl.NumberOfLinesToSkip = 1;
                    msbl.FieldQuotationCharacter = '"';
                    msbl.Load();
                   // System.IO.File.Delete(tempCsvFileSpec);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CreateCarProduct    " + ex.ToString());
            }
        }

        public vehicles ReadModelVehicle(string connectionstring,string id)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {                   
                    return connection.Get<vehicles>(id);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadModelVehicle    " + ex.ToString());
                return null;
            }
        }
        public List<vehicles> ReadModelVehicles(string connectionstring, string id)
        {
            try
            {
                string sql = $"SELECT * FROM vehicles WHERE make_id = {id};";
                using (var connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    return connection.Query<vehicles>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadModelVehicles    " + ex.ToString());
                return null;
            }
        }

        public List<vehicles> ReadVehicles(string connectionstring)
        {
            try
            {
                string sql = $"SELECT * FROM vehicles;";
                using (var connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    return connection.Query<vehicles>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadVehicles    " + ex.ToString());
                return null;
            }
        }



        public List<vehicles> ReadMakeUniqueNamesVehicle(string connectionstring)
        {
            try
            {
                string sql = $"SELECT DISTINCT make_name , make_id  FROM vehicles ;";
                using (var connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    return connection.Query<vehicles>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadMakeUniqueNamesVehicle    " + ex.ToString());
                return null;
            }
        }

        public List<categories> ReadCategories(string connectionstring)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {                   
                    return connection.GetAll<categories>().ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadCategories    " + ex.ToString());
                return null;
            }
        }

        public int GetNumberOfCarProducts(string connectionstring)
        {
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    mycon.Open();
                    string cmdString = "SELECT COUNT(*) FROM CarProduct;";
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandText = cmdString;
                    cmd.Connection = mycon;
                    int temp = Convert.ToInt32(cmd.ExecuteScalar());
                    mycon.Close();
                    return temp;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("GetNumberOfCarProducts    " + ex.ToString());
                return -2;
            }
        }
        public bool CheckIfCarProductExists(string connectionstring, string Unique_id)
        {
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    mycon.Open();
                    string cmdString = $"SELECT COUNT(*) FROM CarProduct WHERE unique_id = '{Unique_id}' ;";
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandText = cmdString;
                    cmd.Connection = mycon;
                    int temp = Convert.ToInt32(cmd.ExecuteScalar());
                    mycon.Close();

                    if (temp <1) return false;
                    else return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CheckIfCarProductExists    " + ex.ToString());
                return false;
            }
        }
        public void CreateVehicleStorage(string connectionstring, List<VehicleStorage> vehicleStorageList)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {                    
                    connection.Insert(vehicleStorageList);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CreateVehicleStorage    " + ex.ToString());
            }
        }
        public List<VehicleStorage> ReadVehicleStorageID(string connectionstring, string id)
        {
            try
            {
                string sql = $"SELECT * FROM VehicleStorage WHERE unique_id = '{id}';";
                using (var connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    return connection.Query<VehicleStorage>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadVehicleStorageID    " + ex.ToString());
                return null;
            }
        }
        public List<VehicleStorage> ReadVehicleStorage(string connectionstring)
        {
            try
            {
                string sql = $"SELECT * FROM VehicleStorage;";
                using (var connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    return connection.Query<VehicleStorage>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadVehicleStorage    " + ex.ToString());
                return null;
            }
        }
        public void DeleteVehicleStorage(string connectionstring, List<VehicleStorage> vehicleStorageList)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    connection.Delete(vehicleStorageList);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("DeleteVehicleStorage    " + ex.ToString());
            }
        }
        public void CreateInfo(string connectionstring, List<Info> infolist)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    connection.Insert(infolist);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CreateInfo    " + ex.ToString());
            }
        }
        public void UpdateInfo(string connectionstring, List<Info> infolist)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    connection.Update(infolist);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("UpdateInfo    " + ex.ToString());
            }
        }
        public List<Info> ReadInfo(string connectionstring)
        {
            try
            {
                string sql = $"SELECT * FROM Info;";
                using (var connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    return connection.Query<Info>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadInfo    " + ex.ToString());
                return null;
            }
        }
        public List<User> ReadUsers(string connectionstring)
        {
            try
            {
                string sql = $"SELECT * FROM User;";
                using (var connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    return connection.Query<User>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ReadUsers    " + ex.ToString());
                return null;
            }
        }
        public void UpdateUsers(string connectionstring, List<User> infolist)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    connection.Update(infolist);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("UpdateUsers    " + ex.ToString());
            }
        }
        public void CreateUsers(string connectionstring, List<User> infolist)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    connection.Insert(infolist);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CreateUsers    " + ex.ToString());
            }
        }
        public void DeleteUser(string connectionstring, List<User> infolist)
        {
            try
            {
                using (IDbConnection connection = new MySqlConnection(Helper.ReadCnn(connectionstring)))
                {
                    connection.Delete(infolist);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("DeleteUser    " + ex.ToString());
            }
        }

    }
}
