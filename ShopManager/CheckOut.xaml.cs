﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for CheckOut.xaml
    /// </summary>
    public partial class CheckOut : UserControl
    {
        public ObservableCollection<OrderItem> items { get; set; }
        public CheckOut()
        {
            items = new ObservableCollection<OrderItem>();
            items.Add(new OrderItem() { Name = "Item1", Price = 10, Tax = 24, Amount = 1, Discount = 0 });
            items.Add(new OrderItem() { Name = "Item2", Price = 20, Tax = 24, Amount = 1, Discount = 0 });
            InitializeComponent();
            items.Add(new OrderItem() { Name = "Item1", Price = 10, Tax = 24, Amount = 1, Discount = 0 });
            items.Add(new OrderItem() { Name = "Item2", Price = 20, Tax = 24, Amount = 1, Discount = 0 });
            LoadProcedure();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
           // DataGrid_order.ItemsSource = caching.CarsOrder;
            busyIndo.IsBusy = false;
            items.Add(new OrderItem() { Name = "Item1", Price = 10, Tax = 24, Amount = 1, Discount = 0 });
            items.Add(new OrderItem() { Name = "Item2", Price = 20, Tax = 24, Amount = 1, Discount = 0 });
        }
        public void LoadProcedure()
        {
            //DataGrid_order.ItemsSource = null;
            //DataGrid_order.ItemsSource = caching.CarsOrder;
           

            items.Add(new OrderItem() { Name = "Item1", Price = 10, Tax = 24, Amount = 1, Discount = 0 });
            items.Add(new OrderItem() { Name = "Item2",  Price = 20, Tax = 24, Amount = 1, Discount = 0 });
            DataGrid_order.ItemsSource = items;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string temp = "";
            int i = 0;
            foreach (OrderItem item in items)
            {
                temp += i.ToString() + item.Amount.ToString() + "     ";
            }
            MessageBox.Show(temp);

            //DataGrid_order.ItemsSource = null;
            //DataGrid_order.ItemsSource = items;
        }
    }
}
