﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
using System.Windows.Forms;

namespace ShopManager
{
    public static class Helper
    {
        public static string ReadCnn(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString ;
        }
        public static void UpdateCnn(string name,string cnn)
        {
            ConfigurationManager.ConnectionStrings[name].ConnectionString = cnn;
            ConfigurationManager.RefreshSection("connectionStrings");
        }
        public static void CreateCnn(string conStringname, string conString, string providerName)
        {
            //RemoveReadOnly();            

            //conStringname = "test";
            //conString = @"data source=.\SQLEXPRESS;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|aspnetdb.mdf;User Instance=true";
            //providerName = "System.Data.SqlClient";           

            //ConfigurationManager.ConnectionStrings.Add(new ConnectionStringSettings(conStringname, conString, providerName));
            //AddReadOnly();

            //-------------------------------------

             conStringname = "test";
            conString = @"data source=.\SQLEXPRESS;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|aspnetdb.mdf;User Instance=true";
            providerName = "System.Data.SqlClient";

            ConnectionStringSettings connStrSettings = new ConnectionStringSettings();

            connStrSettings.Name = conStringname;
            connStrSettings.ConnectionString = conString;
            connStrSettings.ProviderName = providerName;
            CreateConfigurationFile(connStrSettings);
            Application.Restart();
        }
//        public static void Create2Cnn(string name, string connectionstring)
//        {
//            Configuration configuration;
//            ConnectionStringSettings connectionStringSettings = new ConnectionStringSettings(name, connectionstring);
//{
//                // You cannot add to ConfigurationManager.ConnectionStrings using
//                // ConfigurationManager.ConnectionStrings.Add
//                // (connectionStringSettings) -- This fails.

//                // But you can add to the configuration section and refresh the ConfigurationManager.

//                // Get the connection strings section; Even if it is in another file.
//                ConnectionStringsSection connectionStringsSection = configuration.ConnectionStrings;

//                // Add the new element to the section.
//                connectionStringsSection.ConnectionStrings.Add(connectionStringSettings);

//                // Save the configuration file.
//                configuration.Save(ConfigurationSaveMode.Minimal);

//                // This is needed. Otherwise the connection string does not show up in
//                // ConfigurationManager
//                ConfigurationManager.RefreshSection("connectionStrings");
//            }
//        }
        public static void DeleteCnn(string name)
        {
            RemoveReadOnly();
            ConfigurationManager.ConnectionStrings.Remove(name);
            AddReadOnly();
        }
        public static List<string> GetAllCnnNames()
        {
            return ConfigurationManager.ConnectionStrings
                                       .Cast<ConnectionStringSettings>()
                                       .Select(v => v.Name)
                                       .ToList();
        }
        private static void RemoveReadOnly()
        {
            typeof(ConfigurationElementCollection)
                    .GetField("bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(ConfigurationManager.ConnectionStrings, false);
            //ConfigurationManager.ConnectionStrings.Add(new ConnectionStringSettings());
        }
        private static void AddReadOnly()
        {
            typeof(ConfigurationElementCollection)
                    .GetField("bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(ConfigurationManager.ConnectionStrings, true);
            //ConfigurationManager.ConnectionStrings.Add(new ConnectionStringSettings());
        }

        private static void CreateConfigurationFile(ConnectionStringSettings connStrSettings)
        {
            try
            {

                // Create a custom configuration section.
                ConnectionStringsSection customSection = new ConnectionStringsSection();

                // Get the current configuration file.
                System.Configuration.Configuration config =
                        ConfigurationManager.OpenExeConfiguration(
                        ConfigurationUserLevel.None);

                // Create the custom section entry  
                // in <configSections> group and the 
                // related target section in <configuration>.
                if (config.Sections["ConnectionStringsSection"] == null)
                {
                    config.Sections.Add("ConnectionStringsSection", customSection);
                }

               

                config.ConnectionStrings.ConnectionStrings.Add(connStrSettings);

                // Add an entry to appSettings section.
                int appStgCnt =
                    ConfigurationManager.AppSettings.Count;
                string newKey = "NewKey" + appStgCnt.ToString();

                string newValue = DateTime.Now.ToLongDateString() +
                  " " + DateTime.Now.ToLongTimeString();

                config.AppSettings.Settings.Add(newKey, newValue);

                // Save the configuration file.
                customSection.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Full);

                System.Windows.MessageBox.Show("Created configuration file: {0}"+ config.FilePath, config.FilePath);

            }
            catch (ConfigurationErrorsException err)
            {
                System.Windows.MessageBox.Show("NOT CreateConfigurationFile: {0}" + err.ToString(), err.ToString());
            }

        }
    }
}