﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Xceed.Wpf.Toolkit;

namespace ShopManager
{
    public class XMLWrite
    {
        DataAccess dts = new DataAccess();
        List<CarProduct> Cars = new List<CarProduct>();
        public void WriteXML()
        {
            //Update stamp
            DataAccess dts = new DataAccess();
            Properties.Settings.Default.stamp = dts.ReadInfo(Properties.Settings.Default.DBName).Where(c => c.id == 1).ToList()[0].MainInfo;

            List<CarProduct> Adcars = new List<CarProduct>();
            Cars.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
            Adcars.AddRange(Cars.Where(c => c.isParent == "ΝΑΙ"));

            void WriteBookData(XmlWriter writer, CarProduct product, List<VehicleStorage> makeHolder, List<string> photos , List<string> categories)
            {
                writer.WriteStartElement("classified");
                writer.WriteElementString("unique_id", product.unique_id);
                writer.WriteElementString("title", product.title + " ΚΩΔΙΚΟΣ[" + product.unique_id + "]");
                writer.WriteElementString("description", product.description);

                foreach (string cat in categories)
                {
                    writer.WriteElementString("category_id", cat);
                }       

                writer.WriteElementString("price", product.price.ToString());

                writer.WriteStartElement("makemodels");
                //
                foreach (VehicleStorage v in makeHolder)
                {
                    writer.WriteStartElement("makemodel");
                    writer.WriteElementString("make", v.make_name);
                    writer.WriteElementString("model", v.model_name);
                    writer.WriteElementString("yearfrom", v.yearfrom.ToString());
                    writer.WriteElementString("yearto", v.yearto.ToString());
                    writer.WriteEndElement();
                }                               
                writer.WriteEndElement();

                writer.WriteStartElement("photos");
                foreach (string p in photos)
                {
                    writer.WriteElementString("photo", p);
                }
                writer.WriteEndElement();

                writer.WriteElementString("condition", product.conditions);
                writer.WriteElementString("update_interval", product.update_interval.ToString());
                writer.WriteEndElement();
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//CarGR.xml";
            using (XmlWriter writer = XmlWriter.Create(path, settings))
            {
                writer.WriteStartDocument();



                writer.WriteStartElement("cardealer");
                writer.WriteElementString("lastupdate", DateTime.UtcNow.ToString("o"));
                writer.WriteStartElement("classifieds");



                //Start products
                foreach (CarProduct Ad in Adcars)
                {

                    if (!Ad.Public) continue;
                    List<CarProduct> products1 = new List<CarProduct>();
                    List<CarProduct> products = new List<CarProduct>();
                    products1.AddRange(Cars.Where(c => c.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList().Contains(Ad.unique_id)));
                    products.AddRange(products1.Where(c => c.amount > 0));


                    if (products.Count == 0) continue;

                    //if (Ad.amount <= 0) continue;


                    //List<CarProduct> list = new List<CarProduct>();
                    List<string> photos = new List<string>();
                    List<string> categories = new List<string>();
                    List<string> colors = new List<string>();
                    List<VehicleStorage> vehiclestorageList = new List<VehicleStorage>();
                    //list.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName).Where(c => c.unique_id.Contains(Ad.unique_id) && c.unique_id != Ad.unique_id));

                    foreach (CarProduct c in products)
                    {
                        //makeHolder.addExtra(c.make_name, c.model_name, c.yearfrom, c.yearto);                       
                        //if (!colors.Contains(c.color) && c.color != caching.EmptyColor) colors.Add(c.color);
                        //

                        if (c.amount <= 0) continue;

                        //Category
                        List<string> tempID = new List<string>();
                        tempID = c.category_id.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList();
                        for (int i = 0; i < tempID.Count; i++)
                        {
                            if (tempID[i].Length == 0) continue;

                            string tempcat = tempID[i];
                            bool flag = true;

                            foreach (string g in categories)
                            {
                                if (g == tempcat)
                                {
                                    flag = false;
                                    break;
                                }
                            }
                            if (flag)
                            {
                                categories.Add(tempcat);
                            }
                        }
                        //endcat

                        //startPhoto
                        foreach (string photo in c.photo.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList())
                        {
                            if (photos.Contains(photo)) continue;
                            if (photo == "") continue;
                            photos.Add(photo);
                        }
                        //end photo

                        //start colors
                        if (!colors.Contains(c.color) && c.color != caching.EmptyColor) colors.Add(c.color);
                        //end colors

                        //start makes

                        foreach (VehicleStorage temp in dts.ReadVehicleStorageID(Properties.Settings.Default.DBName, c.unique_id))
                        {
                            bool flag = true;
                            foreach (VehicleStorage g in vehiclestorageList)
                            {
                                if (g.Have(temp))
                                {
                                    flag = false;
                                    break;
                                }
                            }
                            if (flag)
                            {
                                vehiclestorageList.Add(temp);
                            }
                        }
                        //End makes
                    }
                    

                    //Add to decription the colors
                    if (colors.Count > 0)
                    {
                        Ad.description += "\r\n";
                        Ad.description += "\r\n";
                        if (colors.Count == 1)
                        {
                            Ad.description += "Διαθέσιμο χρώμα:";
                        }
                        else
                        {
                            Ad.description += "Διαθέσιμα χρώματα:";
                        }                       
                        Ad.description += "\r\n";
                        foreach (string col in colors)
                        {
                            Ad.description += col + "\r\n";
                        }
                    }

                    //Add Stamp
                    Ad.description += "\r\n";
                    Ad.description += "\r\n";
                    Ad.description += Properties.Settings.Default.stamp;


                    //Write it down
                    WriteBookData(writer, Ad, vehiclestorageList, photos , categories);
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();
            }


            //Uploads
            string ServerPath = Properties.Settings.Default.CarXMLPath;
            ServerPath = ServerPath.Replace("/", "//");
            ServerPath = ServerPath.Replace("http:////", "ftp://");

            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(Properties.Settings.Default.SklavisUsername, Properties.Settings.Default.SklavisPassword);
                client.UploadFile(ServerPath, "STOR", path);
            }
        }

        public void ReadXML()
        {
            try
            {
                string path = "";
                OpenFileDialog filedialog = new OpenFileDialog();
                filedialog.Filter = "XML Files (*.xml)|*.xml";
                filedialog.FilterIndex = 1;
                filedialog.Multiselect = false;

                if (filedialog.ShowDialog() == DialogResult.OK)
                {
                    path = filedialog.FileName;
                }
                else return;

                List<CarProduct> cars = new List<CarProduct>();
                DataAccess dts = new DataAccess();

                XmlTextReader xmlReader = new XmlTextReader(path);
                while (xmlReader.Read())
                {
                    string temp;
                    switch (xmlReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (xmlReader.Name == "unique_id") temp = xmlReader.Value.ToString();
                            //listBox1.Items.Add("<" + xmlReader.Name + ">");
                            break;
                        case XmlNodeType.Text:
                            //listBox1.Items.Add(xmlReader.Value);
                            break;
                        case XmlNodeType.EndElement:
                            //listBox1.Items.Add("");
                            break;
                    }
                }

            }
            catch (Exception e)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(@"There was an error." + Environment.NewLine + Environment.NewLine + "Error: " + e.ToString());
            }
        }

    }
    public class ExtraMakeHolder
    {
        public List<string> Make { get; set; } = new List<string>();
        public List<string> Model { get; set; } = new List<string>();
        public List<int> Datefrom { get; set; } = new List<int>();
        public List<int> Dateto { get; set; } = new List<int>();
        public int Counter { get; set; } = 0;

        public void addExtra(string make, string model, int datefrom, int dateto)
        {
            for (int i = 0; i < Counter; i++)
            {
                if (Make[i] == make && Model[i] == model && Datefrom[i] == datefrom && Dateto[i] == dateto) return;
            }


            Model.Add(model);
            Make.Add(make);
            Datefrom.Add(datefrom);
            Dateto.Add(dateto);
            Counter++;
        }
    }

}
