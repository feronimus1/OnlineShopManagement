﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace ShopManager
{
    [Table("User")]
    public class User
    {
        [Key]
        public int ID { get; set; }
        public string UserName { get; set; }
        public int auth { get; set; }
        public string authReadable { get; set; }
        public string  Password { get; set; }
    }
}
