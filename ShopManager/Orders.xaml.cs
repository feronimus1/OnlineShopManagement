﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.DataGrid;
using Xceed.Wpf.Toolkit;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for Orders.xaml
    /// </summary>
    public partial class Orders : UserControl
    {
        List<WorkFlowScriptIDDataStore> TreeView_List = new List<WorkFlowScriptIDDataStore>();
        List<categories> CategoriesRaw = new List<categories>();
        List<vehicles> VehiclesRaw = new List<vehicles>();
        List<string> columnstrings = new List<string>();
        List<vehicles> VehiclesMake = new List<vehicles>();
        List<vehicles> VehiclesModel = new List<vehicles>();


        ObservableCollection<vehicles> OVehiclesMake = new ObservableCollection<vehicles>();
        ObservableCollection<vehicles> OVehiclesModel = new ObservableCollection<vehicles>();
        List<CarProduct> CarProductsRaw = new List<CarProduct>();
        List<CarProduct> CarProductsCart = new List<CarProduct>();
        //List<CarProduct> carsReadable = new List<CarProduct>();
        List<CarProduct> carsSearch = new List<CarProduct>();
        List<CarProduct> CarsToDelete = new List<CarProduct>();
        DataAccess dt = new DataAccess();
        int MaxPrice = 0;
        int MinPrice = 0;
        int MaxAmount = 0;
        int MinAmount = 0;
        int ComboBox_Make_Index;
        int ComboBox_Model_Index;
        //End  variables


        //Start program
        public Orders()
        {
            InitializeComponent();
            busyIndo.IsBusy = true;
            UpdateGrid();
            InitializeComonents();
        }
        /*
        * Reads Car Products remotely 
        * Clears and updates Datagrid
        *  
        */
        private void UpdateGrid()
        {
            CategoriesRaw.Clear();
            VehiclesRaw.Clear();
            CarProductsRaw.Clear();
            CategoriesRaw.AddRange(caching.CategoriesCached);
            VehiclesRaw.AddRange(caching.VehiclesCached);

            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Downloading Data...";

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += ReadCarProductData;
            worker.RunWorkerCompleted += (o, a) =>
            {
                try
                {
                    DataGrid_1.ItemsSource = null;
                    DataGrid_1.ItemsSource = CarProductsRaw;
                    if (CarProductsRaw.Count == 0)
                    {
                        Xceed.Wpf.Toolkit.MessageBox.Show("Δεν βρέθηκαν προϊόντα!");
                    }
                    busyIndo.IsBusy = false;
                    DataGrid_1.Visibility = Visibility.Visible;
                }
                catch (Exception) { }
            };
            worker.RunWorkerAsync();
        }
        /*
        *  Reads Products Data
        */
        private void ReadCarProductData(object sender, DoWorkEventArgs e)
        {
            DataAccess dts = new DataAccess();
            CarProductsRaw.Clear();
            CarProductsRaw.AddRange(dts.ReadCarProduct(Properties.Settings.Default.DBName));
        }
        /*
         * Hides dataview 
         * Calls the initialize Data async
         */
        private void InitializeComonents()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += CreateCategories_Parents;
            worker.RunWorkerCompleted += (o, a) =>
            {
                try
                {
                    treeView.ItemsSource = TreeView_List;
                    VehiclesMake.AddRange(VehiclesRaw.DistinctBy(e => e.make_name).ToList());
                    VehiclesModel.AddRange(VehiclesRaw.DistinctBy(e => e.model_name).ToList());

                    foreach (vehicles v in VehiclesRaw.DistinctBy(e => e.make_name).ToList())
                    {
                        OVehiclesMake.Add(v);
                    }

                    MaxPrice = (int)Math.Ceiling((Double)CarProductsRaw.Max(e => e.price));
                    MinPrice = (int)Math.Ceiling((Double)CarProductsRaw.Min(e => e.price));
                    RangeCarPrice.Maximum = MaxPrice;
                    RangeCarPrice.HigherValue = MaxPrice;
                    RangeCarPrice.Minimum = MinPrice;
                    RangeCarPrice.LowerValue = MinPrice;


                    LabelMaxPrice.Content = MaxPrice;
                    LabelMinPrice.Content = MinPrice;

                    MaxAmount = (int)Math.Ceiling((Double)CarProductsRaw.Max(e => e.amount));
                    MinAmount = (int)Math.Ceiling((Double)CarProductsRaw.Min(e => e.amount));
                    RangeCarAmount.Maximum = MaxAmount;
                    RangeCarAmount.HigherValue = MaxAmount;
                    RangeCarAmount.Minimum = MinAmount;
                    RangeCarAmount.LowerValue = MinAmount;

                    LabelMaxAmount.Content = MaxAmount;
                    LabelMinAmount.Content = MinAmount;

                    SearchBox_Make.ItemsSource = OVehiclesMake;
                    ComboBox_Make_Index = SearchBox_Make.SelectedIndex;
                    SearchBox_Model.ItemsSource = VehiclesModel;
                    ComboBox_Model_Index = SearchBox_Model.SelectedIndex;
                    SearchBox_Color.ItemsSource = caching.colors;

                    CheckboxCar.IsChecked = true;
                    busyIndo.IsBusy = false;
                }
                catch (Exception) { InitializeComonents(); }
            };
            worker.RunWorkerAsync();
        }

        /*
         * Takes Categories into CategoriesRaw
         * Takes Vehicles into VehiclesRaw
         * Creates categories Parents          
         * 
         */
        private void CreateCategories_Parents(object sender, DoWorkEventArgs e)
        {
            foreach (categories p in CategoriesRaw.ToList())
            {
                //if (t.parent_id ==  "" && t.id=="30")
                //If its a car-parts
                if (p.parent_id == "77")
                {
                    WorkFlowScriptIDDataStore parent = new WorkFlowScriptIDDataStore()
                    {
                        Id = p.id,
                        Name = p.name,
                        ParentId = p.parent_id,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    parent.Subcategories.AddRange(FindChilds(parent.Id));
                    if (parent.Subcategories.Count == 0) parent.Place = 2;
                    else parent.Place = 0;
                    TreeView_List.Add(parent);
                }
            }
        }
        /*
        * Creates categories Childs 
        *          
        */
        public List<WorkFlowScriptIDDataStore> FindChilds(string id)
        {
            List<WorkFlowScriptIDDataStore> childList = new List<WorkFlowScriptIDDataStore>();
            while (CategoriesRaw.Count == 0) System.Threading.Thread.Sleep(10);
            foreach (categories t in CategoriesRaw.ToList())
            {
                if (t.parent_id == id)
                {
                    WorkFlowScriptIDDataStore child = new WorkFlowScriptIDDataStore()
                    {
                        Id = t.id,
                        Name = t.name,
                        ParentId = t.parent_id,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    child.Subcategories.AddRange(FindChilds(child.Id));
                    if (child.Subcategories.Count == 0) child.Place = 2;
                    else child.Place = 1;
                    childList.Add(child);
                }
            }
            return childList;
        }



        /*
         * Action commands For Filter
         * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         */


        private void TextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            CheckData_Filters();
        }
        private void treeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
                if (temp.Place != 2) return;
                Textcategory_id.IsOpen = false;
                Textcategory_id.Content = temp.Name;
                CheckData_Filters();
            }
            catch (Exception) { return; }

        }
        private void treeView_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //if ENTER is pressed.
            if (e.Key == Key.Return)
            {
                WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
                if (temp.Place != 2) return;
                Textcategory_id.IsOpen = false;
                Textcategory_id.Content = temp.Name;
                CheckData_Filters();
            }
        }
        private void RangeCarPrice_LowerValueChanged(object sender, RoutedEventArgs e)
        {
            MinPrice = (int)Math.Round(RangeCarPrice.LowerValue);
            LabelMinPrice.Content = MinPrice;
            CheckData_Filters();
        }
        private void RangeCarPrice_HigherValueChanged(object sender, RoutedEventArgs e)
        {
            MaxPrice = (int)Math.Round(RangeCarPrice.HigherValue);
            LabelMaxPrice.Content = MaxPrice;
            CheckData_Filters();
        }
        private void RangeCarAmount_LowerValueChanged(object sender, RoutedEventArgs e)
        {
            MinAmount = (int)Math.Round(RangeCarAmount.LowerValue);
            LabelMinAmount.Content = MinAmount;
            CheckData_Filters();
        }
        private void RangeCarAmount_HigherValueChanged(object sender, RoutedEventArgs e)
        {
            MaxAmount = (int)Math.Round(RangeCarAmount.HigherValue);
            LabelMaxAmount.Content = MaxAmount;
            CheckData_Filters();
        }
        private void CheckboxCar_Checked_change(object sender, RoutedEventArgs e)
        {
            CheckData_Filters();
        }
        private void CheckboxCar_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckData_Filters();
        }


        private void SearchBox_Make_DropDownClosed(object sender, EventArgs e)
        {
            if (SearchBox_Make.SelectedIndex == -1) return;
            else
            {
                CheckData_Filters();
                SearchBox_Model_FilterforMake();
            }
        }
        private void SearchBox_Model_FilterforMake()
        {
            if (SearchBox_Make.SelectedIndex == -1 || SearchBox_Make.Text == "" || SearchBox_Make.Text == null)
            {
                VehiclesModel.Clear();
                VehiclesModel.AddRange(VehiclesRaw.DistinctBy(v => v.model_name).ToList());
            }
            else
            {
                VehiclesModel.Clear();
                VehiclesModel.AddRange(VehiclesRaw.Where(v => v.make_name == SearchBox_Make.Text));
            }
            SearchBox_Model.ItemsSource = VehiclesModel;
        }
        private void SearchBox_Model_DropDownClosed(object sender, EventArgs e)
        {
            CheckData_Filters();
        }

        private void CheckData_Filters()
        {
            try
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(DataGrid_1.ItemsSource);

                itemsViewOriginal.Filter = ((o) =>
                {
                    bool UniqueID = false;
                    if (String.IsNullOrEmpty(SearchBox_Unique_id.Text) || ((CarProduct)o).unique_id.ToLower().Contains(SearchBox_Unique_id.Text.ToLower())) UniqueID = true;

                    bool Title = false;
                    if (String.IsNullOrEmpty(SearchBox_Title.Text) || ((CarProduct)o).title.ToLower().Contains(SearchBox_Title.Text.ToLower())) Title = true;

                    bool Description = false;
                    if (String.IsNullOrEmpty(SearchBox_Description.Text) || ((CarProduct)o).description.ToLower().Contains(SearchBox_Description.Text.ToLower())) Description = true;

                    bool Category = false;
                    if (String.IsNullOrEmpty((String)Textcategory_id.Content) || ((CarProduct)o).category_id == Textcategory_id.Content.ToString()) Category = true;

                    bool Price = false;
                    if (((CarProduct)o).price >= RangeCarPrice.LowerValue && ((CarProduct)o).price <= RangeCarPrice.HigherValue) Price = true;

                    bool Amount = false;
                    if (((CarProduct)o).amount >= RangeCarAmount.LowerValue && ((CarProduct)o).amount <= RangeCarAmount.HigherValue) Amount = true;

                    bool Make = false;
                    if (String.IsNullOrEmpty(SearchBox_Make.Text) || ((CarProduct)o).make_name == SearchBox_Make.Text) Make = true;

                    bool Model = false;
                    if (String.IsNullOrEmpty(SearchBox_Model.Text) || ((CarProduct)o).model_name == SearchBox_Model.Text) Model = true;

                    bool Color = false;
                    if (String.IsNullOrEmpty(SearchBox_Color.Text) || ((CarProduct)o).color == SearchBox_Color.Text) Color = true;

                    bool Cargrbool = false;
                    if (CheckboxCar.IsChecked ?? false)
                    {
                        if (((CarProduct)o).isParent == "ΝΑΙ") Cargrbool = true;
                    }
                    else Cargrbool = true;


                    if (UniqueID && Title && Description && Category && Price && Amount && Make && Model && Cargrbool && Color) return true;
                    else return false;
                });

                itemsViewOriginal.Refresh();
            }
            catch (Exception)
            {
                return;
            }
        }

        /*
         * Rest Actions follow
         * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         */        
        private void UpdateDataset()
        {
            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Updating Data...";

            //var temp = data1.ItemsSource;
            //cars.Count();
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += UpdateProsses;
            worker.RunWorkerCompleted += (o, a) =>
            {
                busyIndo.IsBusy = false;
                UpdateGrid();
            };
            worker.RunWorkerAsync();
        }
        private void UpdateProsses(object sender, DoWorkEventArgs e)
        {
            //fix cars       
            dt.UpdateCarPoduct(Properties.Settings.Default.DBName, CarProductsRaw);
        }       
        private void Button_Click_Insert(object sender, RoutedEventArgs e)
        {           
            List<CarProduct> tempcar = new List<CarProduct>();
            tempcar.Clear();
            try
            {
                tempcar.AddRange(DataGrid_1.SelectedItems.Cast<CarProduct>().ToList());
            }
            catch (Exception)
            {
                return;
            }

            int amount = TextAmount.Value ?? 0;
            if (amount == 0) return;
            bool exists = false;
            foreach (CarProduct RawItem in tempcar)
            {
                foreach (CarProduct CartItem in CarProductsCart)
                {
                    if (CartItem.unique_id == RawItem.unique_id)
                    {
                        if (CartItem.amount + amount <= RawItem.amount && CartItem.amount + amount > 0)
                        {
                            CartItem.amount += amount;
                            exists = true;
                            break;
                        }
                        else if (CartItem.amount + amount <= 0)
                        {
                            CarProductsCart.Remove(CartItem);
                            exists = true;
                            break;
                        }
                        else
                        {
                            CartItem.amount = RawItem.amount;
                            exists = true;
                            break;
                        }
                    }   
                }
                if (!exists)
                {
                    CarProduct tempCarProduct = new CarProduct(RawItem);
                    tempCarProduct.amount = (RawItem.amount < amount) ? RawItem.amount : amount;
                    CarProductsCart.Add(tempCarProduct);
                }

            }

            DataGrid_2.ItemsSource = null;
            try
            {

                DataGrid_2.ItemsSource = CarProductsCart;
            }
            catch (Exception)
            {

                DataGrid_2.ItemsSource = CarProductsCart;
            }
        }
        private void Button_Click_Remove(object sender, RoutedEventArgs e)
        {        
            List<CarProduct> tempcar = new List<CarProduct>();
            tempcar.Clear();
            try
            {
                tempcar.AddRange(DataGrid_1.SelectedItems.Cast<CarProduct>().ToList());
            }
            catch (Exception)
            {
                return;
            }

            int amount = -TextAmount.Value ?? 0;
            if (amount == 0) return;
            bool exists = false;
            foreach (CarProduct RawItem in tempcar)
            {
                foreach (CarProduct CartItem in CarProductsCart)
                {
                    if (CartItem.unique_id == RawItem.unique_id)
                    {
                        if (CartItem.amount + amount <= RawItem.amount && CartItem.amount + amount > 0)
                        {
                            CartItem.amount += amount;
                            exists = true;
                            break;
                        }
                        else if (CartItem.amount + amount <= 0)
                        {
                            CarProductsCart.Remove(CartItem);
                            exists = true;
                            break;
                        }
                        else
                        {
                            CartItem.amount = RawItem.amount;
                            exists = true;
                            break;
                        }
                    }
                }
                if (!exists)
                {
                    if (amount <= 0) continue;
                    CarProduct tempCarProduct = new CarProduct(RawItem);
                    tempCarProduct.amount = (RawItem.amount < amount) ? RawItem.amount : amount;
                    CarProductsCart.Add(tempCarProduct);
                }

            }

            DataGrid_2.ItemsSource = null;
            try
            {

                DataGrid_2.ItemsSource = CarProductsCart;
            }
            catch (Exception)
            {

                DataGrid_2.ItemsSource = CarProductsCart;
            }
        }

        private void Button_Click_Checkout(object sender, RoutedEventArgs e)
        {
            caching.CarsOrder.Clear();
            caching.CarsOrder.AddRange(CarProductsCart);


            
            var loginForm = new PlaceHolder();
            loginForm.ShowDialog();
            //LocalTest.LoadProcedure();
            //UpdateItemWindow.Show();
        }
    }
}

