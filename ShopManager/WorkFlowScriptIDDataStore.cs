﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManager
{
    public class WorkFlowScriptIDDataStore
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ParentId { get; set; }
        // 0 = parent
        // 1 = middle
        // 2 =  leaf
        public int Place { get; set; }

        public List<WorkFlowScriptIDDataStore> Subcategories { get; set; }
        public void Clear()
        {
            Id = "";
            Name = "";
            ParentId = "";
        }
    }
}
