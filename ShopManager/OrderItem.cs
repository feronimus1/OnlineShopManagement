﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ShopManager
{
    public class OrderItem : INotifyPropertyChanged
    {
        public string Name { get; set; }

        private double _price;
        public double Price
        {
            get { return _price; }
            set { _price = value;
                NotifyPropertyChanged(nameof(TotalPrice));
                NotifyPropertyChanged(nameof(TaxPrice));
                NotifyPropertyChanged(nameof(DiscountPrice));
            }
        }

        private double _tax;
        public double Tax
        {
            get { return _tax; }
            set { _tax = value;
                NotifyPropertyChanged(nameof(TotalPrice));
                NotifyPropertyChanged(nameof(TaxPrice));
                NotifyPropertyChanged(nameof(DiscountPrice));
                NotifyPropertyChanged(nameof(PriceBeforeTax));
            }
        }

        private double _discount;
        public double Discount
        {
            get { return _discount; }
            set { _discount = value;
                NotifyPropertyChanged(nameof(TotalPrice));
                NotifyPropertyChanged(nameof(TaxPrice));
                NotifyPropertyChanged(nameof(DiscountPrice));
                NotifyPropertyChanged(nameof(PriceBeforeTax));
            }
        }

        private double _amount;
        public double Amount
        {
            get { return _amount; }
            set { _amount = value;
                NotifyPropertyChanged(nameof(TotalPrice));
                NotifyPropertyChanged(nameof(TaxPrice));
                NotifyPropertyChanged(nameof(DiscountPrice));
                NotifyPropertyChanged(nameof(PriceBeforeTax));                
            }
        }

        public double TotalPrice
        {
            get
            {
                return Math.Round(((_price * _amount) * (1 - _discount / 100)),2);
            }
        }
        public double TaxPrice
        {
            get
            {
                return Math.Round(((_price * _amount) * (1 - _discount / 100))-((_price * _amount) * (1 - _discount / 100)) / (1 + _tax / 100),2);
            }
        }

        public double PriceBeforeTax
        {
            get
            {
                return Math.Round(((_price * _amount) * (1 - _discount / 100)) / (1 + _tax / 100),2);
            }
        }

        public double DiscountPrice
        {
            get
            {
                return Math.Round(((_price * _amount) * (_discount / 100)),2);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            //if (PropertyChanged != null)
          
            //    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));


        }
    }
}
