﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace ShopManager
{
    [Table("Info")]
    public class Info
    {
        [Key]
        public int id { get; set; }
        public string MainInfo { get; set; }
    }
}
