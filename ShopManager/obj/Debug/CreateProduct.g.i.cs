#pragma checksum "..\..\CreateProduct.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "03ACA19068A82053608CD4F7FE4FC4E8BC02B37A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using ShopManager;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ShopManager
{


    /// <summary>
    /// CreateProduct
    /// </summary>
    public partial class CreateProduct : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector
    {

        private bool _contentLoaded;

        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent()
        {
            if (_contentLoaded)
            {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ShopManager;component/createproduct.xaml", System.UriKind.Relative);

#line 1 "..\..\CreateProduct.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);

#line default
#line hidden
        }

        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
        {
            this._contentLoaded = true;
        }

        internal System.Windows.Controls.UserControl UserControl;
        internal Xceed.Wpf.Toolkit.BusyIndicator busyIndo;
        internal System.Windows.Controls.TextBox TextUnique_id_Child_Secondary;
        internal System.Windows.Controls.ComboBox TextUnique_id_Child_Primary;
        internal System.Windows.Controls.TextBox TextUnique_id_Parent;
        internal System.Windows.Controls.TextBox TextTitle;
        internal Xceed.Wpf.Toolkit.DropDownButton Textcategory_id;
        internal System.Windows.Controls.TreeView treeView;
        internal Xceed.Wpf.Toolkit.LongUpDown TextPrice;
        internal Xceed.Wpf.Toolkit.MultiLineTextEditor TextDescription;
        internal System.Windows.Controls.ComboBox TextMake;
        internal System.Windows.Controls.ComboBox TextModel;
        internal System.Windows.Controls.ComboBox TextYearfrom;
        internal System.Windows.Controls.ComboBox TextYearTo;
        internal System.Windows.Controls.ComboBox Combo_Aftermarket_Manufacturer;
        internal System.Windows.Controls.TextBox Text_Aftermarket_Manufacturer;
        internal Xceed.Wpf.Toolkit.IntegerUpDown TextAmount;
        internal System.Windows.Controls.Image Picturebox;
        internal Xceed.Wpf.Toolkit.IntegerUpDown TextInterval;
        internal System.Windows.Controls.ComboBox TextCondition;
        internal Xceed.Wpf.Toolkit.MultiLineTextEditor TextPhoto;
        internal System.Windows.Controls.TextBox cat;
        internal System.Windows.Controls.TextBox make;
        internal System.Windows.Controls.TextBox model;
        internal System.Windows.Controls.TextBox buffer;
        internal System.Windows.Controls.ComboBox TextShowOnCarGr;
        internal System.Windows.Controls.ComboBox TextColor;
    }
}

