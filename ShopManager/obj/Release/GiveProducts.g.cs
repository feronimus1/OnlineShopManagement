﻿#pragma checksum "..\..\GiveProducts.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5A308A6C577DC0B2A18396F0A4C8BEF8B3D9E312"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using ShopManager;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.DataGrid;
using Xceed.Wpf.DataGrid.Converters;
using Xceed.Wpf.DataGrid.Markup;
using Xceed.Wpf.DataGrid.ValidationRules;
using Xceed.Wpf.DataGrid.Views;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace ShopManager {
    
    
    /// <summary>
    /// GiveProducts
    /// </summary>
    public partial class GiveProducts : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.BusyIndicator busyIndo;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SearchBox_Unique_id;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SearchBox_Title;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SearchBox_Description;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DropDownButton Textcategory_id;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TreeView treeView;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox SearchBox_Color;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.WrapPanel PanelPrice;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LabelMinPrice;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.RangeSlider RangeCarPrice;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LabelMaxPrice;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.WrapPanel PanelAmount;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LabelMinAmount;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.RangeSlider RangeCarAmount;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LabelMaxAmount;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox SearchBox_Make;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox SearchBox_Model;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CheckboxCar;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.DataGrid.DataGridControl DataGrid_1;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox checkBoxPhoto;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.ChildWindow UpdateItemWindow;
        
        #line default
        #line hidden
        
        
        #line 182 "..\..\GiveProducts.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ShopManager.UpdateProduct LocalTest;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ShopManager;component/giveproducts.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\GiveProducts.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.busyIndo = ((Xceed.Wpf.Toolkit.BusyIndicator)(target));
            return;
            case 2:
            this.SearchBox_Unique_id = ((System.Windows.Controls.TextBox)(target));
            
            #line 35 "..\..\GiveProducts.xaml"
            this.SearchBox_Unique_id.KeyUp += new System.Windows.Input.KeyEventHandler(this.TextBox_KeyUp);
            
            #line default
            #line hidden
            return;
            case 3:
            this.SearchBox_Title = ((System.Windows.Controls.TextBox)(target));
            
            #line 41 "..\..\GiveProducts.xaml"
            this.SearchBox_Title.KeyUp += new System.Windows.Input.KeyEventHandler(this.TextBox_KeyUp);
            
            #line default
            #line hidden
            return;
            case 4:
            this.SearchBox_Description = ((System.Windows.Controls.TextBox)(target));
            
            #line 47 "..\..\GiveProducts.xaml"
            this.SearchBox_Description.KeyUp += new System.Windows.Input.KeyEventHandler(this.TextBox_KeyUp);
            
            #line default
            #line hidden
            return;
            case 5:
            this.Textcategory_id = ((Xceed.Wpf.Toolkit.DropDownButton)(target));
            return;
            case 6:
            this.treeView = ((System.Windows.Controls.TreeView)(target));
            
            #line 56 "..\..\GiveProducts.xaml"
            this.treeView.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.treeView_MouseDoubleClick);
            
            #line default
            #line hidden
            
            #line 56 "..\..\GiveProducts.xaml"
            this.treeView.KeyDown += new System.Windows.Input.KeyEventHandler(this.treeView_KeyDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.SearchBox_Color = ((System.Windows.Controls.ComboBox)(target));
            
            #line 77 "..\..\GiveProducts.xaml"
            this.SearchBox_Color.DropDownClosed += new System.EventHandler(this.SearchBox_Model_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 8:
            this.PanelPrice = ((System.Windows.Controls.WrapPanel)(target));
            return;
            case 9:
            this.LabelMinPrice = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.RangeCarPrice = ((Xceed.Wpf.Toolkit.RangeSlider)(target));
            
            #line 87 "..\..\GiveProducts.xaml"
            this.RangeCarPrice.LowerValueChanged += new System.Windows.RoutedEventHandler(this.RangeCarPrice_LowerValueChanged);
            
            #line default
            #line hidden
            
            #line 87 "..\..\GiveProducts.xaml"
            this.RangeCarPrice.HigherValueChanged += new System.Windows.RoutedEventHandler(this.RangeCarPrice_HigherValueChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            this.LabelMaxPrice = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.PanelAmount = ((System.Windows.Controls.WrapPanel)(target));
            return;
            case 13:
            this.LabelMinAmount = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.RangeCarAmount = ((Xceed.Wpf.Toolkit.RangeSlider)(target));
            
            #line 96 "..\..\GiveProducts.xaml"
            this.RangeCarAmount.LowerValueChanged += new System.Windows.RoutedEventHandler(this.RangeCarAmount_LowerValueChanged);
            
            #line default
            #line hidden
            
            #line 96 "..\..\GiveProducts.xaml"
            this.RangeCarAmount.HigherValueChanged += new System.Windows.RoutedEventHandler(this.RangeCarAmount_HigherValueChanged);
            
            #line default
            #line hidden
            return;
            case 15:
            this.LabelMaxAmount = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.SearchBox_Make = ((System.Windows.Controls.ComboBox)(target));
            
            #line 113 "..\..\GiveProducts.xaml"
            this.SearchBox_Make.DropDownClosed += new System.EventHandler(this.SearchBox_Make_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 17:
            this.SearchBox_Model = ((System.Windows.Controls.ComboBox)(target));
            
            #line 131 "..\..\GiveProducts.xaml"
            this.SearchBox_Model.DropDownClosed += new System.EventHandler(this.SearchBox_Model_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 18:
            this.CheckboxCar = ((System.Windows.Controls.CheckBox)(target));
            
            #line 136 "..\..\GiveProducts.xaml"
            this.CheckboxCar.Checked += new System.Windows.RoutedEventHandler(this.CheckboxCar_Checked_change);
            
            #line default
            #line hidden
            
            #line 136 "..\..\GiveProducts.xaml"
            this.CheckboxCar.Unchecked += new System.Windows.RoutedEventHandler(this.CheckboxCar_Unchecked);
            
            #line default
            #line hidden
            return;
            case 19:
            this.DataGrid_1 = ((Xceed.Wpf.DataGrid.DataGridControl)(target));
            
            #line 141 "..\..\GiveProducts.xaml"
            this.DataGrid_1.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.DataGrid_1_MouseDoubleClick);
            
            #line default
            #line hidden
            
            #line 141 "..\..\GiveProducts.xaml"
            this.DataGrid_1.MouseEnter += new System.Windows.Input.MouseEventHandler(this.DataGrid_1_MouseEnter);
            
            #line default
            #line hidden
            return;
            case 20:
            
            #line 159 "..\..\GiveProducts.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Delete_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            
            #line 160 "..\..\GiveProducts.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Update_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            
            #line 161 "..\..\GiveProducts.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.checkBoxPhoto = ((System.Windows.Controls.CheckBox)(target));
            
            #line 162 "..\..\GiveProducts.xaml"
            this.checkBoxPhoto.Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 162 "..\..\GiveProducts.xaml"
            this.checkBoxPhoto.Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Unchecked);
            
            #line default
            #line hidden
            return;
            case 24:
            this.UpdateItemWindow = ((Xceed.Wpf.Toolkit.ChildWindow)(target));
            return;
            case 25:
            this.LocalTest = ((ShopManager.UpdateProduct)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

