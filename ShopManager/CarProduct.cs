﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace ShopManager
{
    [Table("CarProduct")]
    public class CarProduct
    {
        [ExplicitKey]
        public string unique_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string category_id { get; set; }
        public string category_name { get; set; }
        public double price { get; set; }
        public string photo { get; set; }
        public string conditions { get; set; }
        public int update_interval { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public int yearfrom { get; set; }
        public int yearto { get; set; }
        public int amount { get; set; }
        public string isParent { get; set; }
        public string make_name { get; set; }
        public string model_name { get; set; }
        public string color { get; set; }
        public string manufacturer_number { get; set; }
        public string aftermarket_number { get; set; }
        public string StoragePlace { get; set; }
        public string AdId { get; set; }
        public int taken { get; set; }
        public bool Public { get; set; }
        public string info { get; set; }
        //public string FristPartCode { get; set; }
        //public string SecondPartCode { get; set; }


        public string GetFirstPartCode()
        {
            if (isParent == "Ναι")
            {
                return unique_id;
            }
            return unique_id.Split('-')[0];
        }

        public string GetSecondPartCode()
        {
            if (isParent == "Ναι")
            {
                return null;
            }
            return unique_id.Split('-')[1];
        }

        public CarProduct()
        {
        }

            public CarProduct(CarProduct c)
        {
            unique_id = c.unique_id;
            title = c.title;
            description = c.description;
            category_id = c.category_id;
            price = c.price;
            photo = c.photo;
            conditions = c.conditions;
            update_interval = c.update_interval;
            make = c.make;
            model = c.model;
            yearfrom = c.yearfrom;
            yearto = c.yearto;
            amount = c.amount;
            isParent = c.isParent;
            category_name = c.category_name;
            make_name = c.make_name;
            model_name = c.model_name;
            color = c.color;
            manufacturer_number = c.manufacturer_number;
            aftermarket_number = c.aftermarket_number;
            StoragePlace = c.StoragePlace;
        }

        
    }
  
}
