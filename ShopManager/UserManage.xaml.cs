﻿using ShopManager;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for UserManage.xaml
    /// </summary>
    public partial class UserManage : UserControl
    {
        List<User> users = new List<User>();
        List<User> OldUsers = new List<User>();
        DataAccess dts = new DataAccess();
        List<string> authsTypes = new List<string>();
        bool newUser = false;
        string NameInUse = "";
        public UserManage()
        {
            InitializeComponent();
            authsTypes.Add("Χρήστης");
            authsTypes.Add("Διαχειριστής");
            comboAuth.ItemsSource = authsTypes;
            RefreshData();
            RefreshDatagrid();


        }
        private void RefreshData()
        {
            users.Clear();
            OldUsers.Clear();
            users.AddRange(dts.ReadUsers(Properties.Settings.Default.DBName));
            OldUsers.AddRange(users);
        }
        private void RefreshDatagrid()
        {
            MainDatagrid.ItemsSource = null;
            MainDatagrid.ItemsSource = users;

        }
        private void MainDatagrid_MouseEnter(object sender, MouseEventArgs e)
        {
            MainDatagrid.Focus();
        }
        private void LoadUser(User user)
        {
            NameInUse = user.UserName;
            TextUsername.Text = user.UserName;
            TextPass.Text = user.Password;
            if (user.auth == 1) comboAuth.SelectedIndex = 0;
            else if (user.auth == 2) comboAuth.SelectedIndex = 1;
        }
        private void ClearAll()
        {
            TextUsername.Text = "";
            TextPass.Text = "";
            comboAuth.SelectedIndex = -1;
        }
        private void Button_Click_delete(object sender, RoutedEventArgs e)
        {
            try
            {
                User temp = (User)MainDatagrid.SelectedItem;
                if (temp == null) return;

                List<User> templist = new List<User>();
                templist.Add(temp);
                dts.DeleteUser(Properties.Settings.Default.DBName, templist);

                RefreshData();
                RefreshDatagrid();

            }
            catch (Exception)
            {
                return;
            }
        }
        private void MainDatagrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            User temp = (User)MainDatagrid.SelectedItem;
            if (temp == null) return;
            LoadUser(temp);
        }


        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            if (Check_Filled()) return;
            if (CheckUnique(TextUsername.Text))
            {//update
                foreach (User u in users)
                {
                    if (u.UserName == TextUsername.Text)
                    {
                        u.Password = TextPass.Text;
                        u.authReadable = comboAuth.Text;
                        u.auth = comboAuth.SelectedIndex + 1;

                        List<User> templist = new List<User>();
                        templist.Add(u);
                        dts.UpdateUsers(Properties.Settings.Default.DBName, templist);
                    }
                    else continue;
                }
            }
            else
            {
                //create
                User temp = new User { UserName = TextUsername.Text, Password = TextPass.Text, authReadable = comboAuth.Text, auth = comboAuth.SelectedIndex + 1 };
                List<User> templist = new List<User>();
                templist.Add(temp);
                dts.CreateUsers(Properties.Settings.Default.DBName, templist);
            }

            RefreshData();
            ClearAll();
            RefreshDatagrid();
            //System.Windows.MessageBox.Show("Αποθηκεύτηκαν με Επιτυχία!");
        }
        private bool CheckUnique(string Username)
        {
            SQLCreation sql = new SQLCreation();
            int author = Int32.Parse(dts.LoginAuth(Properties.Settings.Default.DBName, sql.AuthUserName(Username)));

            if (author > 0) return true;
            else return false;
        }

        private void Button_Click_Clear(object sender, RoutedEventArgs e)
        {
            ClearAll();
        }
        private bool Check_Filled()
        {
            if (TextUsername.Text == "" || TextPass.Text == "" || comboAuth.SelectedIndex == -1)
            {
                MessageBox.Show("Παρακαλώ συμπληρώστε όλα τα πεδία.");
                return true;
            }

            return false;
        }
    }




    
}

