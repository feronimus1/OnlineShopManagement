﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : UserControl
    {
        public Dashboard()
        {
            InitializeComponent();

            DataAccess dts = new DataAccess();
            List<Info> infolist = new List<Info>();
            infolist.AddRange(dts.ReadInfo(Properties.Settings.Default.DBName).Where(c => c.id == Properties.Settings.Default.NoteID));
            Properties.Settings.Default.NoteText = infolist[0].MainInfo;
            NoteText.Text = Properties.Settings.Default.NoteText;

            List<CarProduct> Cars = dts.ReadCarProduct(Properties.Settings.Default.DBName);
            int totalProducts = 0;
            int totalamountProducts = 0;
            int emptyProducts = 0;
            int totalAds = 0;
            int emptyAds = 0;
            int HiddenAds = 0;
            foreach (CarProduct item in Cars)
            {
                
                if (item.isParent == "ΝΑΙ")
                {
                    //List<CarProduct> products = new List<CarProduct>();
                    //products.AddRange(Cars.Where(c => c.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList().Contains(item.unique_id)));

                    if (Cars.Where(c => c.AdId.Split(new string[] { "!#!" }, StringSplitOptions.None).ToList().Contains(item.unique_id)).Count() == 0) emptyAds++;
                    else if (item.Public) totalAds++;
                    else HiddenAds++;
                }
                else
                {

                    if (item.AdId == "")
                    {
                        emptyProducts++;
                    }
                    else
                    {
                        totalamountProducts += item.amount;
                        totalProducts++;
                    }
                }

            }
            TextTotalProductLabel.Content = "Προϊόντα:";
            TextTotalProduct.Content = totalProducts.ToString();
            TextTotalAdLabel.Content = "Αγγελίες:";
            TextTotalAd.Content = totalAds.ToString();

            TextNoadProductsLabel.Content = "Προϊόντα χωρίς αγγελία:";
            TextNoadProducts.Content = emptyProducts.ToString();
            TextNoProductsADLabel.Content = "Αγγελίες χωρίς προϊόντα:";
            TextNoProductsAD.Content = emptyAds.ToString();
            TextHiddenAdslabel.Content = "Κρυφές: ";
            TextHiddenAds.Content = HiddenAds.ToString();

            if (emptyProducts == 0 )
            {
                //foreground
                TextNoadProductsLabel.Foreground = Brushes.Green;
                TextNoadProducts.Foreground = Brushes.Green;
            }
            if (emptyAds == 0 )
            {
                //foreground
                TextNoProductsADLabel.Foreground = Brushes.Green;
                TextNoProductsAD.Foreground = Brushes.Green;
            }




        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.car.gr/account/statistics/");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.car.gr/classifieds/cars/?fs=1&condition=Μεταχειρισμένο&offer_type=sale&make=57&make=57&model=1183&significant_damage=t&sort=pra");
        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.car.gr/classifieds/cars/?sort=pra&price=>50&offer_type=sale&make=57&model=1183&onlyprice=1&rg=3");
        }

        private void Button_Click_SaveInfo(object sender, RoutedEventArgs e)
        {

            Properties.Settings.Default.NoteText = NoteText.Text;
            Properties.Settings.Default.Save();

            List<Info> infolist = new List<Info>();
            infolist.Add((new Info { id = Properties.Settings.Default.NoteID, MainInfo = Properties.Settings.Default.NoteText }));

            //First time

            //DataAccess dts = new DataAccess();
            //dts.CreateInfo(Properties.Settings.Default.DBName, infolist);

            //uncomment
            DataAccess dts = new DataAccess();
            dts.UpdateInfo(Properties.Settings.Default.DBName, infolist);

            System.Windows.MessageBox.Show("Αποθηκεύτηκε με Επιτυχία!");
        }
    }
}
