﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopManager
{
    public partial class Form1 : Form
    {
        private List<Button> BtnList;
        public Form1()
        {
            AddButtonsList();
            InitializeComponent();
            AddButtonsList();
            resizeButtons();
            //
            //caching.LoginFlag = 1;
            //caching.ReCache();
            //
            this.Visible = false;
           
        }

        private void resizeButtons()
        {
            try
            {
                int HeightTemp = (int)Math.Floor((Double)(panelMenu.Height / (BtnList.Count + 1)));
                pictureBox_Logo.Height = HeightTemp;
                foreach (Button btn in BtnList) btn.Height = HeightTemp;
            }
            catch (Exception) { }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Open_login();
            if (caching.LoginFlag <= 0) { Application.Exit(); return; }
            if (caching.LoginFlag < 2) { button_Users.Visible = false; }
            this.Visible = true;
        }       
        private void AddButtonsList()
        {
            BtnList = new List<Button>();
            BtnList.Add(button_Dashboard);
            BtnList.Add(button_Sell);
            BtnList.Add(button_Users);
            BtnList.Add(button_GetProduct);
            BtnList.Add(button_CarGR);
        }
        private void ResetAllButtons()
        {
            foreach (Button btn in BtnList)
            {
                btn.BackColor = Color.FromArgb(41, 53, 65);
                
            }
        }
        private void FocusThisButton(Button sender)
        {
            ResetAllButtons();
            sender.BackColor =  Color.FromArgb(69, 89, 110);
            sender.Invalidate();
            sender.Update();
            sender.Refresh(); 
             Application.DoEvents();

            panelleft.Visible = true;
            panelleft.Height = sender.Height;
            panelleft.Top = sender.Top;
            panelleft.Invalidate();
            panelleft.Update();
            panelleft.Refresh();
            Application.DoEvents();

           
        }

        private void Open_login()
        {
            this.Hide();
            var loginForm = new Login();
            loginForm.ShowDialog();
            try
            {
                this.Show();
            }
            catch {}
        }

       

        

        private void pictureBox_Logo_Click(object sender, EventArgs e)
        {
            panelleft.Visible = false;
            this.elementHostMain.Child = null;
            ResetAllButtons();

        }

        private void button_Dashboard_Click(object sender, EventArgs e)
        {
            FocusThisButton((Button)sender);

            this.elementHostMain.Child = null; ;
            this.elementHostMain.Child = new Dashboard();
        }

        private void button_Sell_Click(object sender, EventArgs e)
        {
            FocusThisButton((Button)sender);

            this.elementHostMain.Child = null;
            this.elementHostMain.Child = new GiveProducts();
            //FocusThisButton((Button)sender);
            //DataAccess dt = new DataAccess();
            //int temp = dt.GetNumberOfCarProducts(Properties.Settings.Default.DBName);
            //MessageBox.Show(temp.ToString());
        }
       

        private void button_GetProduct_Click(object sender, EventArgs e)
        {
            var loginForm = new PlaceHolder();
            loginForm.ShowDialog();


            FocusThisButton((Button)sender);
            this.elementHostMain.Child = null;
            this.elementHostMain.Child = new Orders();
        }    

        private void button_CarGR_Click(object sender, EventArgs e)
        {
            FocusThisButton((Button)sender);
            this.elementHostMain.Child = null;
            this.elementHostMain.Child = new CarGr();
        }

        private void button_Options_Click(object sender, EventArgs e)
        {
            button_Users.Text = "L:" + caching.StartLoopNUmber.ToString() + "|A:" + caching.CloseAd + "|P:" + caching.CloseProduct;
            Properties.Settings.Default.VisibleAndPositionAndWidth = "";
        }

        private void panelMenu_SizeChanged_1(object sender, EventArgs e)
        {
            resizeButtons();
        }

        private void button_Users_Click(object sender, EventArgs e)
        {
            //Check if user has authority to open this
            if (caching.LoginFlag <= 1)
            {
                MessageBox.Show("Δεν έχετε τα κατάλληλα δικαιώματα να δείτε αυτή την καρτέλα. Επικοινωνήστε με έναν διαχειριστή του συστήματος.");
                return;
            }
            FocusThisButton((Button)sender);
            this.elementHostMain.Child = null;
            this.elementHostMain.Child = new UserManage();
        }
    }
}
