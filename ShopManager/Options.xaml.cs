﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for Options.xaml
    /// </summary>
    public partial class Options : UserControl
    {
        List<string> CnnNames = new List<string>();
        public Options()
        {
            InitializeComponent();
            CnnNames.Clear();
            CnnNames.AddRange(Helper.GetAllCnnNames());
            //con.Text = Properties.Settings.Default.ConString;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SeeconStr.Text = Helper.ReadCnn(SeeconName.Text);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CnnNames.Clear();
            CnnNames.AddRange(Helper.GetAllCnnNames());
            combo.ItemsSource = CnnNames;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (CnnNames.Contains(InputconName.Text) )
            {
                Helper.UpdateCnn(InputconName.Text, InputconStr.Text);
            }
            else
            {
                Helper.CreateCnn(InputconName.Text, InputconStr.Text,"");
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.DBName = combo.SelectedValue.ToString();
        }

        private void Button_Click_RecreateDB(object sender, RoutedEventArgs e)
        {
            SQLCreation sql = new SQLCreation();
            DataAccess dts = new DataAccess();
            //string command = sql.CreateDB() + sql.User() + sql.CarProduct() + sql.categories() + sql.vehicles();
            dts.SQLCommand(Properties.Settings.Default.DBName, sql.CreateDB());
            dts.SQLCommand(Properties.Settings.Default.DBName, sql.User());
            dts.SQLCommand(Properties.Settings.Default.DBName, sql.CreateFirstUser());
            dts.SQLCommand(Properties.Settings.Default.DBName, sql.categories());
            dts.SQLCommand(Properties.Settings.Default.DBName, sql.vehicles());
            dts.SQLCommand(Properties.Settings.Default.DBName, sql.CarProduct());
        }

        private void Button_Click_CreateDummy(object sender, RoutedEventArgs e)
        {
            DataAccess dt = new DataAccess();
            SQLCreation sq = new SQLCreation();
            dt.SQLCommand(Properties.Settings.Default.DBName, sq.CarProduct());

            List<CarProduct> cars = new List<CarProduct>();
            string temp = caching.CategoriesCached.Where(c => c.id == "1908").ToList()[0].name;
            for (int i = 1; i <= NumberOfObjects.Value/10; i++)
            {
                DataAccess dts = new DataAccess();
                String ran = "";
                bool flag = true;
                while (flag)
                {
                    Random generator = new Random();
                    ran = generator.Next(0, 9999).ToString("D4");
                    flag = dts.CheckIfCarProductExists(Properties.Settings.Default.DBName, ran);
                }               
                cars.Add(new CarProduct
                {
                    unique_id = ran,
                    title = i.ToString(),
                    description = i.ToString(),
                    category_id = "1908",
                    category_name = caching.CategoriesCached.Where(c => c.id == "1908").ToList()[0].name,
                    price = i,
                    photo = "http://antallaktika-smart.com/wp-content/uploads/Car_gr/SM-F2-3-001.JPG",
                    conditions = "Μεταχειρισμένο",
                    update_interval = i,
                    make = "57",
                    make_name = caching.VehiclesCached.Where(c => c.make_id == "57").ToList()[0].make_name,
                    model = "1183",
                    model_name = caching.VehiclesCached.Where(c => c.model_id == "1183").ToList()[0].model_name,
                    yearfrom = 1980,
                    yearto = 2018,
                    amount = i,
                    isParent = "ΝΑΙ",
                    manufacturer_number = "none",
                    aftermarket_number = "none",
                    color = "none"
                }                
                );
                for (int j = 0; j < 10; j++)
                {
                    cars.Add(new CarProduct
                    {
                        unique_id = ran + "-" + j.ToString(),
                        title = i.ToString(),
                        description = i.ToString(),
                        category_id = "1908",
                        category_name = caching.CategoriesCached.Where(c => c.id == "1908").ToList()[0].name,
                        price = i,
                        photo = "http://antallaktika-smart.com/wp-content/uploads/Car_gr/SM-F2-3-"+j+".JPG",
                        conditions = "Μεταχειρισμένο",
                        update_interval = i,
                        make = "57",
                        make_name = caching.VehiclesCached.Where(c => c.make_id == "57").ToList()[0].make_name,
                        model = "1183",
                        model_name = caching.VehiclesCached.Where(c => c.model_id == "1183").ToList()[0].model_name,
                        yearfrom = 1980,
                        yearto = 2018,
                        amount = i,
                        isParent = "ΟΧΙ",
                        manufacturer_number = "none",
                        aftermarket_number = "none",
                        color = caching.colors[ i % 22]
                    }
                );
                }
            }
            dt.CreateCarProduct(Properties.Settings.Default.DBName, cars);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            XMLWrite xmlw = new XMLWrite();
            xmlw.ReadXML();
        }
    }
}
