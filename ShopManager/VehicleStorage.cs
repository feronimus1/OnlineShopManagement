﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace ShopManager
{
    [Table("VehicleStorage")]
    public class VehicleStorage
    {
        [Key]
        public int IDStore { get; set; }
        public string model_id { get; set; }
        public string model_name { get; set; }
        public string make_id { get; set; }
        public string make_name { get; set; }
        public int yearfrom { get; set; }
        public int yearto { get; set; }
        public string unique_id { get; set; }
        public string readable { get; set; }



        public bool Have(VehicleStorage temp)
        {
            if (
                temp.model_id == model_id &&
                temp.make_id == make_id &&
                temp.yearfrom == yearfrom &&
                temp.yearto == yearto 
                //temp.unique_id == unique_id 
                ) return true;
            return false;
        }

        //public VehicleStorage(string modelid,string modelname, string makeid,string makename,int year_from, int year_to, string uniqueid)
        //{
        //    model_id = modelid;
        //    model_name = modelname;
        //    make_id = makeid;
        //    make_name = makename;
        //    yearfrom = year_from;
        //    yearto = year_to;
        //    unique_id = uniqueid;
        //    readable =  make_name + "-" + model_name + "-" + yearfrom + "-" + yearto;
        //}
        //public static VehicleStorage FromCsv(string csvLine)
        //{
        //    string[] values = csvLine.Split(',');
        //    VehicleStorage tempvehicleStorage = new VehicleStorage();
        //    tempvehicleStorage.model_id = values[0].Trim('"');
        //    tempvehicleStorage.model_name = values[1].Trim('"');
        //    tempvehicleStorage.make_id = values[2].Trim('"');
        //    tempvehicleStorage.make_name = values[3].Trim('"');
        //    return tempvehicleStorage;
        //}


    }
}
