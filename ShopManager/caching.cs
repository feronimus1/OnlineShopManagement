﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ShopManager
{
    public static class caching
    {
        //public static List<CarProduct> CarProducsCached { get; set; } = new List<CarProduct>();
        //public static bool Flag { get; set; } = true;

        public static List<CarProduct> CarsCached { get; set; } = new List<CarProduct>();
        public static List<CarProduct> CarsOrder { get; set; } = new List<CarProduct>();
        public static List<vehicles> VehiclesCached { get; set; } = new List<vehicles>();
        public static List<categories> CategoriesCached { get; set; } = new List<categories>();       
        public static int LoginFlag { get; set; } = 0;
        public static int NumberOfCarProducts { get; set; } = -3;
        public static CarProduct CachedCar = new CarProduct();
        //public static bool ShowPhoto = Properties.Settings.Default.ShowPhotos;
        public static string EmptyColor = "N/A";
        public static List<string> colors = new List<string> { EmptyColor, "Κόκκινο", "Κόκκινο σκούρο", "Κίτρινο", "Πράσινο", "Πράσινο σκούρο", "Μαύρο", "Ασπρο", "Μπλεσκούρο", "Κόκκινο σκούρο", "Πορτοκαλί", "Λεμονί", "Ασημί", "Ροζ", "Καφέ", "Μπεζ", "Γκρι", "Χρυσαφί", "Μπλέ", "Μπλε σκούρο", "Χρώμιο", "Γαλάζιο", "Μπορντό", "Μόβ-Βιολετί", };

        // 0 =  nothing
        // 1 =  Close
        // 2 =  Pass
        public static int CloseAd = 0;
        public static int CloseProduct = 0;

        // 0 =  none
        // 1 == ad
        // 2 == product
        public static int StartLoopNUmber = 0;

        public static void ReCache()
        {
            DataAccess dt = new DataAccess();
            NumberOfCarProducts = dt.GetNumberOfCarProducts(Properties.Settings.Default.DBName);

            VehiclesCached.Clear();
            VehiclesCached.AddRange(dt.ReadVehicles(Properties.Settings.Default.DBName));

            CategoriesCached.Clear();
            CategoriesCached.AddRange(dt.ReadCategories(Properties.Settings.Default.DBName));
        }
    }
}
