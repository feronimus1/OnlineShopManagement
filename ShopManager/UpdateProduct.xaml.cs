﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for UpdateProduct.xaml
    /// </summary>
    public partial class UpdateProduct : System.Windows.Controls.UserControl
    {
        List<WorkFlowScriptIDDataStore> lst = new List<WorkFlowScriptIDDataStore>();
        List<categories> data = new List<categories>();
        List<vehicles> vehicles = new List<vehicles>();
        DataAccess dts = new DataAccess();
        string Imagepath = "";
        bool IsParent = false;
        bool loading = false;

        public UpdateProduct()
        {
            InitializeComponent();
            busyIndo.IsBusy = true;
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            busyIndo.IsBusy = true;
            Combo_Aftermarket_Manufacturer.ItemsSource = new List<string> { "Aftermarket", "Manufacturer" };
            Combo_Aftermarket_Manufacturer.SelectedIndex = 0;

            TextColor.ItemsSource = caching.colors;




            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += InisializeData;
            worker.RunWorkerCompleted += (o, a) =>
            {                
                UpdateWPF();
                busyIndo.IsBusy = false;
            };
            worker.RunWorkerAsync();
        }
        private void InisializeData(object sender, DoWorkEventArgs e)
        {
            while (caching.VehiclesCached.Count == 0 || caching.CategoriesCached.Count == 0)
            {
                System.Threading.Thread.Sleep(50);
            }
            while (caching.NumberOfCarProducts == 0 && caching.NumberOfCarProducts != 0)
            {
                System.Threading.Thread.Sleep(50);
            }
            data.Clear();
            data.AddRange(caching.CategoriesCached);
            vehicles.Clear();
            vehicles.AddRange(caching.VehiclesCached
                                 .GroupBy(item => item.make_id)
                                 .ToDictionary(grp => grp.Key, grp => grp.ToList())
                                 .Select(kvp => kvp.Value[0])
                                 .ToList());

            foreach (categories p in data.ToList())
            {
                //if (t.parent_id ==  "" && t.id=="30")
                if (p.parent_id == "77")
                {
                    WorkFlowScriptIDDataStore parent = new WorkFlowScriptIDDataStore()
                    {
                        Id = p.id,
                        Name = p.name,
                        ParentId = p.parent_id,
                        Place = 0,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    parent.Subcategories.AddRange(FindChilds(parent.Id));
                    lst.Add(parent);
                }
            }

        }
        public List<WorkFlowScriptIDDataStore> FindChilds(string id)
        {
            List<WorkFlowScriptIDDataStore> childList = new List<WorkFlowScriptIDDataStore>();
            while (data.Count == 0)
            {
                System.Threading.Thread.Sleep(10);
            }
            //int tempint = data.Count;
            //int count = 0;
            //categories tempcat;
            foreach (categories t in data.ToList())
            {
                //count++;
                if (t is null) continue;
                if (t.parent_id == id)
                {
                    WorkFlowScriptIDDataStore child = new WorkFlowScriptIDDataStore()
                    {
                        Id = t.id,
                        Name = t.name,
                        ParentId = t.parent_id,
                        Subcategories = new List<WorkFlowScriptIDDataStore>()
                    };
                    child.Subcategories.AddRange(FindChilds(child.Id));
                    if (child.Subcategories.Count == 0) child.Place = 2;
                    else child.Place = 1;
                    childList.Add(child);
                }
                //tempcat = t;
            }
            return childList;
        }
        private void InitializeCombos()
        {
            Combo_Aftermarket_Manufacturer.ItemsSource = new List<string> {"Aftermarket","Manufacturer" };
            Combo_Aftermarket_Manufacturer.SelectedIndex = 0;
            TextColor.ItemsSource = caching.colors;

            TextUnique_id_Child_Primary.ItemsSource = caching.CarsCached.Where(c => c.isParent == "ΝΑΙ");
            TextUnique_id_Child_Primary.DisplayMemberPath = "unique_id";
            TextUnique_id_Child_Primary.SelectedValuePath = "unique_id";
        }

        private void UpdateWPF()
        {
           

            treeView.ItemsSource = lst;
            TextMake.ItemsSource = vehicles;
            TextMake.SelectedIndex = 23;
            TextModel.SelectedIndex = 2;
            TextInterval.Text = "1";
            TextYearfrom.ItemsSource = GetDateList();
            TextYearfrom.SelectedIndex = 0;
            TextYearTo.ItemsSource = GetDateList();
            TextYearTo.SelectedIndex = GetDateList().Count - 1;
            TextCondition.ItemsSource = new List<string> { "Μεταχειρισμένο", "Καινούριο", "Ανακατασκευή" };
            TextisParent.ItemsSource = new List<string> { "ΝΑΙ" , "ΟΧΙ" };
            TextisParent.SelectedIndex = 0;
            TextCondition.SelectedIndex = 0;
        }
        private void treeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!IsParent) return;
            WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
            if (temp.Place != 2) return;
            Textcategory_id.IsOpen = false;
            Textcategory_id.Content = temp.Name;
            buffer.Text = temp.Id;
            cat.Text = temp.Id;
        }

        private void treeView_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (!IsParent) return;
            if (e.Key == Key.Return)
            {
                WorkFlowScriptIDDataStore temp = (WorkFlowScriptIDDataStore)treeView.SelectedItem;
                if (temp.Place != 2) return;
                Textcategory_id.IsOpen = false;
                Textcategory_id.Content = temp.Name;
                buffer.Text = temp.Id;
                cat.Text = temp.Id;
            }
        }

       

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!IsParent) return;
            TextPrice.Value = Increase10(TextPrice.Value ?? 0);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!IsParent) return;
            TextPrice.Value = Decrease10(TextPrice.Value ?? 0);
        }

        private long Increase10(long value)
        {
            return value + 10;
        }
        private long Decrease10(long value)
        {
            long temp = value - 10;
            if (temp < 0) value = 0;
            else value = temp;
            return value;
        }

        private void TextMake_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                string temp = TextMake.SelectedValue.ToString();
                TextModel.ItemsSource = dts.ReadModelVehicles(Properties.Settings.Default.DBName, temp);
                make.Text = temp;
            }
            catch (Exception)
            {

                return;
            }

        }

        public List<string> GetDateList()
        {
            List<string> datatemp = new List<string>();
            DateTime date;
            date = DateTime.Today;
            for (int i = 1980; i <= date.Year; i++)
            {
                datatemp.Add(i.ToString());
            }
            return datatemp;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string path = "";
            OpenFileDialog filedialog = new OpenFileDialog();
            filedialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            filedialog.FilterIndex = 1;
            filedialog.Multiselect = false;

            if (filedialog.ShowDialog() == DialogResult.OK)
            {
                path = filedialog.FileName;
            }
            else return;
            Imagepath = path;
            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Uploading Image...";
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += UploadImage;
            worker.RunWorkerCompleted += (o, a) =>
            {
                int pos = Imagepath.LastIndexOf("/") + 1;
                TextPhoto.Text = "http://vps545945.ovh.net/CarGR/" + Imagepath.Substring(pos, Imagepath.Length - pos);
                busyIndo.IsBusy = false;
            };
            worker.RunWorkerAsync();

        }
        private void UploadImage(object sender, DoWorkEventArgs e)
        {

            string path = Imagepath;
            string filename = System.IO.Path.GetFileName(path);
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(Properties.Settings.Default.Username, Properties.Settings.Default.Password);
                string FtpImagePath = Properties.Settings.Default.CarImagePath;
                FtpImagePath = FtpImagePath.Replace("/", "//");
                FtpImagePath = FtpImagePath.Replace("http:////", "ftp://");
                //client.UploadFile("ftp://antallaktika-smart.com//wp-content//uploads//Car_gr//" + filename, "STOR", path);
                client.UploadFile(FtpImagePath + filename, "STOR", path);
            }
            Imagepath = Properties.Settings.Default.CarImagePath + filename;
        }
       
        private void Button_Click_Upadate(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckConditions()) return;

                CarProduct car = new CarProduct
                {
                    //unique_id = caching.CachedCar.unique_id,
                    category_id = cat.Text,
                    category_name = Textcategory_id.Content.ToString(),
                    title = TextTitle.Text,
                    description = TextDescription.Text,
                    price = TextPrice.Value ?? 0,
                    photo = TextPhoto.Text,
                    conditions = TextCondition.Text,
                    isParent = TextisParent.Text,
                    update_interval = TextInterval.Value ?? 0,
                    make = TextMake.SelectedValue.ToString(),
                    make_name = TextMake.Text,
                    model = TextModel.SelectedValue.ToString(),
                    model_name = TextModel.Text,
                    yearfrom = Int32.Parse(TextYearfrom.Text),
                    yearto = Int32.Parse(TextYearTo.Text),
                    amount = TextAmount.Value ?? 0,
                    color = TextColor.Text ?? "none",
                    manufacturer_number = "none",
                    aftermarket_number = "none",
                    StoragePlace = StoragePlaceTextBox.Text
                };
                if (IsParent) car.unique_id = TextUnique_id_Parent.Text;
                else car.unique_id = TextUnique_id_Child_Primary.Text + "-" + TextUnique_id_Child_Secondary.Text;
                
                if (Combo_Aftermarket_Manufacturer.Text == "Aftermarket" && !String.IsNullOrEmpty(Text_Aftermarket_Manufacturer.Text)) car.aftermarket_number = Text_Aftermarket_Manufacturer.Text;
                else if (Combo_Aftermarket_Manufacturer.Text == "Manufacturer" && !String.IsNullOrEmpty(Text_Aftermarket_Manufacturer.Text)) car.manufacturer_number = Text_Aftermarket_Manufacturer.Text;


                List<CarProduct> lst = new List<CarProduct>();
                lst.Add(car);

                //If exists update it
                if (dts.CheckIfCarProductExists(Properties.Settings.Default.DBName, car.unique_id))
                {
                    dts.UpdateCarPoduct(Properties.Settings.Default.DBName, lst);
                }
                //Does not exist and is a parent
                else if (car.isParent == "ΝΑΙ")
                {
                    dts.CreateCarProduct(Properties.Settings.Default.DBName, lst);
                }
                //Does not exist  and is a child
                else
                {
                    //If parent exists
                    string temp2 = car.unique_id.Split('-')[0];
                    if (!dts.CheckIfCarProductExists(Properties.Settings.Default.DBName, car.unique_id.Split('-')[0]))
                    {
                        System.Windows.MessageBox.Show("Δεν υπάρχει πατέρας για αυτο το αντικειμενο. Κάτι έχει πάει στραβά!");
                        return;
                    }
                    dts.CreateCarProduct(Properties.Settings.Default.DBName, lst);

                    
                    
                }
                //Update Parent quantity
                if (!IsParent)
                {
                    //Search all items of parent and add the quantities. Add them to parent
                    List<CarProduct> temp = new List<CarProduct>();
                    temp.AddRange(dts.ReadCarProductLike(Properties.Settings.Default.DBName, car.unique_id.Split('-')[0]));
                    int count = 0;
                    lst.Clear();
                    foreach (CarProduct tempcar in temp)
                    {
                        if (tempcar.isParent == "ΟΧΙ")
                        {
                            count += tempcar.amount;
                        }
                        if(tempcar.isParent == "ΝΑΙ")
                        {
                            lst.Add(tempcar);
                        }
                    }
                    lst[0].amount = count;
                    dts.UpdateCarPoduct(Properties.Settings.Default.DBName, lst);
                }

                System.Windows.MessageBox.Show("Αποθηκεύτηκε με Επιτυχία!");
            }
            catch (Exception ex)
            {

                System.Windows.MessageBox.Show("Παρουσιάστηκε πρόβλημα. Δεν αποθηκεύτηκε το προϊόν.      " + ex.ToString());
            }


        }
        private bool CheckConditions()
        {
            if (IsParent)
            {
                if (String.IsNullOrEmpty(TextUnique_id_Parent.Text))
                {
                    Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε Κωδικό!");
                    return true;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(TextUnique_id_Child_Secondary.Text) || String.IsNullOrEmpty(TextUnique_id_Child_Primary.SelectedValue.ToString()))
                {
                    Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε Κωδικό!");
                    return true;
                }
            }


            if (String.IsNullOrEmpty(TextTitle.Text)
                || String.IsNullOrEmpty(Textcategory_id.Content.ToString())
                  || String.IsNullOrEmpty((TextPrice.Value ?? 0).ToString())
                   || String.IsNullOrEmpty((TextInterval.Value ?? 0).ToString())
                    //|| String.IsNullOrEmpty(TextPhoto.Text)
                     || String.IsNullOrEmpty(TextCondition.Text)
                      || String.IsNullOrEmpty(TextisParent.Text)
                       //|| String.IsNullOrEmpty(TextColor.Text)
                        //|| String.IsNullOrEmpty(TextDescription.Text)
                         || String.IsNullOrEmpty(TextMake.SelectedValue.ToString())
                          || String.IsNullOrEmpty(TextModel.SelectedValue.ToString())
                           || String.IsNullOrEmpty(TextYearfrom.Text)
                            || String.IsNullOrEmpty(TextYearTo.Text)
                             || String.IsNullOrEmpty((TextAmount.Value ?? 0).ToString()))

            {
                Xceed.Wpf.Toolkit.MessageBox.Show("Παρακαλώ συμπληρώστε όλα τα πεδία.");
                return true;
            }
            else return false;
        }
        private void TextModel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                model.Text = TextModel.SelectedValue.ToString();
            }
            catch (Exception)
            {

                return;
            }

        }
        string Url = "";
        private void loadImage()
        {
            if (!Properties.Settings.Default.ShowPhotos) return;
            try
            {
                Url = TextPhoto.Text;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(Url, UriKind.Absolute);
                bitmap.EndInit();
                Picturebox.Source = bitmap;
            }
            catch (Exception)
            {
            }               
        }     

        private void CheckParent_Child()
        {
            try
            {

                if (caching.CachedCar.isParent == "ΝΑΙ") IsParent = true;
                else  IsParent = false;
                HandleChildParentChange();

                if (IsParent)
                {
                    TextUnique_id_Parent.Text = caching.CachedCar.unique_id;

                    int index = -1;
                    for (int i = 0; i < TextUnique_id_Child_Primary.Items.Count; i++)
                    {
                        CarProduct temp = (CarProduct)TextUnique_id_Child_Primary.Items[i];
                        if (temp.unique_id == caching.CachedCar.unique_id.Split('-')[0])
                        {
                            index = i;
                            break;
                        }
                    }

                    TextUnique_id_Child_Primary.SelectedIndex = index;
                }
                else
                {
                    TextisParent.IsReadOnly = true;
                    int index = -1;
                    for (int i = 0; i < TextUnique_id_Child_Primary.Items.Count; i++)
                    {
                        CarProduct temp = (CarProduct)TextUnique_id_Child_Primary.Items[i];
                        if (temp.unique_id == caching.CachedCar.unique_id.Split('-')[0])
                        {
                            index = i;
                            break;
                        }
                    }

                    TextUnique_id_Child_Primary.SelectedIndex = index;
                    TextUnique_id_Child_Secondary.Text = caching.CachedCar.unique_id.Split('-')[1];
                }
            }
            catch (Exception)
            {

                return;
            }
        }


        private void LoadCachedProduct()
        {  
            TextTitle.Text = caching.CachedCar.title;
            TextPhoto.Text = caching.CachedCar.photo;
            Textcategory_id.Content = caching.CachedCar.category_name;
            TextPrice.Text = caching.CachedCar.price.ToString();
            TextInterval.Text = caching.CachedCar.update_interval.ToString();
            TextCondition.Text = caching.CachedCar.conditions;
            TextisParent.SelectedIndex = caching.CachedCar.isParent == "ΝΑΙ" ? 0 : 1;
            cat.Text = caching.CachedCar.category_id;
            make.Text = caching.CachedCar.make;
            TextAmount.Text = caching.CachedCar.amount.ToString();
            model.Text = caching.CachedCar.model;
            TextDescription.Text = caching.CachedCar.description;
            TextMake.Text = caching.CachedCar.make_name;
            TextModel.Text = caching.CachedCar.model_name;
            TextYearfrom.Text = caching.CachedCar.yearfrom.ToString();
            TextYearTo.Text = caching.CachedCar.yearto.ToString();
            TextColor.Text = caching.CachedCar.color;
            StoragePlaceTextBox.Text = caching.CachedCar.StoragePlace;
            if (caching.CachedCar.manufacturer_number != "none")
            {
                Combo_Aftermarket_Manufacturer.SelectedIndex = 2;
                Text_Aftermarket_Manufacturer.Text = caching.CachedCar.manufacturer_number;
            }
            else if (caching.CachedCar.aftermarket_number != "none")
            {
                Combo_Aftermarket_Manufacturer.SelectedIndex = 1;
                Text_Aftermarket_Manufacturer.Text = caching.CachedCar.aftermarket_number;
            }
        }

        public void LoadProcedure()
        {
            loading = true;
            InitializeCombos();
            CheckParent_Child();
            LoadCachedProduct();            
            loadImage();
            loading = false;
        }

        private void TextYearfrom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextisParent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TextisParent.SelectedIndex == 0 ) IsParent = true;
            else IsParent = false;
            HandleChildParentChange();
        }
        private void HandleChildParentChange()
        {

            if (!IsParent)
            {    
                TextUnique_id_Parent.Visibility = Visibility.Collapsed;
                TextUnique_id_Child_Primary.Visibility = Visibility.Visible;
                TextUnique_id_Child_Secondary.Visibility = Visibility.Visible;

                TextInterval.IsReadOnly = true;
                TextPrice.IsReadOnly = true;
                TextInterval.IsReadOnly = true;
                TextCondition.IsHitTestVisible = !true;
                TextDescription.IsReadOnly = true;
                Combo_Aftermarket_Manufacturer.IsHitTestVisible = !true;
                Text_Aftermarket_Manufacturer.IsReadOnly = true;

                TextColor.IsHitTestVisible = !false;
                TextAmount.IsReadOnly = false;
            }
            else
            {
                TextUnique_id_Parent.Visibility = Visibility.Visible;
                TextUnique_id_Child_Primary.Visibility = Visibility.Collapsed;
                TextUnique_id_Child_Secondary.Visibility = Visibility.Collapsed;

                TextInterval.IsReadOnly = false;
                TextPrice.IsReadOnly = false;
                TextInterval.IsReadOnly = false;
                TextCondition.IsHitTestVisible = !false;
                TextDescription.IsReadOnly = false;
                Combo_Aftermarket_Manufacturer.IsHitTestVisible = !false;
                Text_Aftermarket_Manufacturer.IsReadOnly = false;

                TextColor.IsHitTestVisible = !true;
                TextAmount.Value = 0;
                TextAmount.IsReadOnly = true;
            }
        }

        private void TextUnique_id_Child_Primary_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (loading) return;
            string temp = TextUnique_id_Child_Secondary.Text;
            caching.CachedCar = caching.CarsCached.Where(v => v.unique_id == TextUnique_id_Child_Primary.SelectedValue.ToString()).ToList()[0];
            LoadProcedure();
            TextUnique_id_Child_Secondary.Text = temp;
            TextisParent.SelectedIndex = 1;
        }

        private void Button_Click_ΝewParent(object sender, RoutedEventArgs e)
        {
            IsParent = true;
            HandleChildParentChange();

            DataAccess dts = new DataAccess();
            String ran="";
            bool flag = true;
            while (flag)
            {
                Random generator = new Random();
                ran = generator.Next(0, 9999).ToString("D4");
                flag = dts.CheckIfCarProductExists(Properties.Settings.Default.DBName,ran);
            }           

            TextUnique_id_Parent.Text = ran;

            TextUnique_id_Parent.IsReadOnly = true;
            TextisParent.IsHitTestVisible = !true;

            //new code create last one
            //lock to parent mode
        }
    }
}





