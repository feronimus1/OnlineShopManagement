﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShopManager
{
    /// <summary>
    /// Interaction logic for CarGr.xaml
    /// </summary>
    public partial class CarGr : UserControl
    {
        public CarGr()
        {
            InitializeComponent();
            DataAccess dts = new DataAccess();

            Properties.Settings.Default.CarXMLPath = dts.ReadInfo(Properties.Settings.Default.DBName).Where(i => i.id == Properties.Settings.Default.CarXMLPathInfoID).ToList()[0].MainInfo;
            CarHtmlTextBox.Text = Properties.Settings.Default.CarXMLPath;

            
            StampText.Text = dts.ReadInfo(Properties.Settings.Default.DBName).Where(c => c.id == 1).ToList()[0].MainInfo;
        }

        private void Button_ReadCategoreis_Click(object sender, RoutedEventArgs e)
        {
            CsvReader scvR = new CsvReader();
            scvR.ReadCategories();
        }

        private void Button_ReadVehicles_Click(object sender, RoutedEventArgs e)
        {
            CsvReader scvR = new CsvReader();
            scvR.ReadVehicles();
        }       
        
        private void Button_XmlUrlUpdate_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result =System.Windows.MessageBox.Show("Μην ξεχάσετε να ενημερώσετε και το Car.gr!", "Σημαντικό!!!", MessageBoxButton.OK);
            Properties.Settings.Default.CarXMLPath = CarHtmlTextBox.Text;
            Properties.Settings.Default.Save();

            List<Info> infolist = new List<Info>();
            infolist.Add(new Info { MainInfo = Properties.Settings.Default.CarXMLPath, id = Properties.Settings.Default.CarXMLPathInfoID });

            DataAccess dts = new DataAccess();
            dts.UpdateInfo(Properties.Settings.Default.DBName, infolist);
        }

        private void IconButton_Click(object sender, RoutedEventArgs e)
        {        
            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Uploading...";
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += UploadXML;
            worker.RunWorkerCompleted += (o, a) =>
            {
                try
                {             
                    busyIndo.IsBusy = false;
                }
                catch (Exception)
                {
                }

            };
            worker.RunWorkerAsync();
        }

        private void UploadXML(object sender, DoWorkEventArgs e)
        {
            XMLWrite xmlW = new XMLWrite();
            xmlW.WriteXML();
        }

        private void Button_Stamp_Save(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.stamp = StampText.Text;
            Properties.Settings.Default.Save();

            List<Info> infolist = new List<Info>();
            infolist.Add((new Info { id = Properties.Settings.Default.StampID, MainInfo = Properties.Settings.Default.stamp }));

            //First time

            //DataAccess dts = new DataAccess();
            //dts.CreateInfo(Properties.Settings.Default.DBName, infolist);

            //uncomment
            DataAccess dts = new DataAccess();
            dts.UpdateInfo(Properties.Settings.Default.DBName, infolist);

            System.Windows.MessageBox.Show("Αποθηκεύτηκε με Επιτυχία!");
        }

        private void Button_Click_CsvCreate(object sender, RoutedEventArgs e)
        {
            busyIndo.IsBusy = true;
            busyIndo.BusyContent = "Creating...";
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += UploadCsv;
            worker.RunWorkerCompleted += (o, a) =>
            {
                try
                {
                    busyIndo.IsBusy = false;
                }
                catch (Exception)
                {
                }

            };
            worker.RunWorkerAsync();
        }
        private void UploadCsv(object sender, DoWorkEventArgs e)
        {
            CsvMaker csm = new CsvMaker();
            csm.WriteWooCommerseCsv();
        }

    }
}
