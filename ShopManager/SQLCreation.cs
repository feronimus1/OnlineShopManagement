﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopManager
{
    class SQLCreation
    {
        public string CreateDB()
        {
            return @"CREATE DATABASE IF NOT EXISTS admin_Manager;";
        }

       
        public string User()
        {
            //throw new NotImplementedException();
            return @"DROP TABLE IF EXISTS User;
                          CREATE TABLE User (
                          auth           INT(2) NOT NULL DEFAULT '1' ,
                          UserName       VARCHAR(50) NOT NULL , 
                          Password       VARCHAR(50) NOT NULL 
                          )";
        }
        public string CreateFirstUser()
        {
            return @"INSERT INTO `User` (`auth`, `UserName`, `Password`) VALUES ('1', 'user', 'user');";
        }
        public string CarProduct()
        {
            //throw new NotImplementedException();
            return @"DROP TABLE IF EXISTS CarProduct;
                          CREATE TABLE CarProduct (						  
						  unique_id				VARCHAR(50) NOT NULL PRIMARY KEY,
						  title					TEXT, NOT NULL,
						  description			TEXT,
						  category_id			VARCHAR(200) NOT NULL ,
						  category_name			TEXT,
						  price					NUMERIC (5,2) NOT NULL CHECK (price > 0),
						  photo					TEXT,
						  conditions			VARCHAR(50),
						  update_interval		SMALLINT,
						  make_name				TEXT,
						  make					VARCHAR(200),
						  model_name			TEXT,
						  model					VARCHAR(200),
						  yearfrom				SMALLINT,
						  yearto				SMALLINT,
                          amount                SMALLINT,
                          isParent              VARCHAR(20),
                          color                 VARCHAR(20),
                          manufacturer_number   VARCHAR(40),
                          aftermarket_number    VARCHAR(40),
                          StoragePlace          VARCHAR(64),
                          AdId                  VARCHAR(200),
                          taken                 SMALLINT NOT NULL
                          );";
        }
        public string Info()
        {
            return @"DROP TABLE IF EXISTS Info;
                          CREATE TABLE `Info` (
                          MainInfo              TEXT NOT NULL , 
                          id                    INT(11) NOT NULL AUTO_INCREMENT , PRIMARY KEY 
                          );";
        }
        public string VehicleStorage()
        {
            //throw new NotImplementedException();
            return @"DROP TABLE IF EXISTS VehicleStorage;
                          CREATE TABLE VehicleStorage (		
                          IDStore               INT(11) NOT NULL AUTO_INCREMENT , PRIMARY KEY,
						  model_id			    VARCHAR(10) NOT NULL ,
						  model_name			VARCHAR(100) NOT NULL,		
						  make_id				VARCHAR(10) NOT NULL,
						  make_name				VARCHAR(50)	NOT NULL,
                          yearfrom				SMALLINT,
						  yearto				SMALLINT,
                          unique_id				VARCHAR(50) NOT NULL,
                          readable              VARCHAR(200) NOT NULL,
                          FOREIGN KEY (unique_id) REFERENCES CarProduct(unique_id)
                          );";
        }


        public string FOREIGN_KEY(int number)
        {
            //throw new NotImplementedException();
            return "SET FOREIGN_KEY_CHECKS = " + number + "; ";
        }

        public string AuthUser(string userName, string Password)
        {
            return $"SELECT auth" +
                $"   FROM User" +
                $"   WHERE UserName = '{userName}' AND Password = '{Password}';";
        }
        public string AuthUserName(string userName)
        {
            return $"SELECT auth" +
                $"   FROM User" +
                $"   WHERE UserName = '{userName}';";
        }


        //public string vehicle_makes()
        //{
        //    //throw new NotImplementedException();
        //    return @"DROP TABLE IF EXISTS vehicle_makes;
        //                  CREATE TABLE vehicle_makes (						  
        //make_id				VARCHAR(10) NOT NULL PRIMARY KEY,
        //name					VARCHAR(50) NOT NULL,						 
        //                  )";
        //}

        public string categories()
        {
            //throw new NotImplementedException();
            return @"DROP TABLE IF EXISTS categories;
                          CREATE TABLE categories (						  
						  id				    VARCHAR(10) NOT NULL PRIMARY KEY,
                          parent_id  		    VARCHAR(10) NOT NULL,
						  name					VARCHAR(100) NOT NULL,		
						  path					TEXT NOT NULL,				
						  has_make				VARCHAR(10)						 
                          );";
        }

        public string vehicles()
        {
            //throw new NotImplementedException();
            return @"DROP TABLE IF EXISTS vehicles;
                          CREATE TABLE vehicles (						  
						  model_id			    VARCHAR(10) NOT NULL PRIMARY KEY,
						  model_name			VARCHAR(100) NOT NULL,		
						  make_id				VARCHAR(10) NOT NULL,
						  make_name				VARCHAR(50)			 
                          );";
        }
        public string Customers()
        {
            return @"DROP TABLE IF EXISTS Customers;
                          CREATE TABLE Customers (						  
						  CustomerID			VARCHAR(10) NOT NULL PRIMARY KEY,
						  Name  			    VARCHAR(100) NOT NULL,	
						  Address				VARCHAR(100),	
						  State  				VARCHAR(50),			
						  PostCode				VARCHAR(10),			
						  Email  				VARCHAR(100),			
						  Phone1				VARCHAR(20),			
						  Phone2				VARCHAR(20),			
						  AFM			    	VARCHAR(20),			
						  DOY   				VARCHAR(50),			
						  Profession   		    VARCHAR(50),
                          Discount              SMALLINT(3)
                          );";
        }
        public string Store()
        {
            return @"DROP TABLE IF EXISTS Store;
                          CREATE TABLE Store (						  
						  StoreID			    VARCHAR(10) NOT NULL PRIMARY KEY,
                          StoreName             VARCHAR(100) NOT NULL,
						  OwnerFirstName	    VARCHAR(100),	
						  OwnerLastName		    VARCHAR(100), 
						  Address				VARCHAR(100),	
						  State  				VARCHAR(50),			
						  PostCode				VARCHAR(10),			
						  Email  				VARCHAR(100),			
						  Phone1				VARCHAR(20),
						  Phone2				VARCHAR(20),
                          Site                  VARCHAR(100)
                          );";
        }
        public string Invoice()
        {
            return @"DROP TABLE IF EXISTS Invoice;
                          CREATE TABLE Invoice (						  
						  InvoiceID			    VARCHAR(10) NOT NULL PRIMARY KEY,					  
						  StoreID			    VARCHAR(10) NOT,	  
						  CustomerID			VARCHAR(10) NOT,
                          FOREIGN KEY (StoreID) REFERENCES Store(StoreID),
                          FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID),
                          TransactionDate       DATE NOT NULL,
                          InvoiceDate           DATE NOT NULL,
                          Currency              VARCHAR(10)
                          );";
        }
        public string InvoiceContainer()
        {
            return @"DROP TABLE IF EXISTS InvoiceContainer;
                          CREATE TABLE InvoiceContainer (						  
						  InvoiceID			    VARCHAR(10) NOT NULL PRIMARY KEY,
                          Discount              SMALLINT(3),	
                          Tax                   SMALLINT(3),    
						  unique_id			    VARCHAR(20) NOT NULL PRIMARY KEY,
                          FOREIGN KEY (InvoiceID) REFERENCES Invoice(InvoiceID),
                          FOREIGN KEY (unique_id) REFERENCES CarProduct(unique_id)
                          );";
        }
    }



}
